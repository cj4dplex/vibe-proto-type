import glob
import os.path

import numpy as np
import cv2
from PIL import Image


def read_images(path, fmt=None):
    img_files = sorted(glob.glob(os.path.join(path, "*.png")))

    images = []
    for img_file in img_files:
        if fmt == "YCbCr":
            bgr_img = cv2.imread(img_file, 1)
            img = cv2.cvtColor(bgr_img, cv2.COLOR_BGR2YCrCb)
        elif fmt == "YUV":
            bgr_img = cv2.imread(img_file, 1)
            img = cv2.cvtColor(bgr_img, cv2.COLOR_BGR2YUV)
        else:
            img = cv2.imread(img_file, 1)
        images.append(img)

    frames = np.stack(images, axis=0)
    # print("Read Images is Finished ...!")
    return frames


def read_masks(path, mode=None):
    mask_files = sorted(glob.glob(os.path.join(path, "*.png")))

    masks = []
    for mask_file in mask_files:
        mask = Image.open(mask_file).convert("P")
        binary_mask = np.array(mask) * 255
        if mode == 'NOT':
            np_mask = cv2.bitwise_not(binary_mask)
        else:
            np_mask = binary_mask
        masks.append(np_mask)

    frames = np.stack(masks, axis=0)
    # print("Read Masks is Finished ...!")
    return frames


def read_video(path, fmt=None):
    vidcap = cv2.VideoCapture(path)
    success, image = vidcap.read()

    images = []
    while success:
        if fmt == "YCbCr":
            img = cv2.cvtColor(image, cv2.COLOR_BGR2YCrCb)
        elif fmt == "YUV":
            img = cv2.cvtColor(image, cv2.COLOR_BGR2YUV)
        else:
            img = image.copy()

        success, image = vidcap.read()
        images.append(img)
    frames = np.stack(images, axis=0)
    print("Read Video is Finished ...!")
    return frames


def count_pixel(mask):
    return np.count_nonzero(mask)


def get_histogram(image, mask, area_pixel, fmt=None):
    """
    :param image: BGR, YCbCr or YUV
    :return: Histogram Value
    """
    if fmt == 'YCbCr' or fmt == 'YUV':
        # hist = cv2.calcHist([image], [1, 2], mask, [256, 256], [0, 256, 0, 256])
        cb = cv2.calcHist([image], [1], mask, [256], [0, 256])
        cr = cv2.calcHist([image], [2], mask, [256], [0, 256])
        norm_cb = cb / area_pixel
        norm_cr = cr / area_pixel
        norm_hist = np.array([norm_cb, norm_cr])
    else:
        # hist = cv2.calcHist([image], [0, 1, 2], mask, [256, 256, 256], [0, 256, 0, 256, 0, 256])
        b = cv2.calcHist([image], [0], mask, [256], [0, 256])
        g = cv2.calcHist([image], [1], mask, [256], [0, 256])
        r = cv2.calcHist([image], [2], mask, [256], [0, 256])
        norm_b = b / area_pixel
        norm_g = g / area_pixel
        norm_r = r / area_pixel
        norm_hist = np.array([norm_b, norm_g, norm_r])

    # Normalize Histogram
    # norm_hist = hist / area_pixel

    return norm_hist


def mean_histogram(frames_hist, deno=None):
    sum_hist = None
    for idx, frame_hist in enumerate(frames_hist):
        if idx == 0:
            sum_hist = frame_hist
        else:
            sum_hist += frame_hist

    if deno is None:
        mean_hist = sum_hist / len(frames_hist)
    else:
        mean_hist = sum_hist / deno

    # cv2.normalize(mean_hist, mean_hist, 0, 1, cv2.NORM_MINMAX)

    return mean_hist


def hist_similar(hist1, hist2):
    hist_diff = hist1 - hist2
    sum_diff = np.sum(abs(hist_diff))

    h, w = hist_diff.shape

    sim_val = sum_diff / (h * w)

    return sim_val