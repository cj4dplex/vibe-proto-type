import os

from hist_util import *


def histExtract(imgPath, maskPath, outputPath, fmt="BGR"):
    """
    [input]
    * imgPath : RGB Images Folder Path
    * maskPath : Dynamic (Fore-Ground) Masks Folder Path
    * ouputPath : Video Histogram Save Path
    """

    images = read_images(imgPath, fmt=fmt)
    if maskPath is not None:
        bg_masks = read_masks(maskPath, mode='NOT')
        mask_cnt = len(bg_masks)
    else:
        bg_masks = None
        mask_cnt = len(images)

    print("# [Hist Extract] Load Images & Masks is Finished ...!")

    if len(images) == mask_cnt:
        total_px = 0
        frames_hist = []
        if bg_masks is not None:
            for img, msk in zip(images, bg_masks):
                px_cnt = count_pixel(msk)
                frame_hist = get_histogram(img, msk, px_cnt, fmt=fmt)

                total_px += px_cnt
                frames_hist.append(frame_hist)
        else:
            for img in images:
                h, w, _ = img.shape()
                msk = None
                px_cnt = h * w
                frame_hist = get_histogram(img, msk, px_cnt, fmt=fmt)

                total_px += px_cnt
                frames_hist.append(frame_hist)

        mean_hist = mean_histogram(frames_hist)

        scene_name = os.path.basename(imgPath)
        np.save(os.path.join(outputPath, "{}.npy".format(scene_name)), mean_hist)
        print("# [Hist Extract] Create Histogram Features-({}) is Finished ...!".format(fmt))

    else:
        print("[Error] Number of Images and Masks is not Equal ...!")

# def getHistSimilar(scenePath, tagGroups, savePath):
#     # Load histogram Data
#     hist_dataPath = os.path.join(savePath, '/histogram_feature')
#     # featFiles = [_ for _ in os.listdir(hist_dataPath) if _.endswith('npy')]
#     featFiles = glob.glob(os.path.join(savePath, "*.npy"))
#
#     featNames = []
#     feat_db = {}
#     for f in featFiles:
#         featName = os.path.basename(f).split(".")[0]
#         featNames.append(f)
#
#         feat = np.load(f)
#         feat_db[f] = feat
#
#
#     for t in tagGroups:
#         if t in featNames:
#
#
#
#     feat_db = {}
#     for n, f in enumerate(featFiles):
#         if n % 10 == 0 or n == len(featFiles) - 1:
#             print("Load Histogram Feats -- ({} / {})".format(n, len(featFiles) - 1))
#         # print("Query Name : {}".format(f))
#         query_feat = np.load(f)
#         feat_db[f] = query_feat

