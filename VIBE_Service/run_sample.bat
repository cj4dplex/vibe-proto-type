
set envname=cuda11env
set drive=D:
set root_path=\Projects\bitbucket\vibe-proto-type
set script_path=".\VIBE_Service\VibeServer.py"

call conda activate %envname%
call %drive%
call cd%root_path%
call python %script_path%
pause