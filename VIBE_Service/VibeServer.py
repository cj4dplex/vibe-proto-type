import sys, os
from Util.Definitions_for_VIBESERVICE import _SLEEP_UNIT
# from Util.timeStamp import TimeStamper
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
from Util.qCtrl import Procq
import sys

from threading import Thread
import time

import keyboard

class VibeService:
    threadFlag = {}
    id = 0
    q = Procq()

    def __init__(self, init_id):
        self.threadFlag = {}
        self.id = init_id
        # self.q.qPush("")
        # sprint()

    def __del__(self):
        print("run __del__ function")
        # self.VibeServerRun()
        # return 0

    def _keyCapture(self, id):
        input = keyboard.read_key()
        if input == "esc":
            # try:
            #     test = self.threadFlag[id]
            # except:
            self.threadFlag[id] = False
            print("\n== Call abort, id : %s, state : %s ==" %(id, self.threadFlag[id]))
            sys.stdin.flush()
            sys.stdout.flush()
            time.sleep(_SLEEP_UNIT)

    def VibeServerRun(self):
        # self.id = self.id + 1
        id = str(self.id)
        print("=== Run Service ID : %s ===" % id)
        Thread(target=self._keyCapture, args=(id), name=id, daemon=True).start()
        self.VibeServerService(id)
        return -1

    def VibeServerService(self, id):
        print("== run VibeServerService ==")
        self.threadFlag[id] = True
        # Thread(target=self._keyCapture, args=(id), name=id, daemon=True).start()
        flag = False
        count = 0
        while self.threadFlag[id]:
            f = open("condition.txt")
            state = f.readline()
            self.threadFlag[id] = int(state)
            count = count + 1
            print("\rfrom file : %d, id : %s, state : %s, %06d" %(int(state), self.id, self.threadFlag[id], count), end = '')

            if count == 300:
                print("\n%s proc done" %id)
                self.threadFlag[id] = False

            time.sleep(_SLEEP_UNIT)

        return -1


if __name__ == '__main__':
    id = 0
    while True:
        serv = VibeService(id)
        serv.VibeServerRun()
        id = id + 1

    print("all process terminated")

