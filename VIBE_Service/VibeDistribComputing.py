from Util import Definitions_for_VIBESERVICE as DEF


def CopyPreparedDataToServer(preparedData_list, ServerStates):

    return -1

def CheckEnvironment(serverInfo_list):
    _tryUpdateEnvironment
    return serverInfo_list


def _ServerInfoParser(server_info_file_pointer):

    return serverInfo


def GetServerInfo(setSpecificState=-1):
    serverInfo_list = []

    for id in DEF.SERVER_LIST:
        ip_addr = DEF.SERVER_IP_PREFIX + id

        #load server state file pointer

        serverInfo = _ServerInfoParser()

        if setSpecificState == -1:
            serverInfo_list.append(serverInfo)
        else:
            if serverInfo.state == setSpecificState:
                serverInfo_list.append(serverInfo)

    #Server state
    return serverInfo_list


def GitUpdate(serverInfo_list):

    #--- get update ---#

    return serverInfo_list


def GitUpdateAll(serverInfo_list):
    updatedServerInfo_list = []
    for serverInfo in serverInfo_list:
        serverInfo = GitUpdate(serverInfo)

        if serverInfo.isUpdate == True:
            updatedServerInfo_list.append(serverInfo)

    return updatedServerInfo_list


def GetUsableServerList(algorithm_type):
    IdleServerInfo_list = GetServerInfo(DEF.SERVER_STATE_IDLE)
    updatedServerInfo_list = GitUpdateAll(IdleServerInfo_list)
    usableServerInfo_list = CheckEnvironment(updatedServerInfo_list)

    return usableServerInfo_list


def MakeQuantumData(data_path, algorithm_type, unit_size=0):
    quantumSize = DEF.QUANTUM_SIZE[algorithm_type]
    quantumData_list = []


    return quantumData_list


#a.k.a. BDD (Balanced-data-distributor), SQL like, low level imitation
def DataDistributor(data_path, algorithm_type):
    # 1. Make data for distributed computing
    data_list = MakeQuantumData(algorithm_type, data_path)



#######################################################################################################################
# queue-ing strategy
# 1. make queue list for each server
# 2. client management whole queue list
# 3. monitoring abort

def VDC_SR_Launcher():
    # 1. get queue

    # 2. check and add queue

    # 3. Launch (and quit launcher)



#######################################################################################################################


def VibeDistribComputing(algorithm_type, data_path):
    # 1. Get Usable Server list
    usableServer_list = GetUsableServerList(algorithm_type)

    # 2. Data distributing
    readyServer_list = DataDistributor(data_path, algorithm_type, usableServer_list)

    # 3. make queue and Launch server thread
