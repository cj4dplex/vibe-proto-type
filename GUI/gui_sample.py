import os
import tkinter as tk
import tkinter.ttk as ttk
import tkinter.filedialog as tkFileDialog
import tarfile
import gzip
from tkinter import messagebox

class tarView():
    def __init__(self):
        super().__init__()
        self.window = tk.Tk()
        self.window.title("Tar Extracted File View")
        self.window.geometry("600x1200")

        self.tree = ttk.Treeview(self.window, height=55)
        self.tree.pack()
        self.tree.heading('#0', text="File Tree")
        self.tree.column('#0', width=600, anchor="w")

        self.label = tk.Label(self.window, text="")

        self.txt = tk.Entry(self.window)
        self.btn = tk.Button(self.window, overrelief="solid", width=15, command=self.close, text="종료",
                                     repeatdelay=1000, repeatinterval=100)

        self.btn.pack()

        self.txt.pack()

        self.label.pack()

        self.fileselect_button = tk.Button(self.window, overrelief="solid", width=15, command=self.find_file, text="파일선택",
                                           repeatdelay=1000, repeatinterval=100)
        self.fileselect_button.pack()
        self.exit_button = tk.Button(self.window, overrelief="solid", width=15, command=self.close, text="종료",
                                     repeatdelay=1000, repeatinterval=100)
        self.exit_button.pack()

        self.mouseRight = tk.Menu(self.window, tearoff=0)
        self.mouseRight.add_command(label="압축풀기")
        self.mouseRight.add_command(label="취소")

        self.window.bind("<Button-3>", self.clickRight)

        self.window.mainloop()

    def msgBox(self):
        tk.messagebox.showerror("Select File Error", "Can't Find Tar file \n Please Check Your File")

    def close(self):
       self.window.quit()

    def get_filetree(self, filename):
        # Get TAR items
        name = os.path.basename(filename)
        _, ext = os.path.splitext(name)

        flist = None
        if ext == ".tar":
            with tarfile.TarFile(filename) as taropen:
                flist = taropen.getmembers()
        else:
            self.msgBox()

        if flist is not None:
            for item in flist:
                parent, child = os.path.split(item.path)
                self.tree.insert(parent, 'end', iid=item.path, text=child)

    def find_file(self):
        filename = tkFileDialog.askopenfilename(filetypes=(("tar files", "*.tar"),("All files", "*.*")))
        if filename != "":
            self.label.config(text=filename)
            self.tree.delete(*self.tree.get_children())
            self.get_filetree(filename)

    def clickRight(self, event):
        try:
            self.mouseRight.tk_popup(event.x_root, event.y_root)
            print(event.x_root, event.y_root)
        finally:
            self.mouseRight.grab_release()


if __name__ == '__main__':
    tarView()