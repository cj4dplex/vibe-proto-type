from os import listdir, makedirs
from os.path import isfile, join, splitext
import os
import shutil
import sys
import time
import datetime

import argparse

sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
from Util.qCtrl import Procq
from Util.timeStamp import TimeStamper
import Util.Definitions_for_VIBESERVICE as _def

from Util.Logger import dprint

_debug = _def._DEBUG_MODE
_NasRoot = "c"
_smallFileThresh = (1024*1024)
_loopFq = 300
_ResultName = "AssetWatchResult"

_qNameEncVibe = _def.QNAME_DEF_ENCODING_FOR_VIBE
_qNameEncPreview = _def.QNAME_DEF_ENCODING_FOR_PREVIEW

extKeys = {"image", "video", "zip", "3d", "folder", "unknown"}
extValues = {}
extValues["image"] = {".png", ".jpg", ".jpeg", ".bmp", ".tif", ".tiff", ".exr", ".tga", ".cr2"}
extValues["video"] = {".mxf", ".mov", ".mp4"}
extValues["zip"] = {".zip", ".tar"}
extValues["3d"] = {".obj", ".abc", ".fbx", ".3dm", ".ma", ".usd"}

skipList = {'Thumbs.db', "#recycle", ".DStore"}

setDataWords = {"Set_Data", "SET_DATA", "set_data"}


########################################################################################################################
# for Cost comparing
def _findFoldersAndFiles(path):
    files = []
    folders = []
    for f in os.listdir(path):
        if isfile(join(path, f)):
            files.append(f)
        else:
            folders.append(f)

    return files, folders


def _IsThisTargetFile(ext, targetFileExts=[], opt='image'):
    if len(targetFileExts) == 0:
        if opt == 'video':
            targetFileExts = {".mxf", ".mov"}
        elif opt == 'image':
            targetFileExts = {".png", ".jpg", ".bmp"}
        else:
            print("wrong opt is inputted, (opt : image / vidio)\nyour input : %s" %opt)

    flag = False
    for targetExt in targetFileExts:
        if ext == targetExt:
            flag = True
            break

    return flag


def _make_targetFileMap(path, folderMap, specific_ext=[], forbidden_words=[], opt="image"):
    onlyfiles, onlyfolders = _findFoldersAndFiles(path)

    flag = False
    if len(onlyfiles) != 0:
        flag = True
        for filename in onlyfiles:
            if filename == 'Thumbs.db':
                continue

            _, ext = os.path.splitext(filename)
            flag = flag and _IsThisTargetFile(ext, specific_ext, opt)

            if flag == True:
                if False:
                    print('[%03d] add target folder : %s' %(len(folderMap) + 1, path))
                folderMap.append(os.path.join(path, filename))

    if flag == False:
        if len(onlyfolders) != 0:
            for folderName in onlyfolders:
                # forbidden words check for image folder
                forbidden_flag = False
                for forbidden_word in forbidden_words:
                    if forbidden_word in folderName:
                        forbidden_flag = True
                        break
                if forbidden_flag == False:
                    _make_targetFileMap(join(path, folderName), folderMap)

    return folderMap
########################################################################################################################


def typeChecker(filename, path):
    name, ext = splitext(filename)

    fileExts = extValues
    # fileExts["image"] = {".png", ".jpg", ".bmp"}
    # fileExts["video"] = {".mxf", ".mov"}
    # fileExts["zip"] = {".zip", ".tar"}
    # fileExts["3d"] = {".obj", ".abc", ".fbx"}

    videoFileExts = {".mxf", ".mov"}
    imageFileExts = {".png", ".jpg", ".bmp", ".tif", ".tiff"}
    compressedFileExts = {".zip", ".tar"}
    threeDfileExts = {".obj", ".abc", ".fbx", ".ma"}

    type = ""
    if ext == "":
        if os.path.isfile(os.path.join(path, filename)) == False:
            type = "folder"
    else:
        for key, targetExts in fileExts.items():
            if type != "":
                break
            for targetExt in targetExts:
                if ext == targetExt or ext.upper() == targetExt or ext.lower() == targetExt:
                    type = key
                    break

    if type == "":
        type = "unknown"

    return type


def mergeList(list_main, list_sub):
    for key, values in list_sub.items():
        if len(values) != 0:
            for inside_key in values:
                list_main[key][inside_key] = values[inside_key]

    return list_main


def skipChecker(name):
    flag = False
    for skip in skipList:
        if name == skip:
            flag = True
            break

    return flag


def listUp(path, lastList={}):
    list = {}
    for key in extKeys:
        list[key] = {}

    rawList = listdir(path)
    if len(lastList) == 0:
        for key in extKeys:
            lastList[key] = {}

    # dprint("==>>")
    # dprint("num of files from " + path + " is : " + str(len(rawList)))
    # dprint("num of folders from " + path + " is : " + str(len(list)))
    # dprint("<<==")
    folder_num = 0
    ret_video_num = 0
    ret_isLeaf = False
    ret_isVideoFolder = False
    ret_videoSizeTotal = 0
    for filename in rawList:
        # exception check
        if skipChecker(filename) == True:
            continue

        type = typeChecker(filename, path)
        filePath = os.path.join(path, filename)
        # duplication check
        if filePath in lastList[type]:
            # already listed item
            continue

        if type == "video":
            # video usable flag initialize
            isUsable = usableCheck(filePath)
            fileSize = os.path.getsize(filePath)

            # filtering too small files
            if fileSize < _smallFileThresh:
                continue

            # unusable video skip
            if isUsable == False:
                dprint("Unusable file, maybe it's downloading now or some problem block to read this file")
                dprint("skip this file : " + filePath)
                continue

            ret_videoSizeTotal = ret_videoSizeTotal + fileSize
            data = str(fileSize)
            list[type][filePath] = data
            ret_video_num = ret_video_num + 1
        elif type == "image":
            imageFolderName = os.path.dirname(filePath)
            if imageFolderName in list[type]:
                list[type][imageFolderName] = list[type][imageFolderName] + 1
            else:
                list[type][imageFolderName] = 1
        elif type == "folder":
            folder_num = folder_num + 1
            list[type][filePath] = False
        else:
            list[type][filePath] = True

    if folder_num == 0:
        ret_isLeaf = True
    if ret_video_num != 0:
        ret_isVideoFolder = True

    subLists = []
    errorFolderList = []
    for folderPath in list["folder"]:
        try:
            subList, isLeaf, isVideoFolder, video_num, videoSizeTotal = listUp(folderPath, lastList)
            subLists.append(subList)

            data = str(videoSizeTotal) + "," + str(video_num) + ","
            if isLeaf == True:
                 data = data + "Leaf" + ","
            else:
                 data = data + "Not" + ","

            if isVideoFolder == True:
                data = data + "VideoFolder"
            else:
                data = data + "Not"
            list["folder"][folderPath] = data
        # except Exception as e:
        except:
            # folder path is not a folder or it has some error
            # errorFolderList.append(folderPath + "," + str(e))
            # dprint(e)
            errorFolderList.append(folderPath + ",Temp")

    for subList in subLists:
        list = mergeList(list, subList)

    for msg in errorFolderList:
        errorFolderData = msg.split(",")
        del list["folder"][errorFolderData[0]]
        list["unknown"][errorFolderData[0]] = True

    return list, ret_isLeaf, ret_isVideoFolder, ret_video_num, ret_videoSizeTotal


def sizeChecker(videoSize, videoFlag):
    for key, value in videoSize.items():
        filePath = key
        # fileSize = os.path.getsize(filePath)
        fileSize = os.stat(filePath).st_size
        if value == fileSize:
            videoFlag[key] = True
        else:
            videoSize[key] = fileSize
    return videoSize, videoFlag


def usableCheck(path):
    flag = True
    try:
        f = open(path, "r+")
        flag = True
        f.close()
    except:
        flag = False

    return flag


def usableChecker(videoList):
    for key, value in videoList.items():
        filePath = key
        isUsable, size = value.split(",")
        if isUsable == False:
            isUsable = usableCheck(filePath)

        videoList[filePath] = str(isUsable) + "," + str(size)
    return videoList


def popForEncoding(videoList, q):
    for key, value in videoList.items():
        filePath = key
        size = value

        for setDataWord in setDataWords:
            if setDataWord in filePath:
                # video from Set data skip
                continue
        q.qPush(_qNameEncVibe, filePath)
        q.qPush(_qNameEncPreview, filePath)
    return videoList


def listReader(root=""):
    # result reading
    list = {}
    if len(list) == 0:
        for type in extKeys:
            list[type] = {}
            try:
                f = open(os.path.join(root, _ResultName + "_" + type + ".txt"), "r+")
            except:
                dprint("there is no " + type + " list")
                continue
            if type == "video":
                lines = f.readlines()
                for line in lines:
                    try:
                        line = line.replace("\n", "")
                        if type == "video":
                            fileSize = line[(line.rfind(","))+1:]
                            path = line[:(line.rfind(","))]
                            data = fileSize
                            list[type][path] = data
                        # elif type == "image":
                        #     path, imageNum = line.split(",")
                        #     list[type][path] = imageNum
                        # elif type == "unknown":
                    except Exception as e:
                        dprint("error is occurred, error msg : " + str(e))

    return list


def listWrite(list, new_items, q, root=""):
    # result writing
    for item in list.items():
        type = item[0]

        if new_items[type] == 0:
            # there is no new item, skip list writing
            continue
        date = datetime.datetime.now().strftime('%y-%m-%d_')
        # reset old list
        f = open(os.path.join(root, date + _ResultName + "_" + type + ".txt"), "w+")
        f.close()
        for key_path in item[1]:
            data = item[1][key_path]
            if type == "video":
                data = key_path + "," + str(data)
            elif type == "image":
               data = key_path + "," + str(data)
            elif type == "folder":
                folderSize, videoNum, leafType, folderType = data.split(",")
                if folderType == "VideoFolder":
                    data = key_path
                    data = data + "," + str(folderSize)
                    data = data + "," + str(videoNum)
                else:
                    data = ""
            else:
                data = key_path

            if data != "":
                f = open(os.path.join(root, _ResultName + "_" + type + ".txt"), "a+")
                f.write(data + '\n')
                f.close()


def listUpdate(oldQ):
    return -1


def watcher(path, root=""):
    # initialize
    dprint(logging=False)
    ts = TimeStamper()
    total_cost = 0

    # reading the latest list
    ts.lap()
    latestList = listReader(root)
    lap = ts.lap()
    dprint("list reading complete, computational cost (sec)  : " + str(lap))
    total_cost = total_cost + lap

    # asset list up in path
    ts.lap()
    new_list, isLeaf, _, _, totalSize = listUp(path, latestList)
    lap = ts.lap()
    dprint("list up complete, computational cost (sec)  : " + str(lap))

    new_items = {}
    for item in new_list.items():
       new_items[str(item[0])] = len(item[1])

    total_cost = total_cost + lap

    # merge new list and old list
    ts.lap()
    merged_list = mergeList(latestList, new_list)
    lap = ts.lap()
    dprint("list merging complete, computational cost (sec) : " + str(lap))
    total_cost = total_cost + lap

    # merged list writing
    ts.lap()
    listWrite(merged_list, new_items, q, root)
    lap = ts.lap()
    dprint("result writing complete, computational cost (sec) : " + str(lap))
    total_cost = total_cost + lap

    # make video q for encoding from new video list
    ts.lap()
    popForEncoding(new_list["video"], q)
    lap = ts.lap()
    dprint("pop for encoding complete, computational cost (sec) : " + str(lap))
    total_cost = total_cost + lap

    dprint(logging=False)
    dprint("watcher job complete, total computational cost (sec) : " + str(total_cost))
    for item in merged_list.items():
        dprint(str(item[0]) + " number : " + str(len(item[1])) + "(+" + str(new_items[item[0]]) + ")")
    dprint(logging=False)


if __name__ == '__main__':
    dprint("AssetManager.py main start")

    ####################################################################################################################
    # Asset manager

    # initialize
    q = Procq()

    parser = argparse.ArgumentParser()
    ## common options
    parser.add_argument('--path', type=str, default='', help='target folder path')
    # parser.add_argument('--path', type=str, default='E:/test')
    parser.add_argument('--debug_mode', type=bool, default=False, help='True, False')

    args = parser.parse_args()
    root = q.qGetNasRoot()
    if root == '':
        dprint(
            "Cannot find NAS drive!! please check your server or input path for 'Asset manager' processing by \"--path [PATH]\"")
        raise Exception("Cannot run Asset manager")
    else:
        root = root + _def.NAS_DEFAULT_ROOT
    if args.path == '':
        path = root + ":\\"
    else:
        path = args.path

    ####################################################################################################################
    # AssetManager Test Codes

    ## heavy image files sample 477GB
    # path = r"Y:\Gozilla vs. Kong\scene01\apex_r3_2d_v060424_200706_05jk_g_xyz\4096x1716"

    ## files and folders sample
    # path = r"V:\0_Dataset\0_Cine_Assets\IslandPlaza\BROLL\BFM\PKG-201112VFXSubmissions"

    ## asset sample (parts of Island Plaza)
    # path = "V:\\"

    ## large file tgest
    # path = r"V:\0_Dataset\0_Cine_Assets\IslandPlaza\BROLL\Pop_Up\PKG - ORPHEUM - POP-UP - Pt 1\ORPHEUM_POP-UP - VFX Plates Aerial - POP+BFM\test"

    ## sample root
    # path = r"Z:\015_Summerbreak"
    path = r"Z:\0_Dataset\0_Cine_Assets"
    ####################################################################################################################

    loopCnt = 0
    try:
        while True:
            try:
                loopCnt = loopCnt + 1
                # ordinal number suffix selection
                if loopCnt == 1:
                    suffix = "st"
                elif loopCnt == 2:
                    suffix = "nd"
                elif loopCnt == 3:
                    suffix = "rd"
                else:
                    suffix = "th"

                dprint(logging=False)
                dprint(str(loopCnt) + suffix + " watch start")
                watcher(path, root)
            except Exception as e:
                dprint(str(loopCnt) + suffix + " watch fail, because of below error")
                dprint(e)

            dprint("watcher sleep while " + str(_loopFq) + " secs")
            time.sleep(_loopFq)
    except KeyboardInterrupt as e:
        dprint(str(loopCnt) + suffix + " watch canceled by keyboard interruption")

    ####################################################################################################################