import cv2 as cv2
import os, glob

input_path = 'data/fighter2_mask'
output_path = 'data/fighter2_1920_mask/'
image_names = glob.glob(os.path.join(input_path, "*"))

os.makedirs(output_path, exist_ok=True)

for i, image_name in enumerate(image_names):
    src = cv2.imread(image_name, cv2.IMREAD_COLOR)
    # src = cv2.cvtColor(src, cv2.COLOR_BGR2RGB)
    res = cv2.resize(src, dsize=(1920, 1024), interpolation=cv2.INTER_CUBIC)


    name = os.path.basename(image_name)
    print(name)
    cv2.imwrite(output_path + name, res)