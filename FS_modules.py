import glob
import shutil
import time
import os
import dlib
from shutil import copyfile
import numpy as np

from FusionSegmentation.modules.cuda_module import cuda_check as get_gpu
from FusionSegmentation.modules import deepface_module as dfm
from FusionSegmentation.modules import enhance_module as em
from FusionSegmentation.modules import instance_module as im

from videoCtrl import Encoding
from histogramCtrl import histExtract
from placeTagCtrl import getPlaceTag, savaPlaceTag

from qCtrl import Procq
from Definitions_for_VIBESERVICE import QNAME_DEF_INSTANCESEG_FOR_VIBE, QNAME_DEF_FACEFEATURE_FOR_VIBE, QNAME_DEF_FACEMATCHING_FOR_VIBE, NAS_DEFAULT_ROOT, DETECTION_RESULT_FOLDERNAME, QNAME_DEF_HISTOGRAM_FEAT_FOR_VIBE, QNAME_DEF_ENCODING_FOR_VIBE
from Logger import dprint
import commonUtil as util

# from FusionSegmentation.modules import face_module as fm
# from FusionSegmentation.modules import vos_module as vos
# from Util.brollCtrl import genBrollQ, makeBrollSeq

q = Procq()
root = q.qGetNasRoot()
rootPath = root + NAS_DEFAULT_ROOT + '\\' + DETECTION_RESULT_FOLDERNAME
projectRoot = os.path.abspath(os.curdir)

image_file_type = "png"


def run_instanceSeg(scene_path, outputPath, cuda_device=0):
    # scene_path = q.qPop(QNAME_DEF_INSTANCESEG_FOR_VIBE)
    # scene_path = r"D:\TEST_SET\Bohemian_848"

    while scene_path != -1:
        print("Scene Path ", scene_path)
        img_cnt = len(glob.glob(os.path.join(scene_path, "*.%s" %image_file_type)))
        if img_cnt != 0:
            savePath = outputPath + "/" + os.path.basename(scene_path)
            os.makedirs(savePath, exist_ok=True)
            # print("# Save Path : ", savePath)

            # RUN instance module
            print("# Run Instance Segmentation ...")
            # selected_classes = ['person', 'car', ...] -> str
            # dynamic = True -> bool
            selected_classes = ['person']
            dynamic = True
            gpu_id = get_gpu(cuda_device=cuda_device)

            im_result_path = savePath

            try:
                im.main(scene_path, savePath, selected_classes, dynamic, gpu_id)
                # q.qPush("FeatureExtract", im_result_path)  # FeatureExtract 모듈 내 사용
                # q.qPush("MaskEnhance", scene_path)  # MaskEnhance 모듈 내 사용
                # q.qPush(QNAME_DEF_HISTOGRAM_FEAT_FOR_VIBE, scene_path)  # Histogram Feature 모듈 내 사용
                return savePath
            except MemoryError:  # Memory Load 에러 발생 시, failcase 등록
                q.qPush("BigSize_failcase", im_result_path)
                # np.savetxt(savePath + "/too_many_images.txt")

                with open(savePath + "/too_many_images.txt", 'w'):
                    pass
                dprint("[Memory Error] Instance Segmentation --> This Path registered in qBigSize_failcase.txt")
                return None
            except Exception as e:
                dprint("[Error] Instance Segmentation : {}".format(e))
                return None

        else:
            dprint("[Warning] No Image Files in this Path ...")
            return None

        # scene_path = q.qPop(QNAME_DEF_INSTANCESEG_FOR_VIBE)
        scene_path = -1


def run_featureExtract(scene_path, outputPath, cuda_device=0, matchCycle=10):
    # print("Process Feature Extracting")
    # RGBA Image -> Face Detect / Pose Estimation -> Extract Feature in Face & Angle (npy format) & Face Images (npy format)

    # scene_path = q.qPop(QNAME_DEF_FACEFEATURE_FOR_VIBE)  # InstanceF (run_instanceSeg) / feature (TEST)

    # Model Load
    model_name = "Facenet512"
    model, model_name = dfm.deepface_build_model(model_name)

    # Face Detect Model Load
    face_model_name = "retinaface"
    face_model = dfm.faceDetect_build_model(face_model_name)

    # GPU Device Check
    gpu_id = get_gpu(cuda_device=cuda_device)

    # Dlib set GPU
    if gpu_id != -1:
        dlib.cuda.set_device(gpu_id)

    # 테스트 전용 ####################################
    # scenes = [r'D:\TEST_SET_RESULT\Bohemian_197']
    # for scene_path in scenes:
    # ##############################################

    cnt = None
    pass_cnt = 0
    while scene_path != -1:
        if "instance_rgba" in os.listdir(scene_path):
            cls_folders = os.listdir(scene_path + "/instance_rgba")
            for cls_folder in cls_folders:
                rgbaPath = scene_path + "/instance_rgba" + "/" + cls_folder

                # use_path = dfm.featureExtract(rgbaPath, model, model_name, gpu_id, face_model)
                # if use_path is True:
                #     q.qPush('FeatureMatching', rgbaPath)

                try:
                    use_path = dfm.featureExtract(rgbaPath, model, model_name, gpu_id)
                    if use_path is True:  # Feature 데이터가 나온 경로만 Push
                        cnt = q.qPush(QNAME_DEF_FACEMATCHING_FOR_VIBE, rgbaPath)  # "FeatureSearch" 에서 사용할 경로
                except Exception as e:
                    dprint("[Error] Feature Extract : {} \n Error Path : {}".format(e, cls_folder))


                if cnt is not None:
                    if cnt % matchCycle == 0:
                        dprint(logging=False)
                        print("### Start Feature Matching ..! ###")
                        dprint(logging=False)
                        run_featureSearch(outputPath, cuda_device=cuda_device, matchTime=cnt)
                        pass_cnt = 0
                    # else:
                    #     pass_cnt += 1
                    #
                    # if pass_cnt >= cnt:
                    #     dprint(logging=False)
                    #     print("### Start Feature Matching ..! ###")
                    #     dprint(logging=False)
                    #     run_featureSearch(outputPath, cuda_device=cuda_device, matchTime=cnt)
                    #     pass_cnt = 0
                    # else:
                    #     pass

        # scene_path = q.qPop(QNAME_DEF_FACEFEATURE_FOR_VIBE)  # InstanceF (run_instanceSeg) / feature (TEST)
        scene_path = -1


def run_featureSearch(outputPath, cuda_device=0, matchTime=10):
    print("Process Feature Searching")
    # Face Verify with Query Features <---> Face Features

    # classes_path = []
    # for t in range(matchTime):
    #     class_path = q.qPop(QNAME_DEF_FACEMATCHING_FOR_VIBE)
    #     classes_path.append(class_path)

    # Model Load
    model_name = "Facenet512"
    model, model_name = dfm.deepface_build_model(model_name)

    # GPU Device Check
    gpu_id = get_gpu(cuda_device=cuda_device)

    # 테스트용 ###########################################################################
    # classes = [r'D:\TEST_SET_RESULT\Bohemian_848_old\instance_rgba\person_0',
    #            r'D:\TEST_SET_RESULT\Bohemian_848_old\instance_rgba\person_1',
    #            r'D:\TEST_SET_RESULT\Bohemian_848_old\instance_rgba\person_2']
    #
    # for class_path in classes:
    #     dfm.run_featureSearch(class_path, outputPath, model, model_name, gpu_id, flip_mode=False)
    #####################################################################################

    class_path = q.qPop(QNAME_DEF_FACEMATCHING_FOR_VIBE)
    while class_path != -1:
        print("@@ Class Path : ", class_path)
        try:
            dfm.run_featureSearch(class_path, outputPath, model, model_name, gpu_id)
        except Exception as e:
            dprint("[ERROR] Feature Matching : {} --> \n Error Path : {}".format(e, class_path))

        class_path = q.qPop(QNAME_DEF_FACEMATCHING_FOR_VIBE)

    # for class_path in classes_path:
    #     print("@@ Class Path : ", class_path)
    #     try:
    #         dfm.run_featureSearch(class_path, outputPath, model, model_name, gpu_id)
    #     except Exception as e:
    #         dprint("[ERROR] Feature Matching : {} --> \n Error Path : {}".format(e, class_path))


def run_maskEnhance(outputPath, mode=None, cuda_device=0):
    if mode == "instance":
        scene_path = q.qPop("MaskEnhance")
    else:
        scene_path = q.qPop("VOS")

    # GPU Device Check
    gpu_id = get_gpu(cuda_device=cuda_device)

    # scene_path = r"V:\99_sample\trailer_sample\BR_div\005"

    while scene_path != -1:
        img_cnt = len(glob.glob(os.path.join(scene_path, "*.png")))
        if img_cnt != 0:
            print("# Run Mask Enhancement ...")
            savePath = outputPath + "/" + os.path.basename(scene_path)
            os.makedirs(savePath, exist_ok=True)
            print(savePath)
            # RUN Mask Enhancement
            try:
                em.main(scene_path, savePath, mode, gpu_id)
            except Exception as e:
                dprint("[Error] MaskEnhance : {} \n Error Path : {}".format(e, scene_path))
        else:
            dprint("[Error] MaskEnhance : No Image Files in this Path -> {}".format(scene_path))

        if mode == "instance":
            scene_path = q.qPop("MaskEnhance")
        else:
            scene_path = q.qPop("VOS")
        # scene_path = -1


# def run_histExtract(scene_path, outputPath, fmt=None):
#     if fmt is None:
#         fmt = "RGB"
#
#     # scene_path = q.qPop("HistFeature")
#
#     while scene_path != -1:
#         img_cnt = len(glob.glob(os.path.join(scene_path, "*.png")))
#         if img_cnt != 0:
#             print("# Run Histogram Extract ...")
#             dataPath = outputPath + "/" + os.path.basename(scene_path)
#             if 'dynamic_mask' in os.listdir(dataPath):
#                 mask_path = dataPath + "/dynamic_mask"
#             else:
#                 mask_path = None
#             try:
#                 if isinstance(fmt, list):
#                     for f in fmt:
#                         savePath = outputPath + "/histogram_feature/{}_test".format(f)
#                         os.makedirs(savePath, exist_ok=True)
#                         he.histExtract(scene_path, mask_path, savePath, fmt=f)
#                 else:
#                     savePath = outputPath + "/histogram_feature/{}".format(fmt)
#                     os.makedirs(savePath, exist_ok=True)
#                     he.histExtract(scene_path, mask_path, savePath, fmt=fmt)
#             except Exception as e:
#                 dprint("[Error] HistExtract : {} \n Error Path : {}".format(e, scene_path))
#         else:
#             dprint("[Error] HistExtract : No Image Files in this Path -> {}".format(scene_path))
#
#         # scene_path = q.qPop("HistFeature")
#         scene_path = -1


def run_histExtract(scene_path):
    while scene_path != -1:
        histExtract(scene_path)
        scene_path = -1


def run_placeTag(scene_path):
    while scene_path != -1:
        tag_result = getPlaceTag(scene_path)
        sceneName = os.path.basename(scene_path)
        savaPlaceTag(sceneName, tag_result)

        scene_path = -1


def FS_main(outputPath, device=0, matchCycle=10):
    process = True
    # scenePath = r"D:\fashion_test\devil_wear_prada\sample_1"

    # temporal code for debugging
    debugging = True

    if debugging == False:
        srcPath = q.qPop(QNAME_DEF_ENCODING_FOR_VIBE)
    else:
        srcPath = q.qRead(QNAME_DEF_ENCODING_FOR_VIBE)

    dprint("Processing -----> %s" %srcPath)

    while process is True:
        scenePath = ""
        if srcPath != -1:
            try:
                # filename, ext = os.path.splitext(os.path.basename(srcPath))
                # scenePath = os.path.join(projectRoot, filename)
                # os.makedirs(scenePath, exist_ok=True)

                cache_path = util.makeCachePath(srcPath, q.qGetVibeRoot())
                # if debugging == True:
                #     cache_path = cache_path + "_%s" %image_file_type

                dprint("Encoding start")
                # Encoding for Images
                if os.path.exists(cache_path) == False:
                    os.makedirs(cache_path, exist_ok=True)
                    Encoding(srcPath, os.path.join(cache_path, "%06d." + image_file_type))
                scenePath = cache_path
                dprint("Encoding done")

                # onlyfiles = [f for f in os.listdir(scenePath) if os.path.isfile(os.path.join(scenePath, f))]
                # dprint("image num : %d" %len(onlyfiles))

                # Instance Segmentation
                dprint("Instance Segmentation start")
                facePath = run_instanceSeg(scenePath, outputPath, cuda_device=device)
                dprint("Instance Segmentation done")

                # Feature Extraction (Face)
                dprint("Face feature extracting start")
                if facePath is not None:
                    run_featureExtract(facePath, outputPath, cuda_device=device, matchCycle=matchCycle)
                dprint("Face feature extracting done")

                # Histogram Extraction
                dprint("Histogram extracting start")
                run_histExtract(scenePath)
                dprint("Histogram extracting done")

                # Place Tag
                dprint("Place tag classification start")
                run_placeTag(scenePath)
                dprint("Place tag classification done")

            except Exception as e:
                dprint("[Error] FS_main : {}".format(e))
                # q.qPush(QNAME_DEF_ENCODING_FOR_VIBE + "_failcase", srcPath)

            if os.path.exists(scenePath):
                shutil.rmtree(scenePath)

        if debugging == False:
            srcPath = q.qPop(QNAME_DEF_ENCODING_FOR_VIBE)
        else:
            srcPath = q.qRead(QNAME_DEF_ENCODING_FOR_VIBE)

        if srcPath == -1:
            # process = False
            print("ENCODING_FOR_VIBE Queue is Empty ... ---> Wait 60 Seconds ...")
            time.sleep(60)

def hist_main(outputPath, device=0):
    process = True

    while process is True:
        try:
            # Instance Segmentation
            run_instanceSeg(outputPath, cuda_device=device)

            # Histogram Feature
            run_histExtract(outputPath, fmt=["YCbCr", "RGB"])

            # Sleep 60 Seconds
            time.sleep(60)  # Default : 60
            print("Wait 60 Seconds ...")

        except Exception as e:
            dprint("[Error] FS_main : {}".format(e))



# def run_mxf_to_image(rootPath):
#     genBrollQ(rootPath)
#     makeBrollSeq(rootPath, clone=True)


def run_vos(outputPath):
    # scene_path = q.qPop("InstanceV")
    scene_path = r"V:\99_sample\trailer_sample\img"
    outputPath = r"V:\99_sample\trailer_sample\vos_test"

    while scene_path != -1:
        img_cnt = len(glob.glob(os.path.join(scene_path, "*.png")))
        if img_cnt != 0:
            print("# Run Video Object Segmentation ...")
            savePath = outputPath + "/" + os.path.basename(scene_path)
            os.makedirs(savePath, exist_ok=True)
            print(savePath)
            # RUN VOS module
            vos.main(scene_path, savePath)
            # q.qPush("VOS", scene_path)
        else:
            print("No Image Files in this Path")
        # scene_path = q.qPop("InstanceV")
        scene_path = -1


def run_face_search(outputPath):
    scene_path = q.qPop("InstanceF")
    # scene_path = r"V:\99_sample\b-roll_sample\JW_task\trailer_instance_fixTest\bohemian"
    # scene_path = r"V:\99_sample\b-roll_sample\JW_task\BR_div"

    # DeepFace Model = "Facenet512", "SFace", "ArcFace", "Dlib", "Facenet", "VGG-Face", "OpenFace"
    model = "Facenet512"
    # dfm.main(scene_path, outputPath, model)

    # model = "face_recognition"
    # fm.main(scene_path, outputPath)

    while scene_path != -1:
        class_cnt = len(glob.glob(os.path.join(scene_path, "/instance_rgba/person*")))
        if class_cnt != 0:
            # RUN face search module
            print("# Run Face Search ...")
            # fm.main(scene_path, outputPath)
            dfm.main(scene_path, outputPath, model)
        else:
            print("No Instance Data in this Path")

        scene_path = q.qPop("InstanceF")


if __name__ == '__main__':
    # rootPath = r"V:\0_Dataset\0_Cine_Assets"
    # rootPath = r"V:\999_Vibe\VibeResult"
    #
    # outputPath = os.path.join(rootPath, "vibe_test_0928")
    # os.makedirs(outputPath, exist_ok=True)

    outputPath = rootPath

    GPU_Device = 0

    FS_main(outputPath, device=GPU_Device, matchCycle=10)

    # hist_main(outputPath, device=0)


    # 사용되는 Qtxt 정리
    # 1. qInstance.txt (For : run_instanceSeg)
    # 2. qFeatureExtract.txt (For : run_featureExtract // From : run_instanceSeg)
    # 3. qFeatureMatching.txt (for : run_featureSearch // From : run_featureExtract)


    ## MXF to Image
    # run_mxf_to_image(rootPath)

    ## Run Instance Segmentation
    # run_instanceSeg(outputPath, cuda_device=0)

    ## Run Mask Enhancement(instance)
    # run_maskEnhance(outputPath, mode="instance", cuda_device='cuda:0')

    ## Run Face Search
    # tic = time.time()
    # run_face_search(outputPath)
    # toc = time.time()
    # TIME = np.array([toc - tic])
    # print("TIME : ", toc - tic)
    # np.savetxt(outputPath + "/searching_result/TIME.txt", TIME, fmt="%s")

    # run_featureExtract(cuda_device=0)  # Image -> Face Detection -> Features for Verify (npy format)
    # run_featureSearch(outputPath, cuda_device=0)
    # # 테스트용 ###########################################################################
    # classes = np.loadtxt(r'D:\WORK_TEST\search_list.txt', dtype=str)
    #
    # for class_path in classes:
    #     dfm.run_featureSearch(class_path, outputPath, model, model_name, gpu_id, flip_mode=True)
    # #####################################################################################

    ## Run Video Object Segmentation
    # run_vos(outputPath)

    # Run Mask Enhancement(VOS)
    # run_maskEnhance(outputPath, mode="instance")

# scene_path = r"V:\99_sample\trailer_sample\bohemian"
# outputPath = r"V:\99_sample\trailer_sample\face_recog"
# fm.main(scene_path, outputPath)
