import glob
import shutil
import time
import os
import dlib
from shutil import copyfile
import numpy as np

from FusionSegmentation.modules.cuda_module import cuda_check as get_gpu
from FusionSegmentation.modules import deepface_module as dfm
# from FusionSegmentation.modules import enhance_module as em
from FusionSegmentation.modules import instance_image_module as im

from util.videoCtrl import Encoding
from util.histogramCtrl import histExtract
from util.placeTagCtrl import getPlaceTag, savaPlaceTag

from util.qCtrl import Procq
# from Definitions_for_VIBESERVICE import QNAME_DEF_INSTANCESEG_FOR_VIBE, QNAME_DEF_FACEFEATURE_FOR_VIBE, QNAME_DEF_FACEMATCHING_FOR_VIBE, NAS_DEFAULT_ROOT, DETECTION_RESULT_FOLDERNAME, QNAME_DEF_HISTOGRAM_FEAT_FOR_VIBE, QNAME_DEF_ENCODING_FOR_VIBE
from util.Definitions_for_VIBESERVICE import *
from util.Logger import dprint

# from getProjectInfo import *

q = Procq()
root = q.qGetNasRoot()
rootPath = root + NAS_DEFAULT_ROOT + '\\' + DETECTION_RESULT_FOLDERNAME
projectRoot = os.path.abspath(os.curdir)


def run_instanceSeg(scene_path, outputPath, cuda_device=0):
    while scene_path != -1:
        print("Scene Path ", scene_path)
        img_cnt = len(glob.glob(os.path.join(scene_path, "*.png")))
        if img_cnt != 0:
            # savePath = outputPath + "/" + os.path.basename(scene_path)                # 20240206 문제의 원인
            savePath = outputPath + "/" + os.path.basename(scene_path.split('\\')[-2])  # 문제를 수정해보았습니다.
            os.makedirs(savePath, exist_ok=True)

            # RUN instance module
            print("# Run Instance Segmentation ...")
            # selected_classes = ['person', 'car', ...] -> str
            # dynamic = True -> bool
            selected_classes = ['person']
            dynamic = True  # Default = True
            gpu_id = get_gpu(cuda_device=cuda_device)

            im_result_path = savePath

            # 3D View Port Check
            # TODO : 3D Viewport 인지 확인 기능 추가할 것
            viewport = True

            try:
                # im.main(scene_path, savePath, selected_classes, dynamic, gpu_id, viewport=viewport)
                im.main(scene_path, savePath, selected_classes, dynamic, gpu_id)
                return savePath
            except MemoryError:  # Memory Load 에러 발생 시, failcase 등록
                q.qPush("BigSize_failcase", im_result_path)
                with open(savePath + "/too_many_images.txt", 'w'):
                    pass
                dprint("[Memory Error] Instance Segmentation --> This Path registered in qBigSize_failcase.txt")
                return None
            except Exception as e:
                dprint("[Error] Instance Segmentation : {}".format(e))
                return None
        else:
            dprint("[Warning] No Image Files in this Path ...")
            return None

        scene_path = -1


def run_featureExtract(scene_path, outputPath, cuda_device=0, matchCycle=10):
    # print("Process Feature Extracting")
    # RGBA Image -> Face Detect / Pose Estimation -> Extract Feature in Face & Angle (npy format) & Face Images (npy format)

    # Model Load
    model_name = "Facenet512"
    model, model_name = dfm.deepface_build_model(model_name)

    # Face Detect Model Load
    face_model_name = "retinaface"
    face_model = dfm.faceDetect_build_model(face_model_name)

    # GPU Device Check
    gpu_id = get_gpu(cuda_device=cuda_device)

    # Dlib set GPU
    if gpu_id != -1:
        dlib.cuda.set_device(gpu_id)

    # 테스트 전용 ####################################
    # scenes = [r'D:\TEST_SET_RESULT\Bohemian_197']
    # for scene_path in scenes:
    # ##############################################

    cnt = None
    pass_cnt = 0
    while scene_path != -1:
        if "instance_rgba" in os.listdir(scene_path):
            cls_folders = os.listdir(scene_path + "/instance_rgba")
            for cls_folder in cls_folders:
                rgbaPath = scene_path + "/instance_rgba" + "/" + cls_folder

                try:
                    use_path = dfm.featureExtract(rgbaPath, model, model_name, gpu_id)
                    if use_path is True:  # Feature 데이터가 나온 경로만 Push
                        cnt = q.qPush(QNAME_DEF_FACEMATCHING_FOR_VIBE, rgbaPath)  # "FeatureSearch" 에서 사용할 경로
                except Exception as e:
                    dprint("[Error] Feature Extract : {} \n Error Path : {}".format(e, cls_folder))

                if cnt is not None:
                    if cnt % matchCycle == 0:
                        dprint(logging=False)
                        print("### Start Feature Matching ..! ###")
                        dprint(logging=False)
                        run_featureSearch(outputPath, cuda_device=cuda_device, matchTime=cnt)
                        pass_cnt = 0
                    else:
                        pass_cnt += 1

                    if pass_cnt >= cnt:
                        dprint(logging=False)
                        print("### Start Feature Matching ..! ###")
                        dprint(logging=False)
                        run_featureSearch(outputPath, cuda_device=cuda_device, matchTime=cnt)
                        pass_cnt = 0
                    else:
                        pass

        scene_path = -1


def run_featureSearch(outputPath, cuda_device=0, matchTime=10):
    print("Process Feature Searching")
    # Face Verify with Query Features <---> Face Features

    # Model Load
    model_name = "Facenet512"
    model, model_name = dfm.deepface_build_model(model_name)

    # GPU Device Check
    gpu_id = get_gpu(cuda_device=cuda_device)

    # 테스트용 ###########################################################################
    # classes = [r'D:\TEST_SET_RESULT\Bohemian_848_old\instance_rgba\person_0',
    #            r'D:\TEST_SET_RESULT\Bohemian_848_old\instance_rgba\person_1',
    #            r'D:\TEST_SET_RESULT\Bohemian_848_old\instance_rgba\person_2']
    #
    # for class_path in classes:
    #     dfm.run_featureSearch(class_path, outputPath, model, model_name, gpu_id, flip_mode=False)
    #####################################################################################
    # class_path = q.qPop("FeatureMatching_230918")
    class_path = q.qPop(QNAME_DEF_FACEMATCHING_FOR_VIBE)
    while class_path != -1:
        print("@@ Class Path : ", class_path)
        try:
            dfm.run_featureSearch(class_path, outputPath, model, model_name, gpu_id)
        except Exception as e:
            dprint("[ERROR] Feature Matching : {} --> \n Error Path : {}".format(e, class_path))

        class_path = q.qPop(QNAME_DEF_FACEMATCHING_FOR_VIBE)


def run_histExtract(scene_path):
    while scene_path != -1:
        histExtract(scene_path)
        scene_path = -1


def run_placeTag(scene_path):
    while scene_path != -1:
        tag_result = getPlaceTag(scene_path)
        sceneName = os.path.basename(scene_path)
        savaPlaceTag(sceneName, tag_result)

        scene_path = -1


def FS_main(outputPath, device=0, matchCycle=10):
    process = True
    # srcPath = q.qPop(QNAME_DEF_INSTANCE_SEGMENTATION)
    # srcPath = q.qRead(QNAME_DEF_INSTANCE_SEGMENTATION)
    while process is True:
        try:

            srcPath = q.qPop(QNAME_DEF_INSTANCE_SEGMENTATION)

            # project_code = getProjectCodeFromPath(srcPath)
            while srcPath == -1:
                # process = False
                print("waiting for new video asset")
                time.sleep(600)
                srcPath = q.qPop(QNAME_DEF_INSTANCE_SEGMENTATION)

            # project_code = getProjectCodeFromPath(srcPath, trailer_ok=True)
            # outputPath_with_projectCode = os.path.join(outputPath, project_code)
            outputPath_with_projectCode = outputPath

            # srcPath = os.path.join(srcPath, "frame_quarter")
            srcPath = os.path.join(srcPath, "frame_half")

            # filename, ext = os.path.splitext(os.path.basename(dstPath))
            # keyname = srcPath.split('\\')[-2]
            # scenePath = os.path.join(projectRoot, keyname)
            # os.makedirs(scenePath, exist_ok=True)
            scenePath = srcPath

            # Encoding for Images -> 20230926-임시로 셔터 내립니다. (왜냐하면 수지짱님이 이미지를 만들어둔다네용)
            # Encoding(srcPath, os.path.join(scenePath, "%06d.png"))

            # Instance Segmentation
            facePath = run_instanceSeg(scenePath, outputPath_with_projectCode, cuda_device=device)
            # facePath를 모아둘 txt를 만들어야합니다. (예) qFacePath.txt)

            # Feature Extraction (Face) -> 20230926-임시로 셔터 내립니다.
            # if facePath is not None:
            #     run_featureExtract(facePath, outputPath, cuda_device=device, matchCycle=matchCycle)

            # Histogram Extraction  -> 20230926-임시로 셔터 내립니다.
            # run_histExtract(scenePath)

            # Place Tag  -> 20230926-임시로 셔터 내립니다.
            # run_placeTag(scenePath)

            # process = False

        except Exception as e:
            dprint("[Error] FS_main : {}".format(e))
            # q.qPush(QNAME_DEF_ENCODING_FOR_VIBE + "_failcase", srcPath)
        # shutil.rmtree(scenePath)


if __name__ == '__main__':
    outputPath = rootPath
    GPU_Device = 0  # CUDA:1
    # print(outputPath)
    FS_main(outputPath, device=GPU_Device, matchCycle=10)
    # run_featureSearch(outputPath, cuda_device=GPU_Device)