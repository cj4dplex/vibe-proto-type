import os
import time
import socket
import sys
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
import Util.timeStamp as timeStamp
from Util.Logger import dprint, setRoot
import datetime

sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
from Util.Definitions_for_VIBESERVICE import _DEBUG_MODE, NAS_CHECKER, NAS_DEFAULT_ROOT

_debug = _DEBUG_MODE

# _serverRoot_path = r"\\192.168.0.18\vibe_data\99_ComInfo"
_serverRoot_path = r"c:"
_serverInfoFileName = "serverInfo.txt"
_defaultServerInfoPath = _serverRoot_path+"\\"+_serverInfoFileName
_nasRoot = r"v"
_trial_limitation = 100
_errorLogName = 'errorLog'

_qDataFolder = "qData"

class Procq:
    def __init__(self, serverInfoPath=_defaultServerInfoPath):
        print(sys.meta_path)
        self._qPreFix = "q"
        self._qPostFix = ".txt"
        self._qDataFolder = _qDataFolder
        self._bPopReady = False
        self._qPoppedData = {}

        # serverInfo = self._qGetServerInfo(serverInfoPath)
        serverInfo = self._qGetServerInfo(serverInfoPath)

        self._qServerInfo = serverInfo
        self._qNasRoot = self._qServerInfo["root"]
        self._qRoot = self._qNasRoot + NAS_DEFAULT_ROOT
        self._qDataFolderPath = self._qRoot + "\\" + self._qDataFolder
        os.makedirs(self._qDataFolderPath, exist_ok=True)

        self._errorLogName = _errorLogName
        ip = socket.gethostbyname(socket.gethostname())
        self._qServerId = ip[(ip.rfind('.'))+1:]
        self._failQOption = "other" # to [first] / to [end] / to [other]

        self.ts = timeStamp.TimeStamper()
        self._latestPopTime = self.ts.getTimeStr()

        self._qCurProcqName = self._qServerId + "_currProcQ"

        oldQdata = self.qPop(self._qCurProcqName)
        if oldQdata != -1:
            dprint("Fail case from this server is found\ninfo : " + oldQdata)
            oldQdata = oldQdata.split(",")
            if self._failQOption == "end":
                self.qPush(oldQdata[0], oldQdata[1])
            elif self._failQOption == "first":
                self.qInterrupt(oldQdata[0], oldQdata[1])
            elif self._failQOption == "other":
                self.qPush(oldQdata[0] + "_failCase", oldQdata[1])
            else:
                dprint("wrong fail Q control option\nyour input : " + self._failQOption + "\noptions : first / end / other")

    def _qOpen(self, qName, type):
        trial = _trial_limitation
        filepath = self._qMakePath(qName)
        while trial:
            trial = trial - 1
            try:
                if os.path.exists(filepath) == True:
                    f = open(filepath, type)
                    trial = 0
                else:
                    dprint("cannot find qFile, Generate \"%s%s%s\"" % (self._qPreFix, qName, self._qPostFix), level="warning")
                    self.qGen(qName)
            except:
                if trial >= 0:
                    dprint("file name : %s, wait for read... trial : %d" % (filepath, (_trial_limitation - trial)))
                    time.sleep(0.1)
                else:
                    print("cannot open q")
                    break

        return f

    def _qMakePath(self, qName):
        if self._qDataFolderPath[-1] == ':':
            path = self._qDataFolderPath + '\\' + self._qDataFolder + os.path.join(self._qPreFix + qName + self._qPostFix)
        else:
            path = os.path.join(self._qDataFolderPath, self._qPreFix + qName + self._qPostFix)
        return path

    def _qMakeData(self, list):
        return -1

    def qGetNasRoot(self):
        return self._qNasRoot

    def qGetRoot(self):
        return self._qRoot

    def _qMakeServerInfo(self):
        serverInfo = {}

        # find NAS drive
        drive = 'c'
        while ord(drive) != ord('z') + 1:
            flag = os.path.exists(drive + NAS_CHECKER)
            if flag == True:
                break
            else:
                drive = chr(ord(drive) + 1)
        if ord(drive) == ord('z') + 1:
            serverInfo["root"] = ""
        else:
            serverInfo["root"] = drive

        try:
            with open(_defaultServerInfoPath, 'w+') as f:
                for item in serverInfo.items():
                    info = item[0] + "," + item[1]
                    f.write(info + '\n')
        except Exception as e:
            dprint("Cannot generate Serverinfo, because of below error")
            dprint(e)

        return serverInfo

    def _qGetServerInfo(self, serverInfoPath):
        serverInfo = {}
        try:
            f = open(serverInfoPath, 'r')
            for line in f.readlines():
                line = line.replace('\n', "")
                info = line.split(',')
                serverInfo[info[0]] = info[1]

            # NAS root info check
            root = serverInfo["root"]
            dprint("NAS root is " + root, logging=False)

        except:
            serverInfo = self._qMakeServerInfo()

        return serverInfo

    def qGen(self, qName):
        f = open(self._qMakePath(qName), "a")
        f.close()

    def qRead(self, qName):
        f = self._qOpen(qName, "r")
        qData = f.read()
        f.close()
        return qData

    def qPop(self, qName, popBak=True, logging=True):
        f = self._qOpen(qName, "r+")
        if f == -1:
            return -1
        qData = f.readlines()
        if popBak == True:
            self.qPop(self._qCurProcqName, popBak=False, logging=False)
        if len(qData) != 0:
            data = (qData.pop(0)).replace('\n', '')
            f.close()
            f = self._qOpen(qName, "w+")
            f.write("".join(qData))
            self._latestPopTime = self.ts.getTimeStr()
            # self._qPoppedData[qName] = data

            # popped data back-up
            if popBak == True:
                self.qPush(self._qCurProcqName, qName + "," + data + "," + self._latestPopTime, logging=False)
        else:
            data = -1
        self._qPoppedData[qName] = data
        f.close()

        if logging == True:
            if data != -1:
                dprint("Data Pop succeeed, Q name : " + qName + ", data : " + data)
            else:
                dprint("All Data Popped, Q name : " + qName)

        return data

    def pPopDone(self, qName):
        if qName in self._qPoppedData:
           del(self._qPoppedData[qName])

    def qComplete(self):
        popList = []
        for item in self._qPoppedData.items():
            popList.append(item[0])
        for qName in popList:
            self.pPopDone(qName)

    def qPush(self, qName, data, logging=False):
        if type(data) != str:
            data = str(data)
        try:
            cnt = 0
            with self._qOpen(qName, "a") as f:
                # qData = f.readlines()
                # cnt = len(qData)
                if data[-1] != '\n':
                    data = data + '\n'
                f.write(data)
            with self._qOpen(qName, "r") as f:
                qData = f.readlines()
                cnt = len(qData)
            ret = cnt

            if logging == True:
                dprint("Data Push succeeed, Q name : " + qName + ", data : " + data, end="")
        except:
            ret = -1

            if logging == True:
                dprint("Data Push fail, Cannot open q file, Q name : " + qName + ", data : " + data, end="")
        return ret

    def qInterrupt(self, qName, data):
        try:
            with self._qOpen(qName, "r") as f:
                qData = f.readlines()
                if data[-1] != '\n':
                    data = data + '\n'
                qData.insert(0, data)

            with self._qOpen(qName, "w+") as f:
                f.write("".join(qData))
            dprint("Data Interrupt succeeed, Q name : " + qName + ", data : " + data)
            ret = 0
        except:
            dprint("Data Interrupt fail, Cannot open q file, Q name : " + qName + ", data : " + data)
            ret = -1
        return ret

    def logMake(self, logName, msg):
        return msg


if __name__ == '__main__':
    print("==================== qCtrl.py test code ======================")

    try:
        q = Procq()
        n = 0
        flag = True
        while flag:
            cnt = q.qPush("test", n)
            print("push : " + str(n))
            print("cnt : " + str(cnt))
            n = n + 1

            if n == 100:
                flag = False

        flag = True
        while flag:
            data = q.qPop("test")
            print("pop : " + str(data))
            time.sleep(1)
            q.qPush("test_done", data)

            if data == -1:
                flag = False
    except KeyboardInterrupt as e:
        dprint("Keyboard interrupted " + str(e))
    except RuntimeError as e:
        dprint("Runtime error occurred, error msg : " + str(e))




