import datetime
import time

debug = True

class TimeStamper:
    def __init__(self):
        # self.init
        # make init time for whole measuring of accumulated time
        self.base = time.process_time()
        self.last_lap = self.base

    def perLoopTimeAverage(self):
        if debug == True:
            print('loop time measure')

    def getTimeStr(self):
        time = '[' + datetime.datetime.now().strftime('%y-%m-%d %H:%M:%S') + '] '
        return time

    def makeStamp(self):
        stamp = '['
        time = datetime.datetime.now().strftime('%y-%m-%d %H:%M:%S')
        if debug == True:
            print('make Time-stamp')
        stamp += time
        stamp += '] '
        return stamp

    def sprint(self, msg):
        stamp = self.makeStamp()
        print(stamp, end='')
        print(msg)

    def set(self):
        self.base = time.time()

    def lap(self):
        now = time.time()
        lap = now - self.last_lap
        self.last_lap = now
        return lap

    def elapsed(self):
        now = time.time()
        return now - self.base
