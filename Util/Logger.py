import os
import Util.timeStamp as timeStamp
import sys
sys.path.append(os.path.dirname(os.path.abspath(os.path.dirname(__file__))))
from Util.Definitions_for_VIBESERVICE import _DEBUG_MODE, NAS_CHECKER, NAS_DEFAULT_ROOT

import datetime
import time
import socket


debug_level = {}
debug_level["log"] = 0
debug_level["debug"] = 1
debug_level["warning"] = 2
debug_level["error"] = 3

ts = timeStamp.TimeStamper()

if _DEBUG_MODE == True:
    print_level = debug_level["debug"]
else:
    print_level = debug_level["warning"]


def makeStamp():
    stamp = '['
    time = datetime.datetime.now().strftime('%y-%m-%d %H:%M:%S')
    stamp += time
    stamp += '] '
    return stamp


def getDateTimeForFileName():
    time = datetime.datetime.now().strftime('%y-%m-%d_%H-%M-%S')
    return time

class Logger:
    def __init__(self, rootPath=""):
        ip = socket.gethostbyname(socket.gethostname())
        self._serverId = ip[(ip.rfind('.')) + 1:]

        if rootPath == "":
            # find NAS drive

            drive = 'c'
            while ord(drive) != ord('z') + 1:
                flag = os.path.exists(drive + NAS_CHECKER)
                if flag == True:
                    break
                else:
                    drive = chr(ord(drive) + 1)

            if ord(drive) == ord('z') + 1:
                drive = "c"
            self.logRoot = os.path.join(drive + NAS_DEFAULT_ROOT, "log")
            os.makedirs(self.logRoot, exist_ok=True)
        self.logTitle = "Server_" + self._serverId + r"_log_" + getDateTimeForFileName() + ".txt"

    def setRoot(self, root):
        self.logRoot = root

    def _logging(self, str, logPath=""):
        if logPath == "":
            logPath = os.path.join(self.logRoot, self.logTitle)
        else:
            # logPath has Extension?
            if logPath[-4:-3] != ".":
                logPath = os.path.join(logPath, self.logTitle)

        with open(logPath, "a+") as f:
            f.write(str + "\n")

logger = Logger()

def setRoot(root):
    logger.setRoot(root)


def dprint(msg="", end="\n", level="debug", logging=True, progress_mode=False):
    try:
        level = debug_level[level]
    except:
        level = debug_level["debug"]

    timeStamp = makeStamp()

    if msg == "":
        # make border line
        msg = "========================================================================================================"
    else:
        msg = timeStamp + str(msg)

    if level <= print_level:
        if progress_mode == True:
            print("\r" + msg, end="")
        else:
            print(msg, end=end)

    if logging:
        logger._logging(timeStamp + str(msg))

