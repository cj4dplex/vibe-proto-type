import paramiko

try:
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy)
    ssh.connect("192.168.0.30", username="cgv", password="cgv")

    print('ssh connected')

    stdin, stdout, stderr = ssh.exec_command('dir')

    data = stdout.read()

    print(str(data))

    decoded = data.decode('utf-8')

    for i in decoded:
        re = str(i).replace('\n','')
        print(re)

    ssh.close()

except Exception as err:
    print(err)