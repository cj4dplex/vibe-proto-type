import os
from os.path import isfile, join, splitext
import shutil
from qCtrl import Procq
_samplePathFile = "samplePath.txt"

_debug = True
_log_print = False
_errorLog_fileName = "errorLog.txt"
_reCombination_folderName = "_reComb"
_faceRecog_infoFolderName = "searching_result"
_faceRecog_infoFileName = "path_result.csv"

_unknown = 0

_assetTypeList = ["broll", "3d", "dcdm_folder", "dcdm_image"]
_assetExtList = {}
_assetExtList["broll"] = [".mov", ".mxf", ".avi"]
_assetExtList["3d"] = [".abc", ".ma", "mb"]
_assetExtList["dcdm_folder"] = ["dcdm", "DCDM"]
_assetExtList["dcdm_image"] = [".tif", ".png"]

_serverName = "test"
q = Procq(_serverName)


def _log_print(msg, isErrorMsg=False):
    if _log_print == True:
        print(msg)


def _IsThisTargetFile(ext, targetFileExts=[], opt='image'):
    if len(targetFileExts) == 0:
        if opt == 'video':
            targetFileExts = {".mxf", ".mov"}
        elif opt == 'image':
            targetFileExts = {".png", ".jpg", ".bmp"}
        else:
            print("wrong opt is inputted, (opt : image / vidio)\nyour input : %s" %opt)

    flag = False
    for targetExt in targetFileExts:
        if ext == targetExt:
            flag = True
            break

    return flag


def _findFoldersAndFiles(path):
    files = []
    folders = []
    for f in os.listdir(path):
        if isfile(join(path, f)):
            files.append(f)
        else:
            folders.append(f)

    return files, folders


def _make_TargetFolderMap(path, folderMap, target_prefix, forbidden_words):
    onlyfolders = [f for f in os.listdir(path) if not isfile(join(path, f))]

    flag = False
    if flag == False:
        if len(onlyfolders) != 0:
            for folderName in onlyfolders:
                # forbidden words check for main folders
                forbidden_flag = False
                for forbidden_word in forbidden_words:
                    if forbidden_word in folderName:
                        forbidden_flag = True
                        break

                if forbidden_flag == False:
                    if target_prefix in folderName:
                        folderMap.append(join(path, folderName))
                    else:
                        _make_TargetFolderMap(join(path, folderName), folderMap, target_prefix, forbidden_words)

    return folderMap


def _make_targetFileMap(path, folderMap, specific_ext=[], forbidden_words=[], opt="image"):
    onlyfiles, onlyfolders = _findFoldersAndFiles(path)

    flag = False
    if len(onlyfiles) != 0:
        flag = True
        for filename in onlyfiles:
            if filename == 'Thumbs.db':
                continue

            _, ext = os.path.splitext(filename)
            flag = flag and _IsThisTargetFile(ext, specific_ext, opt)

            if flag == True:
                if _log_print:
                    print('[%03d] add target folder : %s' %(len(folderMap) + 1, path))
                folderMap.append(os.path.join(path, filename))

    if flag == False:
        if len(onlyfolders) != 0:
            for folderName in onlyfolders:
                # forbidden words check for image folder
                forbidden_flag = False
                for forbidden_word in forbidden_words:
                    if forbidden_word in folderName:
                        forbidden_flag = True
                        break
                if forbidden_flag == False:
                    _make_targetFileMap(join(path, folderName), folderMap)

    return folderMap


def _make_folderContainTargetFileMap(path, folderMap, specific_ext=[], forbidden_words=[]):
    onlyfiles, onlyfolders = _findFoldersAndFiles(path)

    flag = False
    if len(onlyfiles) != 0:
        flag = True
        for filename in onlyfiles:
            if filename == 'Thumbs.db':
                continue
            if filename == _errorLog_fileName:
                try:
                    gather(path, True)
                    os.remove(join(path, filename))
                    flag = True
                    if _log_print:
                        print("occurred error is recovered, error log file will removed")
                except:
                    print("\n\ncannot recover occurred error, error is remain\n\n")
                    break
            else:
                _, ext = os.path.splitext(filename)
                flag = flag and _IsThisTargetFile(ext, specific_ext)

            if flag == True:
                break
        if flag == True:
            if _log_print:
                print('[%03d] add target folder : %s' % (len(folderMap) + 1, path))
            folderMap.append(path)

    if flag == False:
        if len(onlyfolders) != 0:
            for folderName in onlyfolders:
                # forbidden words check for image folder
                forbidden_flag = False
                for forbidden_word in forbidden_words:
                    if forbidden_word in folderName:
                        forbidden_flag = True
                        break
                if forbidden_flag == False:
                    _make_folderContainTargetFileMap(join(path, folderName), folderMap, specific_ext, forbidden_words)

    return folderMap


def _make_targetFolderMap(path, folderMap, specific_words=[], isPrefix = False, forbidden_words=[]):
    _, onlyfolders = _findFoldersAndFiles(path)

    if len(onlyfolders) != 0:
        for folderName in onlyfolders:
            # forbidden words check for main folders
            forbidden_flag = False
            for forbidden_word in forbidden_words:
                if forbidden_word in folderName:
                    forbidden_flag = True
                    break
            if forbidden_flag == False:
                targetFolder_flag = False
                for targetFolder_word in specific_words:
                    if isPrefix == False:
                        if targetFolder_word in folderName:
                            targetFolder_flag = True
                            break
                    else:
                        if folderName.startswith(targetFolder_word) == True:
                            targetFolder_flag = True
                            break

                if targetFolder_flag == True:
                    folderMap.append(os.path.join(path, folderName))
                else:
                    _make_targetFolderMap(join(path, folderName), folderMap, specific_words, isPrefix, forbidden_words)

    return folderMap


def _find_targetFolder(root_path, specific_words=[], isPrefix = False, forbidden_words=[]):
    targetPath = ""
    _, onlyfolders = _findFoldersAndFiles(root_path)

    if len(onlyfolders) != 0:
        for folderName in onlyfolders:
            # forbidden words check for main folders
            forbidden_flag = False
            for forbidden_word in forbidden_words:
                if forbidden_word in folderName:
                    forbidden_flag = True
                    break
            if forbidden_flag == False:
                targetFolder_flag = False
                for targetFolder_word in specific_words:
                    if isPrefix == False:
                        if targetFolder_word in folderName:
                            targetFolder_flag = True
                            break
                    else:
                        if folderName.startswith(targetFolder_word) == True:
                            targetFolder_flag = True
                            break

                if targetFolder_flag == True:
                    targetPath = os.path.join(root_path, folderName)
                    break

    if targetPath == "" and len(onlyfolders) != 0:
        # start for find root path
        for folderName in onlyfolders:
            # forbidden words check for main folders
            forbidden_flag = False
            for forbidden_word in forbidden_words:
                if forbidden_word in folderName:
                    forbidden_flag = True
                    break
            if forbidden_flag == False:
                targetFolder_flag = False
                for targetFolder_word in specific_words:
                    if isPrefix == False:
                        if targetFolder_word in folderName:
                            targetFolder_flag = True
                            break
                    else:
                        if folderName.startswith(targetFolder_word) == True:
                            targetFolder_flag = True
                            break

                if targetFolder_flag == True:
                    targetPath = os.path.join(root_path, folderName)
                else:
                    _find_targetFolder(join(root_path, folderName), specific_words, isPrefix, forbidden_words)

    return targetPath


def _make_bRoll_map(path, folderMap, bRollExtList=[], forbidden_words=[]):
    onlyfiles, onlyfolders = _findFoldersAndFiles(path)

    flag = False
    if len(onlyfiles) != 0:
        flag = True
        for filename in onlyfiles:
            if filename == 'Thumbs.db':
                continue
            if filename == _errorLog_fileName:
                try:
                    gather(path, True)
                    os.remove(join(path, filename))
                    flag = True
                    if _log_print:
                        print("occurred error is recovered, error log file will removed")
                except:
                    print("\n\ncannot recover occurred error, error is remain\n\n")
                    break
            else:
                _, ext = os.path.splitext(filename)
                flag = flag and _IsThisTargetFile(ext, specific_ext)

            if flag == True:
                break
        if flag == True:
            if _log_print:
                print('[%03d] add target folder : %s' % (len(folderMap) + 1, path))
            folderMap.append(path)

    if flag == False:
        if len(onlyfolders) != 0:
            for folderName in onlyfolders:
                # forbidden words check for image folder
                forbidden_flag = False
                for forbidden_word in forbidden_words:
                    if forbidden_word in folderName:
                        forbidden_flag = True
                        break
                if forbidden_flag == False:
                    _make_folderContainTargetFileMap(join(path, folderName), folderMap, specific_ext, forbidden_words)

    return folderMap



def gather(path, delfolder):
    start_num = 0

    onlyFolders = [f for f in os.listdir(path) if not os.path.isfile(os.path.join(path, f))]
    for n in range(0, len(onlyFolders)):
        folderPath = os.path.join(path, onlyFolders[n])
        onlyFiles = [f for f in os.listdir(folderPath) if os.path.isfile(os.path.join(folderPath, f))]

        for m in range(0, len(onlyFiles)):
            filePath = os.path.join(folderPath, onlyFiles[m])
            _, ext = os.path.splitext(filePath)
            if ext == '.py':
                continue

            shutil.move(filePath, os.path.join(path, onlyFiles[m]))
            print("\rcopy {} to {}".format(filePath, (os.path.join(path, onlyFiles[m]))), end='')

        if delfolder == True:
            os.rmdir(folderPath)


def gather_to_parent(folderMap, delfolder):
    parent_list = []

    for folderPath in folderMap:
        try:
            files, _ = _findFoldersAndFiles(folderPath)

            for fileName in files:
                _, ext = os.path.splitext(fileName)
                if _IsThisTargetFile(ext, opt='image') == False:
                    continue
                src = os.path.join(folderPath, fileName)
                parent_path = os.path.join(folderPath, "..\\")
                parent_path = os.path.abspath(parent_path)
                dst = os.path.join(parent_path, fileName)
                shutil.move(src, dst)
                if _log_print:
                    print("\rcopy {} to {}".format(src, dst), end='')

                if parent_path not in parent_list:
                    parent_list.append(parent_path)

            if _log_print:
                print("\r= move back to %s Complete" % parent_path, end='')

            if delfolder == True:
                os.rmdir(folderPath)
        except:
            print("error is occurred, folder name : %s, skip this folder" % folderPath)
            logPath = os.path.join(folderPath, "..\\", _errorLog_fileName)
            f = open(logPath, 'w')
            f.close()

            # os.rename(folderPath, folderPath + "_error")
            # print("error occurred folder marked by rename (folder name + \"_error\")" % folderPath)

    return parent_list


divideFolder_prefix = "div"
def split(path, limit=60, is_unzipsize=True):
    start_num = 0

    onlyFiles = [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f))]

    accumulatedNum = 0
    nFolders = 0
    holdPath = ""
    holdFileName = ""
    holdByte = 0

    if len(onlyFiles) <= limit:
        return -1
    for n in range(0, len(onlyFiles)):
        if accumulatedNum == 0:
            # make new folder
            targetFolderPath = os.path.join(path, divideFolder_prefix + '%03d' % (start_num + nFolders))
            while (os.path.exists(targetFolderPath)):
                print("\n{} is already exist!!".format(targetFolderPath))
                nFolders = nFolders + 1
                targetFolderPath = os.path.join(path, divideFolder_prefix + '%03d' % (start_num + nFolders))
            os.makedirs(targetFolderPath, exist_ok=True)
            nFolders = nFolders + 1

            if holdPath != "":
                shutil.move(holdPath, os.path.join(targetFolderPath, holdFileName))
                if _log_print:
                    print("\rcopy {} to {}".format(filePath, (os.path.join(targetFolderPath, holdFileName))), end='')

                accumulatedNum = holdByte

        filePath = os.path.join(path, onlyFiles[n])

        _, ext = os.path.splitext(filePath)
        if _IsThisTargetFile(ext,opt='image') == False:
            continue
        byte = 1

        if byte + accumulatedNum > limit:
            if byte > limit:
                print("{} is too big to copy for the current limitation (limit : {} GB, file size : {:.3f} GB)".format(
                    onlyFiles[n], limit / (1024.0 * 1024.0 * 1024.0), byte / (1024.0 * 1024.0 * 1024.0)))

            holdPath = filePath
            holdFileName = onlyFiles[n]
            holdByte = byte

            accumulatedNum = 0
        else:
            shutil.move(filePath, os.path.join(targetFolderPath, onlyFiles[n]))
            print("\rcopy {} to {}".format(filePath, (os.path.join(targetFolderPath, onlyFiles[n]))), end='')

            accumulatedNum = accumulatedNum + byte

    if (start_num + nFolders) == 1:
        # if there is the only folder, don't make divided folder
        gather(path, True)
        is_divided = False
    else:
        is_divided = True

    return is_divided


def _make_listToPath(list):
    path = ""
    for word in list:
        if ' ' in word:
            word = '\\\\"' + word + '\"\\'
            path = path + word
        else:
            path = join(path, word)

        if ':' in word:
            path = path + '\\'
    return path


def assetWatcher():
    return -1


def genBrollQ(root, qName="bRoll", forbidden_words=[]):
    onlyfiles, onlyfolders = _findFoldersAndFiles(root)

    count = 0
    if len(onlyfiles) != 0:
        flag = True
        for filename in onlyfiles:
            _, ext = os.path.splitext(filename)
            flag = flag and _IsThisTargetFile(ext, opt="video")

            if flag == True:
                # forbidden words check for b-roll file
                forbidden_flag = False
                for forbidden_word in forbidden_words:
                    if forbidden_word in filename:
                        forbidden_flag = True
                        break
                if forbidden_flag == False:
                    if _log_print:
                        print('add target file : %s\\%s' %(root, filename))
                    q.qPush(qName, join(root, filename))
                    count = count + 1

    if len(onlyfolders) != 0:
        for folderName in onlyfolders:
            # forbidden words check for image folder
            forbidden_flag = False
            for forbidden_word in forbidden_words:
                if forbidden_word in folderName:
                    forbidden_flag = True
                    break
            if forbidden_flag == False:
                genBrollQ(join(root, folderName), qName, forbidden_words)


def makeBrollSeq(root="", qName="bRoll", clone=False):
    src = q.qPop(qName)
    while src != -1:
        splitted_src = src.split('\\')
        edited_src = _make_listToPath(splitted_src)

        folderpath, filename = os.path.split(src)
        name, ext = os.path.splitext(filename)
        os.makedirs(join(folderpath, name), exist_ok=True)

        folderpath_for_command, filename = os.path.split(edited_src)

        if clone == True:
            folderpath_for_command = folderpath_for_command.replace(root + '\\', '')
            folderpath_for_command = join(root + "_seq", folderpath_for_command, name)

            os.makedirs(folderpath_for_command, exist_ok=True)

        dst = join(folderpath_for_command, "%06d.png")



        command = "ffmpeg -i %s %s" % (edited_src, dst)
        os.system(command)
        q.qPush("FusionSegmentation", folderpath_for_command)
        src = q.qPop(qName)

    return -1


def makeImagesFromVideo(path, unit=0,specific_ext=[]):
    folderMap = []

    if len(specific_ext) == 0:
        folderMap = _make_targetFileMap(path, folderMap, opt="video")
    else:
        folderMap = _make_targetFileMap(path, folderMap, specific_ext)

    splitTarget = []

    for src in folderMap:
        splitted_src = src.split('\\')
        edited_src = _make_listToPath(splitted_src)

        folderpath, filename = os.path.split(src)
        name, ext = os.path.splitext(filename)
        os.makedirs(join(folderpath, name), exist_ok=True)
        splitTarget.append(join(folderpath, name))

        folderpath_for_command, filename = os.path.split(edited_src)
        dst = join(folderpath_for_command, name, "%06d.png")
        command = "ffmpeg -i %s %s" %(edited_src, dst)
        os.system(command)

    if unit != 0:
        for src in splitTarget:
            split(src, unit)


def _reDiv(path, unit=60):
    folderMap = []
    _make_targetFolderMap(path, folderMap, {"div"}, True, {_reCombination_folderName})
    splitTarget_list = gather_to_parent(folderMap, True)
    for src in splitTarget_list:
        split(src, unit)


def _reDiv_for_remain(path, unit=60):
    folderMap = []
    splitTarget_list = _make_folderContainTargetFileMap(path, folderMap, {".png"}, {"div", "_error"})
    # splitTarget_list = gather_to_parent(folderMap, True)
    for src in splitTarget_list:
        split(src, unit)


def _readRecogData(infoFolderPath):
    data = []
    try:
        f = open(os.path.join(infoFolderPath, _faceRecog_infoFileName), "r")
        rawData = f.read()
    except:
        print("cannot read recognition data file, please check below path\n%s" %join(infoFolderPath, _faceRecog_infoFileName))
        return -1
    lines = rawData.split('\n')
    for line in lines:
        if len(line) != 0:
            data.append(line.split(','))

    return data


def _makeRecogInfo(data):
    info = {}

    for line in data:
        key = line[0]
        if key not in info:
            info[key] = []
        info[key].append(line[1])

    return info


def _make_gifFromSegmentation(targetMap, dst_root):
    # integrating by JW's function
    return -1


def _recombination(info, root_path):
    gif_targetMap = []


    #find root path
    root_path_from_info = ""
    for item in info.items():
        value = item[1]
        for path in value:
            path = path.replace('\\', '/')
            path = path.split('/')

            if root_path_from_info == "":
                root_path_from_info = path

            for i in range(len(root_path_from_info)-1, -1, -1):
                unit = root_path_from_info[i]
                if unit not in path:
                    root_path_from_info.remove(unit)
            if 'rgba' in root_path_from_info:
                root_path_from_info.remove('rgba')
            if 'mask' in root_path_from_info:
                root_path_from_info.remove('mask')

    root_path_from_info = _make_listToPath(root_path_from_info)

    for item in info.items():
        label = item[0]
        value = item[1]
        print(label)
        print(value)

        for path in value:
            # make source data path
            path = path.replace('\\', '/')
            src_path = path.split('/')

            for i in range(len(src_path) - 1, -1, -1):
                unit = src_path[i]
                if unit in root_path_from_info:
                    src_path.remove(unit)
            src_keys = src_path
            src_path = os.path.join(root_path, _make_listToPath(src_path))

            # make source key
            for i in range(len(src_keys)-1, -1, -1):
                unit = src_keys[i]
                if unit == 'rgba':
                    src_keys.remove(unit)
                if unit == 'mask':
                    src_keys.remove(unit)

            src_key = ""
            for key in src_keys:
                src_key = src_key + key + "_"
            src_key = src_key[0:-1]

            # make destination path
            ## destination path rule : root \ recombination folder name \ label \ source key

            dst_path = os.path.join(root_path, _reCombination_folderName, label, src_key)
            # os.makedirs(dst_path, exist_ok=True)

            shutil.copytree(src_path, dst_path, dirs_exist_ok=True)
            gif_targetMap.append(dst_path)
            # copy mask

            src_path = src_path.replace("rgba", "mask")
            dst_path = dst_path + "_mask"

            shutil.copytree(src_path, dst_path, dirs_exist_ok=True)

    # make GIFs
    _make_gifFromSegmentation(gif_targetMap, "..\\")

if __name__ == '__main__':
    try:
        f = open(_samplePathFile, 'r')
        path = f.read()
    except:
        print("cannot read sample path data, sample path data path : util\\%s" %_samplePathFile)
        path = r"V:\99_sample\b-roll_sample"

    unit = 60

    ## queueing test
    # q = Procq("30")
    #
    # q.qGen("test")
    # q.qPush("test", "test1")
    # data = q.qPop("test")
    # # data = q.qPop("test")
    # q.qComplete()
    # exit(-1)

    ## reDiv work
    # _reDiv(path, unit)
    # _reDiv_for_remain(path, unit)

    ## recombination folder tree
    # recomb - person# - div# - rgba / mask

    ## make images from broll (mxf) to cloning folder

    root = r"V:\99_sample\b-roll_sample"
    genBrollQ(root)
    makeBrollSeq(root, clone=True)



    # folderMap = []
    # infoFolder_path = _find_targetFolder(path, {_faceRecog_infoFolderName}, True, {_reCombination_folderName})
    # data = _readRecogData(infoFolder_path)
    #
    # if data == -1 or len(data) == 0:
    #     print("recognition data is empty")
    #     exit(-1)
    #
    # info = _makeRecogInfo(data)
    # _recombination(info, path)
    #
    # if _log_print:
    #     print("done")
