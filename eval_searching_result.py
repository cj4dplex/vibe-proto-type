import numpy as np


def convert_result(result_file):
    result_convert = []
    result_convert.append(['Frame number', 'Class'])

    cls_dict = {}
    for idx, data in enumerate(result_file):
        if idx > 0:
            cls_name = data[0]
            start_frame = int(data[5])
            end_frame = int(data[6])

            if cls_name == 'UnKnown':
                cls_name = None

            for f in range(start_frame, end_frame + 1):
                if f not in list(cls_dict.keys()):
                    cls_names = [cls_name]
                    cls_dict[f] = cls_names
                else:
                    cls_names = cls_dict[f]
                    if cls_name not in cls_names:
                        cls_names.append(cls_name)
                        cls_dict[f] = cls_names

    for frame_number in list(cls_dict.keys()):
        cls_data = cls_dict[frame_number]
        new_data = [frame_number]
        for cls in cls_data:
            new_data.append(cls)
        result_convert.append(new_data)

    return result_convert


# TODO : "수정 된 사항"
# TODO : 1. Accuracy 계산 방법 수정
# TODO :    - 맞은 개수 / GT 정답지 개수

def get_accuracy(GT_file, convert_file):
    GT = list(GT_file)
    scores = 0
    answers = 0
    for idx in range(len(convert_file)):
        if idx > 0:
            frame_number = convert_file[idx][0]

            answer = GT[frame_number][1]
            predict = convert_file[idx][1]

            answers += len(answer)

            if answer == predict:
                score = len(answer)
            else:
                cnt = 0
                for p in predict:
                    if p in answer:
                        cnt += 1
                score = cnt
            scores += score

    accuarcy = round(scores / answers * 100, 4)
    return accuarcy


def result_eval(GT_path, result_path):
    GT_file = np.loadtxt(GT_path, dtype=str, delimiter=',')
    result_file = np.loadtxt(result_path, dtype=str, delimiter=',')

    # result_file -> GT_file 형태로 Convert 함수
    convert_file = convert_result(result_file)

    # Convert 된 result_file과 GT_file 비교
    ACC = get_accuracy(GT_file, convert_file)

    print("Accuracy : ", ACC)
    print('Finish')


if __name__ == '__main__':
    GT_path = r'D:\Bohemian_GT.csv'
    result_path = r"D:\TEST_SET_RESULT\searching_result\path_result.csv"
    result_eval(GT_path, result_path)