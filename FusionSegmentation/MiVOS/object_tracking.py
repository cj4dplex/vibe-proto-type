import cv2
from interact.tracking import main_tracker


class VideoTracker():
    def __init__(self):
        self.tracker = []

    def run_on_video_tracking(self, video, labels, scores, stats, frame_idxs, debug):

        ## GET BOX INFO FUNC.
        def get_box_info(labels, scores, stats, debug):
            """
            Args:
                frame: image from video
                labels: label[1, 2, 3 ... n] in frame, which is masked
                scores: score[0.9121, 0.7326, ... n]
                stats: bounding box info[x0, x1, y0, y1], which is matched with labels

            Returns:

            """
            candidate = stats.copy()
            # print("label : ", labels)
            # print("candidate : ", candidate)
            matched_list = []
            mean_iou_result = []
            # print("Candidate : ", candidate)
            # Give life(4) to candidate
            for id in range(len(candidate)):
                score = scores[id]
                candidate[id] = [candidate[id], 4, score]

            # candidate = [[[x0, y0, x1, y1], life, score], [[x0, y0, x1, y1], life, score], ... ]
            # print("Candidate with Life : ", candidate)

            init_tracker = {}
            if len(self.tracker) == 0:
                for n, label in enumerate(labels):
                    init_tracker[n] = candidate[n]

                # init_tracker = {id: [[x0, y0, x1, y1], life], id: [[x0, y0, x1, y1], life], ...}
                # init_tracker[id] = [[x0, y0, x1, y1], life]

                self.tracker = init_tracker  # Dictionary format
                matched_list = list(init_tracker.keys())
                # box_img = draw_boundingBox(frame, init_tracker)
                # print(self.tracker)

            else:
                self.tracker, matched_list, mean_iou_result = main_tracker(candidate, self.tracker, debug)

            yield self.tracker
            yield matched_list
            yield mean_iou_result

        def get_max_element(nested_list):
            max_val = 0
            for list in nested_list:
                for element in list:
                    if max_val < element:
                        max_val = element
            return max_val

        # Counting object id in each frame, id not shown less than 24 remove id.
        def object_id_counter(id_frames):
            # print("id_frames : {}".format(id_frames))
            # max_id = max([id_frame[-1] for id_frame in id_frames])
            max_id = get_max_element(id_frames)

            all_id_frames = [item for sublist in id_frames for item in sublist]
            remove_id = []
            for id in range(max_id + 1):
                cnt = all_id_frames.count(id)
                if cnt < 24:
                    remove_id.append(id)
            # print("REMOVE ID LIST :", remove_id)
            new_id_frames = remove_obj_id(id_frames, remove_id)
            # print("new_id_frames : {}".format(new_id_frames))

            return new_id_frames

        def remove_obj_id(id_frames, remove_id):
            re_id_frames = []
            for id_frame in id_frames:
                find_element = set(id_frame) & set(remove_id)
                find_element = list(find_element)
                if len(find_element) > 0:
                    id_frame_copy = id_frame.copy()
                    for i in range(len(id_frame)):
                        if id_frame[i] in find_element:
                            id_frame_copy.remove(id_frame[i])
                            # del id_frame_copy[i]
                    re_id_frames.append(id_frame_copy)
                else:
                    re_id_frames.append(id_frame)

            return re_id_frames

        def tracked_box_counter(tracked_boxes, id_frames):
            for i, tracked_box in enumerate(tracked_boxes):
                id_frame = id_frames[i]
                tracked_box_keys = list(tracked_box.keys())
                for tracked_box_key in tracked_box_keys:
                    if tracked_box_key not in id_frame:
                        del tracked_box[tracked_box_key]

            return tracked_boxes

        def shot_div(obj_ids):
            frames_num = len(obj_ids)
            shot_cut = [0]
            not_match = []

            for i in range(frames_num - 1):
                pos_id = obj_ids[i]
                pre_id = obj_ids[i + 1]
                a = set(pos_id)
                b = set(pre_id)

                # print(pos_id , pre_id)

                if len(pos_id) > len(pre_id) and len(pos_id) == len(pre_id):
                    not_match = list(a - b)
                elif len(pos_id) < len(pre_id):
                    not_match = list(b - a)

                num_not_match = len(not_match)
                # print("frame : ", i)
                # print(not_match, num_not_match)

                if num_not_match > 0:
                    shot_cut.append(i)
                    not_match.clear()

            return shot_cut

        def shot_div_by_label(labels, obj_ids, frame_idxs):
            shot_divs = {}
            for l in labels:
                shot_div = []
                for i, obj_id in enumerate(obj_ids):
                    if l in obj_id:
                        if frame_idxs is not None:
                            idx = frame_idxs[i]
                            shot_div.append(idx)
                        else:
                            shot_div.append(i)  ## 수정 전
                shot_divs[l] = shot_div

            return shot_divs

        def track_id_box_match(tracked_box_dictionary):
            trakced_id = list(tracked_box_dictionary.keys())
            copy_tracked_box = tracked_box_dictionary.copy()
            new_tracked_box = {}
            for id in trakced_id:
                if copy_tracked_box[id][0] != [-1, -1, -1, -1] or copy_tracked_box[id][1] != 0:
                    box = copy_tracked_box[id][0]
                    score = copy_tracked_box[id][2]
                    new_tracked_box[id] = [box, score]

            return new_tracked_box

        def get_labels_in_frames(id_in_frames):
            labels = []
            for id_in_frame in id_in_frames:
                for i in id_in_frame:
                    if i not in labels:
                        labels.append(i)

            return labels

        def position_not_conner(label_box, h, w, s_mode):
            x0 = label_box[0][0]
            y0 = label_box[0][1]
            x1 = label_box[0][2]
            y1 = label_box[0][3]
            if s_mode == 'hard':
                if x0 > int(w * 0.01) and x1 < int(w * 0.99) and y1 < int(h * 0.99):
                    return True
                else:
                    return False
            elif s_mode == 'soft':
                if x0 > 0 and x1 < w and y1 < h:
                    return True
                else:
                    return False

        def get_seed_frames_sort(tracked_boxes, label_frames, label, h, w):
            from operator import itemgetter
            boxes = []
            scores = []
            for label_frame in label_frames:
                label_box = tracked_boxes[label_frame][label]
                score = label_box[1]
                # print("score : ", score)

                if position_not_conner(label_box, h, w, s_mode='hard'):
                    scores.append([label_frame, score])
                    if not scores:
                        if position_not_conner(label_box, h, w, s_mode='soft'):
                            scores.append([label_frame, score])

                if not scores:
                    scores.append([label_frame, score])
            # sorted_scores = sorted(scores.items(), key=lambda item: item[1], reverse=True)
            sorted_scores = sorted(scores, key=itemgetter(1), reverse=True)
            return sorted_scores

        def get_label_seed_frames(shot_div_frames, tracked_boxes, h, w):
            # print("tracked_boxes : ", tracked_boxes)
            # for boxes in tracked_boxes:
            #     print(boxes)

            label_seed_frames = {}
            labels = list(shot_div_frames.keys())
            for label in labels:
                label_frames = shot_div_frames[label]
                sorted_scores = get_seed_frames_sort(tracked_boxes, label_frames, label, h, w)
                label_seed_frames[label] = sorted_scores
            return label_seed_frames

        # frame_gen = self._frame_from_video(video)
        frame_gen = video

        i = 0
        obj_id_in_frames = []
        mean_iou_frames = []
        tracked_boxes = []
        h, w, c = video[0].shape

        for i, frame in enumerate(frame_gen):
            label = labels[i]
            score = scores[i]
            stat = stats[i]

            if debug:
                print("[Debug in run_on_video_tracking]")
                print("===============================================")
                print("Frame number :", i)

            tracked_box, obj_id_in_frame, frame_iou = get_box_info(label, score, stat, debug)
            obj_id_in_frames.append(obj_id_in_frame)
            mean_iou_frames.append(frame_iou)
            new_tracked_box = track_id_box_match(tracked_box)
            tracked_boxes.append(new_tracked_box)

            # yield process_predictions(frame, self.predictor(frame))

        obj_id_in_frames = object_id_counter(obj_id_in_frames)
        labels_in_frames = get_labels_in_frames(obj_id_in_frames)
        shot_div_frames = shot_div_by_label(labels_in_frames,
                                            obj_id_in_frames,
                                            None)  # shot_div_frames = [[[label id], frame0, frame1, ...], ...]
        shot_div_idxs = shot_div_by_label(labels_in_frames,
                                          obj_id_in_frames,
                                          frame_idxs)
        tracked_boxes = tracked_box_counter(tracked_boxes,
                                            obj_id_in_frames)  # tracked_boxes = [{label0: [x0, y0, x1, y1], label1: [x0, y0, x1, y1], ...}, ...]
        label_seed_frames = get_label_seed_frames(shot_div_frames, tracked_boxes, h, w)

        # div_shots = shot_div(obj_id_in_frames)

        if debug:
            print("[Debug Result in run_on_video_tracking]")
            print("---------------------------------------")
            print("-- Object ID in Frames")
            for i, oif in enumerate(obj_id_in_frames):
                print("Frame : ", i)
                print("Object ID :", oif)
            print("Obj ID in Frames Length : ", len(obj_id_in_frames))

            shot_div_frames_key = list(shot_div_frames.keys())
            print("---------------------------------------")
            print("-- Shot Divide by Labels")
            for i in shot_div_frames_key:
                print("Label :", i)
                print("Label in Frames :", shot_div_frames[i])
            print("Shot Divide by Labels Length : ", len(shot_div_frames))

            print("---------------------------------------")
            print("-- Tracked Boxes in Frames")
            for i, tb in enumerate(tracked_boxes):
                print("Frame : ", i)
                print("tracked_box : ", tb)
            print("TRACKED BOXES Length : ", len(tracked_boxes))

            print("---------------------------------------")
            print("Label Seed Frames : ", label_seed_frames)
            # print("Label Seed Frames Length : ", len(label_seed_frames))
            # print("SHOT DIVIDE : ", div_shots)
            # print("Mean_iou_frames :", mean_iou_frames)
            # print("Mean_iou_frames Length :", len(mean_iou_frames))

        # yield shot_div_frames
        yield shot_div_idxs
        yield label_seed_frames
        yield obj_id_in_frames
        yield tracked_boxes
