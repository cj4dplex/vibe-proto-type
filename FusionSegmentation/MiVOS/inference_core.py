"""
Heart of most evaluation scripts (DAVIS semi-sup/interactive, GUI)
Handles propagation and fusion
See eval_semi_davis.py / eval_interactive_davis.py for examples
"""

import torch
import numpy as np
import cv2
import time
from PIL import Image

from model.propagation.prop_net import PropagationNetwork
from model.fusion_net import FusionNet
from model.aggregate import aggregate_sbg, aggregate_wbg
from davisinteractive.utils.scribbles import scribbles2mask

from vos_util.tensor_util import pad_divide_by

class InferenceCore:
    """
    images - leave them in original dimension (unpadded), but do normalize them. 
            Should be CPU tensors of shape B*T*3*H*W
            
    mem_profile - How extravagant I can use the GPU memory. 
                Usually more memory -> faster speed but I have not drawn the exact relation
                0 - Use the most memory
                1 - Intermediate, larger buffer 
                2 - Intermediate, small buffer 
                3 - Use the minimal amount of GPU memory
                Note that *none* of the above options will affect the accuracy
                This is a space-time tradeoff, not a space-performance one

    mem_freq - Period at which new memory are put in the bank
                Higher number -> less memory usage
                Unlike the last option, this *is* a space-performance tradeoff
    """
    def __init__(self, prop_net:PropagationNetwork, fuse_net:FusionNet, images, num_objects, 
                    mem_profile=0, mem_freq=5, device='cuda:0'):
        with torch.no_grad():
            self.prop_net = prop_net.to(device, non_blocking=True)
            if fuse_net is not None:
                self.fuse_net = fuse_net.to(device, non_blocking=True)
            self.mem_profile = mem_profile
            self.mem_freq = mem_freq
            self.device = device

            if mem_profile == 0:
                self.data_dev = device
                self.result_dev = device
                self.q_buf_size = 105
                self.i_buf_size = -1  # no need to buffer image
            elif mem_profile == 1:
                self.data_dev = 'cpu'
                self.result_dev = device
                self.q_buf_size = 105
                self.i_buf_size = 105
            elif mem_profile == 2:
                self.data_dev = 'cpu'
                self.result_dev = 'cpu'
                self.q_buf_size = 3
                self.i_buf_size = 3
            else:
                self.data_dev = 'cpu'
                self.result_dev = 'cpu'
                self.q_buf_size = 1
                self.i_buf_size = 1

            # print("# DATA Allocation Device #")
            # print("data_dev : {}, result_dev : {}".format(self.data_dev, self.result_dev))

            # True dimensions

            num_imgs = images.shape[1]
            t = 1  # self.masks, prob (<- 적용)

            h, w = images.shape[-2:]
            self.k = num_objects

            # Pad each side to multiples of 16
            self.images, self.pad = pad_divide_by(images, 16, images.shape[-2:])
            # Padded dimensions
            nh, nw = self.images.shape[-2:]
            self.images = self.images.to(self.data_dev)

            # These two store the same information in different formats
            self.masks = torch.zeros((t, 1, nh, nw), dtype=torch.uint8, device=self.result_dev)
            self.np_masks = np.zeros((t, h, w), dtype=np.uint8)

            # Object probabilities, background included
            self.prob = torch.zeros((self.k + 1, t, 1, nh, nw), dtype=torch.float32, device=self.result_dev)
            self.prob[0] = 1e-7

            # self.t, self.h, self.w = t, h, w
            self.t, self.h, self.w = num_imgs, h, w
            self.nh, self.nw = nh, nw
            self.kh = self.nh//16
            self.kw = self.nw//16

            self.query_buf = {}
            self.image_buf = {}
            self.interacted = set()

            self.certain_mem_k = None
            self.certain_mem_v = None

    def get_image_buffered(self, idx):
        if self.data_dev == self.device:
            return self.images[:,idx]

        # buffer the .cuda() calls
        if idx not in self.image_buf:
            # Flush buffer
            if len(self.image_buf) > self.i_buf_size:
                self.image_buf = {}
        self.image_buf[idx] = self.images[:,idx].to(self.device)
        result = self.image_buf[idx]

        return result

    def get_query_kv_buffered(self, idx):
        # Queries' key/value never change, so we can buffer them here
        if idx not in self.query_buf:
            # Flush buffer
            if len(self.query_buf) > self.q_buf_size:
                self.query_buf = {}

            self.query_buf[idx] = self.prop_net.get_query_values(self.get_image_buffered(idx))
        result = self.query_buf[idx]
        torch.cuda.empty_cache()
        return result

    def do_pass(self, key_k, key_v, idx, forward=True, step_cb=None):
        """
        Do a complete pass that includes propagation and fusion
        key_k/key_v -  memory feature of the starting frame
        idx - Frame index of the starting frame
        forward - forward/backward propagation
        step_cb - Callback function used for GUI (progress bar) only
        """

        # print("do pass start -> VRAM : ", torch.cuda.memory_reserved())

        # Pointer in the memory bank
        num_certain_keys = self.certain_mem_k.shape[2]
        m_front = num_certain_keys

        # Determine the required size of the memory bank
        if forward:
            closest_ti = min([ti for ti in self.interacted if ti > idx] + [self.t])
            total_m = (closest_ti - idx - 1) // self.mem_freq + 1 + num_certain_keys
        else:
            closest_ti = max([ti for ti in self.interacted if ti < idx] + [-1])
            total_m = (idx - closest_ti - 1) // self.mem_freq + 1 + num_certain_keys
        K, CK, _, H, W = key_k.shape
        _, CV, _, _, _ = key_v.shape

        # Pre-allocate keys/values memory
        keys = torch.empty((K, CK, total_m, H, W), dtype=torch.float32, device=self.device)
        values = torch.empty((K, CV, total_m, H, W), dtype=torch.float32, device=self.device)

        # Initial key/value passed in
        keys[:, :, 0:num_certain_keys] = self.certain_mem_k
        values[:, :, 0:num_certain_keys] = self.certain_mem_v
        prev_in_mem = True
        last_ti = idx

        # Note that we never reach closest_ti, just the frame before it
        if forward:
            this_range = range(idx + 1, closest_ti)
            step = +1
            end = closest_ti - 1
        else:
            this_range = range(idx - 1, closest_ti, -1)
            step = -1
            end = closest_ti + 1

        for ti in this_range:
            # print("{} in {} : ".format(ti, this_range, torch.cuda.memory_reserved()))
            if prev_in_mem:
                this_k = keys[:, :, :m_front]
                this_v = values[:, :, :m_front]
            else:
                this_k = keys[:, :, :m_front + 1]
                this_v = values[:, :, :m_front + 1]

            # query = self.get_query_kv_buffered(ti)
            # out_mask = self.prop_net.segment_with_query(this_k, this_v, *query)
            # out_mask = aggregate_wbg(out_mask, keep_bg=True)

            self.query = self.get_query_kv_buffered(ti)
            self.propagate_out_mask = self.prop_net.segment_with_query(this_k, this_v, *self.query)
            self.propagate_out_mask = aggregate_wbg(self.propagate_out_mask, keep_bg=True)

            if ti != end:
                keys[:, :, m_front:m_front + 1], values[:, :, m_front:m_front + 1] = self.prop_net.memorize(
                    self.get_image_buffered(ti), self.propagate_out_mask[1:])
                if abs(ti - last_ti) >= self.mem_freq:
                    # Memorize the frame
                    m_front += 1
                    last_ti = ti
                    prev_in_mem = True
                else:
                    prev_in_mem = False

            torch.cuda.empty_cache()

            # In-place fusion, maximizes the use of queried buffer
            # esp. for long sequence where the buffer will be flushed
            if (closest_ti != self.t) and (closest_ti != -1):
                self.prob[:, ti] = self.fuse_one_frame(closest_ti, idx, ti, self.prob[:, ti], self.propagate_out_mask,
                                                       key_k, self.query[3]).to(self.result_dev)
            else:
                self.prob[:, ti] = self.propagate_out_mask.to(self.result_dev)

            # Callback function for the GUI
            if step_cb is not None:
                step_cb()

            self.propagate_out_mask = None
            self.query = None
            torch.cuda.empty_cache()
        # print("do pass end -> VRAM : ", torch.cuda.memory_reserved())
        torch.cuda.empty_cache()

        return closest_ti

    def trim_paddings(self, out_masks):
        if self.pad[2] + self.pad[3] > 0:
            out_masks = out_masks[:, :, self.pad[2]:-self.pad[3], :]
        if self.pad[0] + self.pad[1] > 0:
            out_masks = out_masks[:, :, :, self.pad[0]:-self.pad[1]]

        np_mask = (out_masks.detach().cpu().numpy()[:, 0]).astype(np.uint8)
        del self.masks
        return np_mask

    def do_pass_frame(self, key_k, key_v, idx, forward=True, step_cb=None):
        """
        Do a complete pass that includes propagation and fusion
        key_k/key_v -  memory feature of the starting frame
        idx - Frame index of the starting frame
        forward - forward/backward propagation
        step_cb - Callback function used for GUI (progress bar) only
        """

        # del self.masks
        # del self.prob
        # torch.cuda.empty_cache()
        print("do pass start : ", torch.cuda.memory_reserved())

        # Pointer in the memory bank
        num_certain_keys = self.certain_mem_k.shape[2]
        m_front = num_certain_keys

        start_frame, end_frame = self.ti_range
        nh, nw = self.images.shape[-2:]

        # Determine the required size of the memory bank
        if forward:
            # closest_ti = min([ti for ti in self.interacted if ti > idx] + [self.t])
            closest_ti = end_frame
            total_m = (closest_ti - idx - 1) // self.mem_freq + 1 + num_certain_keys
            this_range = range(idx + 1, end_frame)  # Forward 범위
            t = end_frame - idx + 1  # Forward 결과물 보관을 위한 torch.array 사이즈
            # Forward 결과물 보관을 위한 torch.array 생성
            self.masks = torch.zeros((t, 1, nh, nw), dtype=torch.uint8, device=self.result_dev)
            self.prob = torch.zeros((self.k + 1, t, 1, nh, nw), dtype=torch.float32, device=self.result_dev)
            end = closest_ti - 1
            i = idx + 1
            print("Forward Range : ", this_range)

        else:
            closest_ti = start_frame - 1
            total_m = (idx - closest_ti - 1) // self.mem_freq + 1 + num_certain_keys
            this_range = range(idx, start_frame - 1, -1)  # Backward 범위
            t = idx - start_frame + 1  # Backward 결과물 보관을 위한 torch.array 사이즈
            # Backward 결과물 보관을 위한 torch.array 생성
            self.masks = torch.zeros((t, 1, nh, nw), dtype=torch.uint8, device=self.result_dev)
            self.prob = torch.zeros((self.k + 1, t, 1, nh, nw), dtype=torch.float32, device=self.result_dev)
            end = closest_ti + 1
            i = start_frame
            print("Backward Range : ", this_range)

        K, CK, _, H, W = key_k.shape
        _, CV, _, _, _ = key_v.shape

        # Pre-allocate keys/values memory
        keys = torch.empty((K, CK, total_m, H, W), dtype=torch.float32, device=self.device)
        values = torch.empty((K, CV, total_m, H, W), dtype=torch.float32, device=self.device)

        # Initial key/value passed in
        keys[:, :, 0:num_certain_keys] = self.certain_mem_k
        values[:, :, 0:num_certain_keys] = self.certain_mem_v
        prev_in_mem = True
        last_ti = idx

        # Note that we never reach closest_ti, just the frame before it
        # print("Range Size : ", t)
        cuda_memory_amount = torch.cuda.memory_reserved()
        print("do pass load init data : ", cuda_memory_amount)
        process_cnt = 0
        cuda_memory_allocations = []
        oom = False
        oom_predict = False
        for ti in this_range:
            # print("ti : {} / ti - i : {}".format(ti, ti - i))
            if prev_in_mem:
                this_k = keys[:, :, :m_front]
                this_v = values[:, :, :m_front]
            else:
                this_k = keys[:, :, :m_front + 1]
                this_v = values[:, :, :m_front + 1]
            # print("DO PASS (1) : ", torch.cuda.memory_allocated())

            # Key / Value Buffer -> CUDA 메모리 사용 후, Free 상태가 되지 않음
            query = self.get_query_kv_buffered(ti)
            out_mask = self.prop_net.segment_with_query(this_k, this_v, *query)
            out_mask = aggregate_wbg(out_mask, keep_bg=True)
            # print("DO PASS (2) : ", torch.cuda.memory_allocated())

            cuda_memory_allocations.append(torch.cuda.memory_allocated() / (10**9))
            if process_cnt == 1:
                frame_cuda_memory = cuda_memory_allocations[1] - cuda_memory_allocations[0]
                predict_total_memory = frame_cuda_memory * t + cuda_memory_allocations[0]
                if predict_total_memory > 23:
                    print("Warning : Out of Memory will Occur ..!")
                    oom_predict = True
            if oom_predict:
                oom = True
                break
            else:
                oom = False
                if ti != end:
                    keys[:, :, m_front:m_front + 1], values[:, :, m_front:m_front + 1] = self.prop_net.memorize(
                        self.get_image_buffered(ti), out_mask[1:])
                    if abs(ti - last_ti) >= self.mem_freq:
                        # Memorize the frame
                        m_front += 1
                        last_ti = ti
                        prev_in_mem = True
                    else:
                        prev_in_mem = False
                torch.cuda.empty_cache()

                # print("DO PASS (3) : ", torch.cuda.memory_allocated())
                # In-place fusion, maximizes the use of queried buffer
                # esp. for long sequence where the buffer will be flushed
                if (closest_ti != end_frame + 1) and (closest_ti != start_frame - 1):  # Seed Frame의 index가 아닌 것들
                    self.prob[:, ti-i] = self.fuse_one_frame(closest_ti, idx, ti, self.prob[:, ti-i], out_mask,
                                                           key_k, query[3]).to(self.result_dev)
                else:
                    self.prob[:, ti-i] = out_mask.to(self.result_dev)  # Seed Frame (연산 진행 X)
                self.masks[ti-i] = torch.argmax(self.prob[:, ti-i], dim=0)
                torch.cuda.empty_cache()

                # Callback function for the GUI
                if step_cb is not None:
                    step_cb()
                process_cnt += 1

        if not oom:
            del self.prob
            out_masks = self.masks
            # np_masks = self.trim_paddings(self.masks)
            # out_masks = None
            if self.pad[2] + self.pad[3] > 0:
                out_masks = self.masks[:, :, self.pad[2]:-self.pad[3], :]
            if self.pad[0] + self.pad[1] > 0:
                out_masks = self.masks[:, :, :, self.pad[0]:-self.pad[1]]

            # if out_masks is not None:
            np_masks = (out_masks.detach().cpu().numpy()[:, 0]).astype(np.uint8)
            del self.masks
            masks = np.copy(np_masks)
            del np_masks
            # else:
            #     del self.masks
            #     masks = None
        else:
            del self.masks
            masks = None

        print("do pass end : ", torch.cuda.memory_reserved())
        torch.cuda.empty_cache()
        return masks


    def fuse_one_frame(self, tc, tr, ti, prev_mask, curr_mask, mk16, qk16):
        # print("tc : {} / ti : {} / tr : {}".format(tc, ti, tr))
        assert(tc<ti<tr or tr<ti<tc)

        prob = torch.zeros((self.k, 1, self.nh, self.nw), dtype=torch.float32, device=self.device)

        # Compute linear coefficients
        nc = abs(tc-ti) / abs(tc-tr)
        nr = abs(tr-ti) / abs(tc-tr)
        dist = torch.FloatTensor([nc, nr]).to(self.device).unsqueeze(0)
        for k in range(1, self.k+1):
            attn_map = self.prop_net.get_attention(mk16[k-1:k], self.pos_mask_diff[k:k+1], self.neg_mask_diff[k:k+1], qk16)

            w = torch.sigmoid(self.fuse_net(self.get_image_buffered(ti),
                    prev_mask[k:k+1].to(self.device), curr_mask[k:k+1].to(self.device), attn_map, dist))
            prob[k-1] = w
        return aggregate_wbg(prob, keep_bg=True)

    def interact(self, mask, idx, total_cb=None, step_cb=None, ranges=None):
        """
        Interact -> Propagate -> Fuse

        mask - One-hot mask of the interacted frame, background included
        idx - Frame index of the interacted frame
        total_cb, step_cb - Callback functions for the GUI (Propagate 프레임 범위)

        Return: all mask results in np format for DAVIS evaluation
        """

        print("interacted Start")
        torch.cuda.empty_cache()
        # print(torch.cuda.memory_reserved())
        self.interacted.add(idx)

        if ranges is not None:
            start = ranges[0]
            end = ranges[len(ranges) - 1]
            ti_range = [start, end + 1]
        else:
            ti_range = [0, self.t]

        mask = mask.to(self.device)
        mask, _ = pad_divide_by(mask, 16, mask.shape[-2:])
        self.mask_diff = mask - self.prob[:, idx].to(self.device)
        self.pos_mask_diff = self.mask_diff.clamp(0, 1)
        self.neg_mask_diff = (-self.mask_diff).clamp(0, 1)

        self.prob[:, idx] = mask
        key_k, key_v = self.prop_net.memorize(self.get_image_buffered(idx), mask[1:])
        torch.cuda.empty_cache()

        if self.certain_mem_k is None:
            self.certain_mem_k = key_k
            self.certain_mem_v = key_v
        else:
            self.certain_mem_k = torch.cat([self.certain_mem_k, key_k], 2)
            self.certain_mem_v = torch.cat([self.certain_mem_v, key_v], 2)

        if total_cb is not None:
            # Finds the total num. frames to process
            front_limit = min([ti for ti in self.interacted if ti > idx] + [self.t])
            back_limit = max([ti for ti in self.interacted if ti < idx] + [-1])
            total_num = front_limit - back_limit - 2  # -1 for shift, -1 for center frame
            if total_num > 0:
                total_cb(total_num)

        self.do_pass(key_k, key_v, idx, True, step_cb=step_cb)
        self.do_pass(key_k, key_v, idx, False, step_cb=step_cb)

        self.mask_diff = None
        self.pos_mask_diff = None
        self.neg_mask_diff = None

        # This is a more memory-efficient argmax
        # for ti in range(self.t):
        for ti in range(ti_range[0], ti_range[1]):
            self.masks[ti] = torch.argmax(self.prob[:,ti], dim=0)
        out_masks = self.masks

        # Trim paddings
        if self.pad[2]+self.pad[3] > 0:
            out_masks = out_masks[:,:,self.pad[2]:-self.pad[3],:]
        if self.pad[0]+self.pad[1] > 0:
            out_masks = out_masks[:,:,:,self.pad[0]:-self.pad[1]]

        self.np_masks = (out_masks.detach().cpu().numpy()[:,0]).astype(np.uint8)  # out_masks -> extract : col(index : 0)
        del out_masks

        torch.cuda.empty_cache()
        # print("interacted End")
        # print(torch.cuda.memory_reserved())

        return self.np_masks

    def interact_clear(self, images, num_objects):
        self.prob = None
        self.masks = None
        self.mask_diff = None
        self.pos_mask_diff = None
        self.neg_mask_diff = None
        self.propagate_out_mask = None
        self.query = None

        torch.cuda.empty_cache()

        # True dimensions
        num_imgs = images.shape[1]
        t = 1
        h, w = images.shape[-2:]
        self.k = num_objects

        # Pad each side to multiples of 16
        self.images, self.pad = pad_divide_by(images, 16, images.shape[-2:])
        # Padded dimensions
        nh, nw = self.images.shape[-2:]
        self.images = self.images.to(self.data_dev, non_blocking=False)

        # These two store the same information in different formats
        self.masks = torch.zeros((t, 1, nh, nw), dtype=torch.uint8, device=self.result_dev)
        self.np_masks = np.zeros((t, h, w), dtype=np.uint8)

        # Object probabilities, background included
        self.prob = torch.zeros((self.k + 1, t, 1, nh, nw), dtype=torch.float32, device=self.result_dev)
        self.prob[0] = 1e-7

        self.t, self.h, self.w = num_imgs, h, w
        self.nh, self.nw = nh, nw
        self.kh = self.nh // 16
        self.kw = self.nw // 16

        self.query_buf = {}
        self.image_buf = {}
        self.interacted = set()

        self.certain_mem_k = None
        self.certain_mem_v = None

    def interact_frame(self, mask, idx, total_cb=None, step_cb=None, ranges=None):
        """
        Interact -> Propagate -> Fuse

        mask - One-hot mask of the interacted frame, background included
        idx - Frame index of the interacted frame
        total_cb, step_cb - Callback functions for the GUI (Propagate 프레임 범위)

        Return: all mask results in np format for DAVIS evaluation
        """

        # print("interacted Start")
        # print(torch.cuda.memory_reserved())
        self.interacted.add(idx)

        if ranges is not None:
            start = ranges[0]
            end = ranges[len(ranges) - 1]
            ti_range = [start, end + 1]
        else:
            ti_range = [0, self.t]

        self.ti_range = ti_range
        mask = mask.to(self.device)
        mask, _ = pad_divide_by(mask, 16, mask.shape[-2:])
        # self.mask_diff = mask - self.prob[:, idx].to(self.device) # 기존 코드
        self.mask_diff = mask - self.prob[:, 0].to(self.device)
        self.pos_mask_diff = self.mask_diff.clamp(0, 1)
        self.neg_mask_diff = (-self.mask_diff).clamp(0, 1)

        print("Propagate (1) : ", torch.cuda.memory_allocated())
        # self.prob[:, idx] = mask  # 기존 코드
        self.prob[:, 0] = mask
        key_k, key_v = self.prop_net.memorize(self.get_image_buffered(idx), mask[1:])
        torch.cuda.empty_cache()

        print("Propagate (2) : ", torch.cuda.memory_allocated())
        if self.certain_mem_k is None:
            self.certain_mem_k = key_k
            self.certain_mem_v = key_v
        else:
            self.certain_mem_k = torch.cat([self.certain_mem_k, key_k], 2)
            self.certain_mem_v = torch.cat([self.certain_mem_v, key_v], 2)

        if total_cb is not None:
            # Finds the total num. frames to process
            front_limit = min([ti for ti in self.interacted if ti > idx] + [self.t])
            back_limit = max([ti for ti in self.interacted if ti < idx] + [-1])
            total_num = front_limit - back_limit - 2 # -1 for shift, -1 for center frame
            if total_num > 0:
                total_cb(total_num)

        print("Forward Processing : ", torch.cuda.memory_allocated())
        forward_masks = self.do_pass_frame(key_k, key_v, idx, True, step_cb=step_cb)
        print("Backward Processing : ", torch.cuda.memory_allocated())
        backward_masks = self.do_pass_frame(key_k, key_v, idx, False, step_cb=step_cb)

        # self.np_masks = np.concatenate((backward_masks, forward_masks), axis=0)
        np_masks = np.concatenate((backward_masks, forward_masks), axis=0)

        # return self.np_masks
        return np_masks

    def interact_div(self, mask, idx, total_cb=None, step_cb=None, ranges=None, forward=False):
        """
        Interact -> Propagate -> Fuse
        Propagate Divide (Forward & Backward)
        -> interact_div의 경우, Forward / Backward 작업 분리
        -> interact_frame의 경우, Forward + Backward

        mask - One-hot mask of the interacted frame, background included
        idx - Frame index of the interacted frame
        total_cb, step_cb - Callback functions for the GUI (Propagate 프레임 범위)

        Return: all mask results in np format for DAVIS evaluation
        """

        print("Interact_div START : ", torch.cuda.memory_reserved())
        self.interacted.add(idx)
        if ranges is not None:
            start = ranges[0]
            end = ranges[len(ranges) - 1]
            ti_range = [start, end]
        else:
            ti_range = [0, self.t]

        self.ti_range = ti_range
        mask = mask.to(self.device)
        mask, _ = pad_divide_by(mask, 16, mask.shape[-2:])
        # self.mask_diff = mask - self.prob[:, idx].to(self.device) # 기존 코드
        self.mask_diff = mask - self.prob[:, 0].to(self.device)
        self.pos_mask_diff = self.mask_diff.clamp(0, 1)
        self.neg_mask_diff = (-self.mask_diff).clamp(0, 1)

        # self.prob[:, idx] = mask  # 기존 코드
        self.prob[:, 0] = mask
        key_k, key_v = self.prop_net.memorize(self.get_image_buffered(idx), mask[1:])
        torch.cuda.empty_cache()

        if self.certain_mem_k is None:
            self.certain_mem_k = key_k
            self.certain_mem_v = key_v
        else:
            self.certain_mem_k = torch.cat([self.certain_mem_k, key_k], 2)
            self.certain_mem_v = torch.cat([self.certain_mem_v, key_v], 2)

        if total_cb is not None:
            # Finds the total num. frames to process
            front_limit = min([ti for ti in self.interacted if ti > idx] + [self.t])
            back_limit = max([ti for ti in self.interacted if ti < idx] + [-1])
            total_num = front_limit - back_limit - 2 # -1 for shift, -1 for center frame
            if total_num > 0:
                total_cb(total_num)

        if forward:
            print("Forward Processing : ", torch.cuda.memory_reserved())
            forward_masks = self.do_pass_frame(key_k, key_v, idx, True, step_cb=step_cb)
            print("Forward End")
            if forward_masks is None:
                np_masks = None
                self.certain_mem_k = None
                self.certain_mem_v = None
                torch.cuda.empty_cache()
            else:
                np_masks = forward_masks
                self.certain_mem_k = None
                self.certain_mem_v = None
                torch.cuda.empty_cache()
        else:
            print("Backward Processing : ", torch.cuda.memory_reserved())
            backward_masks = self.do_pass_frame(key_k, key_v, idx, False, step_cb=step_cb)
            print("Backward End")
            if backward_masks is None:
                np_masks = None
                self.certain_mem_k = None
                self.certain_mem_v = None
                torch.cuda.empty_cache()
            else:
                np_masks = backward_masks
                self.certain_mem_k = None
                self.certain_mem_v = None

        torch.cuda.empty_cache()
        print("Interact_div END : ", torch.cuda.memory_reserved())
        return np_masks

    def update_mask_only(self, prob_mask, idx):
        """
        Interaction only, no propagation/fusion
        prob_mask - mask of the interacted frame, background included
        idx - Frame index of the interacted frame

        Return: all mask results in np format for DAVIS evaluation
        """
        mask = torch.argmax(prob_mask, 0)
        self.masks[idx] = mask

        # Mask - 1 * H * W
        if self.pad[2]+self.pad[3] > 0:
            mask = mask[:,self.pad[2]:-self.pad[3],:]
        if self.pad[0]+self.pad[1] > 0:
            mask = mask[:,:,self.pad[0]:-self.pad[1]]

        mask = (mask.detach().cpu().numpy()[0]).astype(np.uint8)
        self.np_masks[idx] = mask

        return self.np_masks