# Copyright (c) Facebook, Inc. and its affiliates. All Rights Reserved
import numpy as np
import atexit
import bisect
import multiprocessing as mp
from collections import deque
import cv2
import torch
import matplotlib.pyplot as plt
from collections import Counter

from detectron2.data import MetadataCatalog
from detectron2.engine.defaults import DefaultPredictor
from detectron2.utils.video_visualizer import VideoVisualizer
from addon.detectron2.detectron2.utils.visualizer import ColorMode, Visualizer

from adet.utils.visualizer import TextVisualizer
from interact.tracking import *

from interact.tracking import *

class VisualizationDemo(object):
    def __init__(self, cfg, instance_mode=ColorMode.IMAGE, parallel=False):
        """
        Args:
            cfg (CfgNode):
            instance_mode (ColorMode):
            parallel (bool): whether to run the model in different processes from visualization.
                Useful since the visualization logic can be slow.
        """
        self.metadata = MetadataCatalog.get(
            cfg.DATASETS.TEST[0] if len(cfg.DATASETS.TEST) else "__unused"
        )

        self.cpu_device = torch.device("cpu")
        self.instance_mode = instance_mode
        self.vis_text = cfg.MODEL.ROI_HEADS.NAME == "TextHead"

        self.parallel = parallel  # True : GPU 병렬연산 (Multi GPU)
        if parallel:
            num_gpu = torch.cuda.device_count()
            self.predictor = AsyncPredictor(cfg, num_gpus=num_gpu)
        else:
            if cfg.MODEL.DEVICE == "cuda:0":  # GPU 연산 (Single GPU)
                self.predictor = DefaultPredictor(cfg)
            else:
                self.predictor = DefaultPredictor(cfg)
        self.tracker = []

    def run_on_image(self, image, selected_classes):
        """
        Args:
            image (np.ndarray): an image of shape (H, W, C) (in BGR order).
                This is the format used by OpenCV.

        Returns:
            predictions (dict): the output of the model.
            vis_output (VisImage): the visualized image output.
        """
        # print("Selected Classes : ", selected_classes)
        vis_output = None

        voc_masks = None
        voc_arrays = None
        boxes = None
        labels = None
        class_in_frame = None
        scores = None
        ds_masks = None

        predictions = self.predictor(image)
        # Convert image from OpenCV BGR format to Matplotlib RGB format.
        image = image[:, :, ::-1]
        if self.vis_text:
            visualizer = TextVisualizer(image, self.metadata, instance_mode=self.instance_mode)
        else:
            visualizer = Visualizer(image, self.metadata, instance_mode=self.instance_mode)

        if "bases" in predictions:
            self.vis_bases(predictions["bases"])
        if "panoptic_seg" in predictions:
            panoptic_seg, segments_info = predictions["panoptic_seg"]
            vis_output = visualizer.draw_panoptic_seg_predictions(
                panoptic_seg.to(self.cpu_device), segments_info
            )
        else:
            if "sem_seg" in predictions:
                vis_output = visualizer.draw_sem_seg(
                    predictions["sem_seg"].argmax(dim=0).to(self.cpu_device))  # .to(self.cpu_device)
            if "instances" in predictions:
                instances = predictions["instances"].to(self.cpu_device)  # .to(self.cpu_device)
                # voc_masks, voc_arrays, boxes, labels, class_in_frame = visualizer.draw_instance_prediction(
                #     predictions=instances, selected_cls_id=selected_classes) ## Get visual output
                vis_output, voc_masks, voc_arrays, boxes, labels, class_in_frame, scores, ds_masks, all_info = visualizer.draw_instance_prediction(
                    predictions=instances, selected_cls_id=selected_classes)  ## Get visual output

        # return voc_masks, voc_arrays, boxes, labels, class_in_frame
        return vis_output, voc_masks, voc_arrays, boxes, labels, class_in_frame, scores, ds_masks, all_info

    def _frame_from_video(self, video):
        while video.isOpened():
            success, frame = video.read()
            if success:
                yield frame
            else:
                break

    def vis_bases(self, bases):
        basis_colors = [[2, 200, 255], [107, 220, 255], [30, 200, 255], [60, 220, 255]]
        bases = bases[0].squeeze()
        bases = (bases / 8).tanh().cpu().numpy()
        num_bases = len(bases)
        fig, axes = plt.subplots(nrows=num_bases // 2, ncols=2)
        for i, basis in enumerate(bases):
            basis = (basis + 1) / 2
            basis = basis / basis.max()
            basis_viz = np.zeros((basis.shape[0], basis.shape[1], 3), dtype=np.uint8)
            basis_viz[:, :, 0] = basis_colors[i][0]
            basis_viz[:, :, 1] = basis_colors[i][1]
            basis_viz[:, :, 2] = np.uint8(basis * 255)
            basis_viz = cv2.cvtColor(basis_viz, cv2.COLOR_HSV2RGB)
            axes[i // 2][i % 2].imshow(basis_viz)
        plt.show()

    def run_on_video(self, video):
        """
        Visualizes predictions on frames of the input video.

        Args:
            video : frames of video [frame0, frame1, frame2 ...]

        Yields:
            ndarray: BGR visualizations of each video frame.
        """
        video_visualizer = VideoVisualizer(self.metadata, self.instance_mode)

        def process_predictions(frame, predictions):
            frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
            if "panoptic_seg" in predictions:
                panoptic_seg, segments_info = predictions["panoptic_seg"]
                vis_frame = video_visualizer.draw_panoptic_seg_predictions(
                    frame, panoptic_seg.to(self.cpu_device), segments_info
                )
            elif "instances" in predictions:  ## Use instances (Following)
                predictions = predictions["instances"].to(self.cpu_device)
                vis_frame, box_info = video_visualizer.draw_instance_predictions(frame, predictions)
            elif "sem_seg" in predictions:
                vis_frame = video_visualizer.draw_sem_seg(
                    frame, predictions["sem_seg"].argmax(dim=0).to(self.cpu_device)
                )

            # Converts Matplotlib RGB format to OpenCV BGR format
            vis_frame = cv2.cvtColor(vis_frame.get_image(), cv2.COLOR_RGB2BGR)
            return vis_frame, box_info

        ## GET BOX INFO FUNC.
        def get_box_infos(frame, predictions, frame_num):  # 함수명 겹침
            predictions = predictions["instances"].to(self.cpu_device)
            frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
            vis_frame, box_info = video_visualizer.draw_instance_predictions(frame, predictions)
            candidate = box_info
            matched_list = []
            mean_iou_result = []

            for id in range(len(candidate)):
                candidate[id] = [candidate[id], 4]

            # candidate = [[[x0, y0, x1, y1], life], [[x0, y0, x1, y1], life], ... ]

            init_tracker = {}
            if len(self.tracker) == 0:
                for id in range(len(candidate)):
                    init_tracker[id] = candidate[id]

            # init_tracker = {id: [[x0, y0, x1, y1], life], id: [[x0, y0, x1, y1], life], ...}
            # init_tracker[id] = [[x0, y0, x1, y1], life]

                self.tracker = init_tracker  # Dictionary format
                matched_list = list(init_tracker.keys())
                box_img = draw_boundingBox(frame, init_tracker)
                # print(self.tracker)

            else:
                self.tracker, matched_list, mean_iou_result = main_tracker(candidate, self.tracker, frame)
                print(mean_iou_result)

            # return self.tracker, box_img
            # return box_img
            yield matched_list
            yield mean_iou_result

        def object_id_counter(id_frames):
            new_id_frames = []
            max_id = max([id_frame[-1] for id_frame in id_frames])
            all_id_frames = [item for sublist in id_frames for item in sublist]
            remove_id = []
            for i in range(max_id):
                cnt = all_id_frames.count(i)
                if cnt < 24:
                    remove_id.append(i)
            print("REMOVE ID LIST :", remove_id)
            new_id_frames = remove_obj_id(id_frames, remove_id)

            return new_id_frames

        def remove_obj_id(id_frames, remove_id):
            re_id_frames = []
            for id_frame in id_frames:
                find_element = set(id_frame) & set(remove_id)
                find_element = list(find_element)
                if len(find_element) > 0:
                    id_frame_copy = id_frame.copy()
                    for i in range(len(id_frame)):
                        if id_frame[i] in find_element:
                            id_frame_copy.remove(id_frame[i])
                            # del id_frame_copy[i]
                    re_id_frames.append(id_frame_copy)
                else:
                    re_id_frames.append(id_frame)

            return re_id_frames

        def shot_div(obj_ids):
            frames_num = len(obj_ids)
            shot_cut = [0]
            not_match = []

            for i in range(frames_num - 1):
                pos_id = obj_ids[i]
                pre_id = obj_ids[i + 1]
                a = set(pos_id)
                b = set(pre_id)

                print(pos_id , pre_id)

                if len(pos_id) > len(pre_id) and len(pos_id) == len(pre_id):
                    not_match = list(a - b)
                elif len(pos_id) < len(pre_id):
                    not_match = list(b - a)

                num_not_match = len(not_match)
                # print("frame : ", i)
                print(not_match, num_not_match)

                if num_not_match > 0:
                    shot_cut.append(i)
                    not_match.clear()

            return shot_cut

        # frame_gen = self._frame_from_video(video)
        frame_gen = video

        if self.parallel:
            print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ PARALLEL @@@@@@@@@@@@@@@@@@@@@@@@@@@@")
            buffer_size = self.predictor.default_buffer_size
            frame_data = deque()

            for cnt, frame in enumerate(frame_gen):
                frame_data.append(frame)
                self.predictor.put(frame)

                if cnt >= buffer_size:
                    frame = frame_data.popleft()
                    predictions = self.predictor.get()
                    # yield process_predictions(frame, predictions)

            while len(frame_data):
                frame = frame_data.popleft()
                predictions = self.predictor.get()
                # yield process_predictions(frame, predictions)
        else:
            i = 0
            obj_id_in_frames = []
            mean_iou_frames = []
            for frame in frame_gen:
                print("======================================================")
                print("Frame number : ", i)
                obj_id_in_frame, frame_iou = get_box_info(frame, self.predictor(frame), i)
                # print("MATCHED LIST : ", obj_id_in_frame)
                obj_id_in_frames.append(obj_id_in_frame)
                mean_iou_frames.append(frame_iou)
                # yield process_predictions(frame, self.predictor(frame))
                i += 1

            get_id = obj_id_in_frames
            obj_id_in_frames = object_id_counter(obj_id_in_frames)
            div_shots = shot_div(obj_id_in_frames)
            print("Obj ID in Frames : ", obj_id_in_frames)
            print("Obj ID in Frames Length : ", len(obj_id_in_frames))
            print("SHOT DIVIDE : ", div_shots)
            print("Mean_iou_frames :", mean_iou_frames)
            print("Mean_iou_frames Length :", len(mean_iou_frames))

            yield div_shots
            yield mean_iou_frames
            yield get_id

    def run_on_video_tracking(self, video, labels, scores, stats, frame_idxs, debug):
        """
        Visualizes predictions on frames of the input video.

        Args:
            video : frames of video [frame0, frame1, frame2 ...]

        Yields:
            ndarray: BGR visualizations of each video frame.
        """

        # video_visualizer = VideoVisualizer(self.metadata, self.instance_mode)

        # def process_predictions(frame, predictions):
        #     frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
        #     if "panoptic_seg" in predictions:
        #         panoptic_seg, segments_info = predictions["panoptic_seg"]
        #         vis_frame = video_visualizer.draw_panoptic_seg_predictions(
        #             frame, panoptic_seg.to(self.cpu_device), segments_info
        #         )
        #     elif "instances" in predictions:  ## Use instances (Following)
        #         predictions = predictions["instances"].to(self.cpu_device)
        #         vis_frame, box_info = video_visualizer.draw_instance_predictions(frame, predictions)
        #     elif "sem_seg" in predictions:
        #         vis_frame = video_visualizer.draw_sem_seg(
        #             frame, predictions["sem_seg"].argmax(dim=0).to(self.cpu_device)
        #         )
        #
        #     # Converts Matplotlib RGB format to OpenCV BGR format
        #     vis_frame = cv2.cvtColor(vis_frame.get_image(), cv2.COLOR_RGB2BGR)
        #     return vis_frame, box_info

        ## GET BOX INFO FUNC.
        def get_box_info(frame, labels, scores, stats, debug):
            """
            Args:
                frame: image from video
                labels: label[1, 2, 3 ... n] in frame, which is masked
                scores: score[0.9121, 0.7326, ... n]
                stats: bounding box info[x0, x1, y0, y1], which is matched with labels

            Returns:

            """
            bgr_frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)

            candidate = stats.copy()
            # print("label : ", labels)
            # print("candidate : ", candidate)
            matched_list = []
            mean_iou_result = []
            # print("Candidate : ", candidate)
            # Give life(4) to candidate
            for id in range(len(candidate)):
                score = scores[id]
                candidate[id] = [candidate[id], 4, score]

            # candidate = [[[x0, y0, x1, y1], life, score], [[x0, y0, x1, y1], life, score], ... ]
            # print("Candidate with Life : ", candidate)

            init_tracker = {}
            if len(self.tracker) == 0:
                for n, label in enumerate(labels):
                    init_tracker[n] = candidate[n]

                # init_tracker = {id: [[x0, y0, x1, y1], life], id: [[x0, y0, x1, y1], life], ...}
                # init_tracker[id] = [[x0, y0, x1, y1], life]

                self.tracker = init_tracker  # Dictionary format
                matched_list = list(init_tracker.keys())
                # box_img = draw_boundingBox(frame, init_tracker)
                # print(self.tracker)

            else:
                self.tracker, matched_list, mean_iou_result = main_tracker(candidate, self.tracker, bgr_frame, debug)

            yield self.tracker
            yield matched_list
            yield mean_iou_result

        def get_max_element(nested_list):
            max_val = 0
            for list in nested_list:
                for element in list:
                    if max_val < element:
                        max_val = element
            return max_val

        # Counting object id in each frame, id not shown less than 24 remove id.
        def object_id_counter(id_frames):
            # print("id_frames : {}".format(id_frames))
            # max_id = max([id_frame[-1] for id_frame in id_frames])
            max_id = get_max_element(id_frames)

            all_id_frames = [item for sublist in id_frames for item in sublist]
            remove_id = []
            for id in range(max_id + 1):
                cnt = all_id_frames.count(id)
                if cnt < 24:
                    remove_id.append(id)
            # print("REMOVE ID LIST :", remove_id)
            new_id_frames = remove_obj_id(id_frames, remove_id)
            # print("new_id_frames : {}".format(new_id_frames))

            return new_id_frames

        def remove_obj_id(id_frames, remove_id):
            re_id_frames = []
            for id_frame in id_frames:
                find_element = set(id_frame) & set(remove_id)
                find_element = list(find_element)
                if len(find_element) > 0:
                    id_frame_copy = id_frame.copy()
                    for i in range(len(id_frame)):
                        if id_frame[i] in find_element:
                            id_frame_copy.remove(id_frame[i])
                            # del id_frame_copy[i]
                    re_id_frames.append(id_frame_copy)
                else:
                    re_id_frames.append(id_frame)

            return re_id_frames

        def tracked_box_counter(tracked_boxes, id_frames):
            for i, tracked_box in enumerate(tracked_boxes):
                id_frame = id_frames[i]
                tracked_box_keys = list(tracked_box.keys())
                for tracked_box_key in tracked_box_keys:
                    if tracked_box_key not in id_frame:
                        del tracked_box[tracked_box_key]

            return tracked_boxes

        def shot_div(obj_ids):
            frames_num = len(obj_ids)
            shot_cut = [0]
            not_match = []

            for i in range(frames_num - 1):
                pos_id = obj_ids[i]
                pre_id = obj_ids[i + 1]
                a = set(pos_id)
                b = set(pre_id)

                # print(pos_id , pre_id)

                if len(pos_id) > len(pre_id) and len(pos_id) == len(pre_id):
                    not_match = list(a - b)
                elif len(pos_id) < len(pre_id):
                    not_match = list(b - a)

                num_not_match = len(not_match)
                # print("frame : ", i)
                # print(not_match, num_not_match)

                if num_not_match > 0:
                    shot_cut.append(i)
                    not_match.clear()

            return shot_cut

        def shot_div_by_label(labels, obj_ids,frame_idxs):
            shot_divs = {}
            for l in labels:
                shot_div = []
                for i, obj_id in enumerate(obj_ids):
                    if l in obj_id:
                        if frame_idxs is not None:
                            idx = frame_idxs[i]
                            shot_div.append(idx)
                        else:
                            shot_div.append(i)  ## 수정 전
                shot_divs[l] = shot_div

            return shot_divs

        def track_id_box_match(tracked_box_dictionary):
            trakced_id = list(tracked_box_dictionary.keys())
            copy_tracked_box = tracked_box_dictionary.copy()
            new_tracked_box = {}
            for id in trakced_id:
                if copy_tracked_box[id][0] != [-1, -1, -1, -1] or copy_tracked_box[id][1] != 0:
                    box = copy_tracked_box[id][0]
                    score = copy_tracked_box[id][2]
                    new_tracked_box[id] = [box, score]

            return new_tracked_box

        def get_labels_in_frames(id_in_frames):
            labels = []
            for id_in_frame in id_in_frames:
                for i in id_in_frame:
                    if i not in labels:
                        labels.append(i)

            return labels

        def position_not_conner(label_box, h, w, s_mode):
            x0 = label_box[0][0]
            y0 = label_box[0][1]
            x1 = label_box[0][2]
            y1 = label_box[0][3]
            if s_mode == 'hard':
                if x0 > int(w * 0.01) and x1 < int(w * 0.99) and y1 < int(h * 0.99):
                    return True
                else:
                    return False
            elif s_mode == 'soft':
                if x0 > 0 and x1 < w and y1 < h:
                    return True
                else:
                    return False

        def get_seed_frames_sort(tracked_boxes, label_frames, label, h, w):
            from operator import itemgetter
            boxes = []
            scores = []
            for label_frame in label_frames:
                label_box = tracked_boxes[label_frame][label]
                score = label_box[1]
                # print("score : ", score)

                if position_not_conner(label_box, h, w, s_mode='hard'):
                    scores.append([label_frame, score])
                    if not scores:
                        if position_not_conner(label_box, h, w, s_mode='soft'):
                            scores.append([label_frame, score])

                if not scores:
                    scores.append([label_frame, score])
            # sorted_scores = sorted(scores.items(), key=lambda item: item[1], reverse=True)
            sorted_scores = sorted(scores, key=itemgetter(1), reverse=True)
            return sorted_scores

        def get_label_seed_frames(shot_div_frames, tracked_boxes, h, w):
            # print("tracked_boxes : ", tracked_boxes)
            # for boxes in tracked_boxes:
            #     print(boxes)

            label_seed_frames = {}
            labels = list(shot_div_frames.keys())
            for label in labels:
                label_frames = shot_div_frames[label]
                sorted_scores = get_seed_frames_sort(tracked_boxes, label_frames, label, h, w)
                label_seed_frames[label] = sorted_scores
            return label_seed_frames

        # def get_label_seed_frames(shot_div_frames, tracked_boxes, h, w):
        #     # print("tracked_boxes : ", tracked_boxes)
        #     # for boxes in tracked_boxes:
        #     #     print(boxes)
        #
        #     # Seed Frame -> score 순으로 sort 진행
        #
        #     label_seed_frames = {}
        #     labels = list(shot_div_frames.keys())
        #     for label in labels:
        #         max_label_score = 0
        #         label_seed_frame = 0
        #         label_frames = shot_div_frames[label]
        #         label_scores = []
        #         for label_frame in label_frames:
        #             label_box = tracked_boxes[label_frame][label]
        #             if position_not_conner(label_box, h, w, s_mode='hard'):
        #                 # label_pixel_size = label_box[4]
        #                 label_score = label_box[1]
        #                 if max_label_score < label_score:
        #                     max_label_score = label_score
        #                     label_seed_frame = label_frame
        #         if label_seed_frame == 0:
        #             # print("Position not Conner -> SOFT mode")
        #             max_label_score = 0
        #             for label_frame in label_frames:
        #                 label_box = tracked_boxes[label_frame][label]
        #                 if position_not_conner(label_box, h, w, s_mode='soft'):
        #                     # label_pixel_size = label_box[4]
        #                     label_score = label_box[1]
        #                     if max_label_score < label_score:
        #                         max_label_score = label_score
        #                         label_seed_frame = label_frame
        #         if label_seed_frame == 0:
        #             for label_frame in label_frames:
        #                 label_box = tracked_boxes[label_frame][label]
        #                 # label_pixel_size = label_box[4]
        #                 label_score = label_box[1]
        #                 if max_label_score < label_score:
        #                     max_label_score = label_score
        #                     label_seed_frame = label_frame
        #
        #         label_seed_frames[label] = label_seed_frame
        #     return label_seed_frames
        # 주석 끝

        # frame_gen = self._frame_from_video(video)
        frame_gen = video

        # if self.parallel:
        #     print("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ PARALLEL @@@@@@@@@@@@@@@@@@@@@@@@@@@@")
        #     buffer_size = self.predictor.default_buffer_size
        #     frame_data = deque()
        #
        #     for cnt, frame in enumerate(frame_gen):
        #         frame_data.append(frame)
        #         self.predictor.put(frame)
        #
        #         if cnt >= buffer_size:
        #             frame = frame_data.popleft()
        #             predictions = self.predictor.get()
        #             # yield process_predictions(frame, predictions)
        #
        #     while len(frame_data):
        #         frame = frame_data.popleft()
        #         predictions = self.predictor.get()
        #         # yield process_predictions(frame, predictions)
        i = 0
        obj_id_in_frames = []
        mean_iou_frames = []
        tracked_boxes = []
        h, w, c = video[0].shape

        for i, frame in enumerate(frame_gen):
            label = labels[i]
            score = scores[i]
            stat = stats[i]

            if debug:
                print("[Debug in run_on_video_tracking]")
                print("===============================================")
                print("Frame number :", i)

            tracked_box, obj_id_in_frame, frame_iou = get_box_info(frame, label, score, stat, debug)
            obj_id_in_frames.append(obj_id_in_frame)
            mean_iou_frames.append(frame_iou)
            new_tracked_box = track_id_box_match(tracked_box)
            tracked_boxes.append(new_tracked_box)

            # yield process_predictions(frame, self.predictor(frame))

        obj_id_in_frames = object_id_counter(obj_id_in_frames)
        labels_in_frames = get_labels_in_frames(obj_id_in_frames)
        shot_div_frames = shot_div_by_label(labels_in_frames,
                                            obj_id_in_frames,
                                            None)  # shot_div_frames = [[[label id], frame0, frame1, ...], ...]
        shot_div_idxs = shot_div_by_label(labels_in_frames,
                                            obj_id_in_frames,
                                            frame_idxs)
        tracked_boxes = tracked_box_counter(tracked_boxes,
                                            obj_id_in_frames)  # tracked_boxes = [{label0: [x0, y0, x1, y1], label1: [x0, y0, x1, y1], ...}, ...]
        label_seed_frames = get_label_seed_frames(shot_div_frames, tracked_boxes, h, w)

        # div_shots = shot_div(obj_id_in_frames)

        if debug:
            print("[Debug Result in run_on_video_tracking]")
            print("---------------------------------------")
            print("-- Object ID in Frames")
            for i, oif in enumerate(obj_id_in_frames):
                print("Frame : ", i)
                print("Object ID :", oif)
            print("Obj ID in Frames Length : ", len(obj_id_in_frames))

            shot_div_frames_key = list(shot_div_frames.keys())
            print("---------------------------------------")
            print("-- Shot Divide by Labels")
            for i in shot_div_frames_key:
                print("Label :", i)
                print("Label in Frames :", shot_div_frames[i])
            print("Shot Divide by Labels Length : ", len(shot_div_frames))

            print("---------------------------------------")
            print("-- Tracked Boxes in Frames")
            for i, tb in enumerate(tracked_boxes):
                print("Frame : ", i)
                print("tracked_box : ", tb)
            print("TRACKED BOXES Length : ", len(tracked_boxes))

            print("---------------------------------------")
            print("Label Seed Frames : ", label_seed_frames)
            # print("Label Seed Frames Length : ", len(label_seed_frames))
            # print("SHOT DIVIDE : ", div_shots)
            # print("Mean_iou_frames :", mean_iou_frames)
            # print("Mean_iou_frames Length :", len(mean_iou_frames))

        # yield shot_div_frames
        yield shot_div_idxs
        yield label_seed_frames
        yield obj_id_in_frames
        yield tracked_boxes

class AsyncPredictor:
    """
    A predictor that runs the model asynchronously, possibly on >1 GPUs.
    Because rendering the visualization takes considerably amount of time,
    this helps improve throughput when rendering videos.
    """

    class _StopToken:
        pass

    class _PredictWorker(mp.Process):
        def __init__(self, cfg, task_queue, result_queue):
            self.cfg = cfg
            self.task_queue = task_queue
            self.result_queue = result_queue
            super().__init__()

        def run(self):
            predictor = DefaultPredictor(self.cfg)

            while True:
                task = self.task_queue.get()
                if isinstance(task, AsyncPredictor._StopToken):
                    break
                idx, data = task
                result = predictor(data)
                self.result_queue.put((idx, result))

    def __init__(self, cfg, num_gpus: int = 1):
        """
        Args:
            cfg (CfgNode):
            num_gpus (int): if 0, will run on CPU
        """
        num_workers = max(num_gpus, 1)
        self.task_queue = mp.Queue(maxsize=num_workers * 3)
        self.result_queue = mp.Queue(maxsize=num_workers * 3)
        self.procs = []
        for gpuid in range(max(num_gpus, 1)):
            print("gpu_id : ",gpuid)
            cfg = cfg.clone()
            cfg.defrost()
            cfg.MODEL.DEVICE = "cuda:{}".format(gpuid) if num_gpus > 0 else "cpu"
            self.procs.append(
                AsyncPredictor._PredictWorker(cfg, self.task_queue, self.result_queue)
            )

        self.put_idx = 0
        self.get_idx = 0
        self.result_rank = []
        self.result_data = []

        for p in self.procs:
            p.start()
        atexit.register(self.shutdown)

    def put(self, image):
        self.put_idx += 1
        self.task_queue.put((self.put_idx, image))

    def get(self):
        self.get_idx += 1  # the index needed for this request
        if len(self.result_rank) and self.result_rank[0] == self.get_idx:
            res = self.result_data[0]
            del self.result_data[0], self.result_rank[0]
            return res

        while True:
            # make sure the results are returned in the correct order
            idx, res = self.result_queue.get()
            if idx == self.get_idx:
                return res
            insert = bisect.bisect(self.result_rank, idx)
            self.result_rank.insert(insert, idx)
            self.result_data.insert(insert, res)

    def __len__(self):
        return self.put_idx - self.get_idx

    def __call__(self, image):
        self.put(image)
        return self.get()

    def shutdown(self):
        for _ in self.procs:
            self.task_queue.put(AsyncPredictor._StopToken())

    @property
    def default_buffer_size(self):
        return len(self.procs) * 5
