# Modifed from https://github.com/seoungwugoh/ivs-demo

import numpy as np
import os
import copy
import cv2
import glob

import matplotlib.pyplot as plt
from scipy.ndimage.morphology import binary_erosion, binary_dilation
from PIL import Image, ImageDraw, ImageFont

import torch
from torchvision import models
from dataset.range_transform import im_normalization
from model.aggregate import aggregate_sbg

def images_to_torch(frames, device):
    frames = torch.from_numpy(frames.transpose(0, 3, 1, 2)).float().unsqueeze(0)/255
    b, t, c, h, w = frames.shape
    for ti in range(t):
        frames[0, ti] = im_normalization(frames[0, ti])
    return frames.to(device)


def load_image(path, mode="RGB"):
    img = cv2.imread(path, 1)  # BGR Channel
    if mode == "RGB":
        img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    imgName = os.path.basename(path).split(".")[0]
    return img, imgName
###
def load_images(files, min_side=None):
    fnames = []
    for file in files:
        # imgName -> 00000.png
        imgName = (os.path.basename(file))
        # name -> 00000 , ext -> .png
        name, _ = os.path.splitext(imgName)
        # print(imgName, "/", int(name))
        fnames.append(int(name))

    frame_list = []
    for i, file in enumerate(files):
        if min_side:
            image = Image.open(file).convert('RGB')
            w, h = image.size
            new_w = (w * min_side // min(w, h))
            new_h = (h * min_side // min(w, h))
            frame_list.append(np.array(image.resize((new_w, new_h), Image.BICUBIC), dtype=np.uint8))
        else:
            image = Image.open(file).convert('RGB')
            frame_list.append(np.array(image, dtype=np.uint8))
    frames = np.stack(frame_list, axis=0)
    return frames, fnames

def all_to_onehot(masks, labels):
    Ms = np.zeros((len(labels), masks.shape[0], masks.shape[1], masks.shape[2]), dtype=np.uint8)
    for k, l in enumerate(labels):
        Ms[k] = (masks == l).astype(np.uint8)
    return Ms

def index_from_filename(fnames):
    index = []
    for fname in enumerate(fnames):
        basename = os.path.splitext(os.path.split(fname[1])[1])[0]
        int_index = int(basename)
        index.append(int_index)

    return index

# def load_masks(path, min_side=None):
#     fnames = sorted(glob.glob(os.path.join(path, '*.png')))
#     frame_list = []
# 
#     first_frame = np.array(Image.open(fnames[0]))
#     binary_mask = (first_frame.max() == 255)
# 
#     for i, fname in enumerate(fnames):
#         if min_side:
#             image = Image.open(fname)
#             w, h = image.size
#             new_w = (w*min_side//min(w, h))
#             new_h = (h*min_side//min(w, h))
#             frame_list.append(np.array(image.resize((new_w, new_h), Image.NEAREST), dtype=np.uint8))
#         else:
#             frame_list.append(np.array(Image.open(fname), dtype=np.uint8))
# 
#     frames = np.stack(frame_list, axis=0)
#     if binary_mask:
#         frames = (frames > 128).astype(np.uint8)
#     return frames

# def load_mask(image, num_frames, num_object, min_side=None):
#     w, h = image.size
#     frame_list = []
#     interected_list = []
#
#     non_masks = np.zeros((h, w), dtype=np.uint8)
#     first_frame = np.array(image)
#     binary_mask = (first_frame.max() == 255)
#     j = 0
#
#     for i in range(num_frames):
#
#         if i in index:
#             if min_side:
#                 w, h = image.size
#                 new_w = (w * min_side // min(w, h))
#                 new_h = (h * min_side // min(w, h))
#                 frame_list.append(np.array(image.resize((new_w, new_h), Image.NEAREST), dtype=np.uint8))
#                 ## TODO : Interected Mask (min_side)
#                 j += 1
#             else:
#                 frame_list.append(np.array(image), dtype=np.uint8)
#                 interected_list.append(np.array(image).convert('P'), dtype=np.uint8)
#                 j += 1
#
#         else:
#             frame_list.append(non_masks)
#
#         frames = np.stack(frame_list, axis=0)
#         interecteds = np.stack(interected_list, axis=0)
#
#         # labels[0] = 1e-7
#         # labels = labels[labels != 0]
#
#         load_mask_labels = np.unique(interecteds[0])
#         if len(load_mask_labels) < num_object:
#             load_mask_labels = load_mask_labels[load_mask_labels != 0]
#             extra = np.zeros((num_object - len(load_mask_labels) + 1), dtype='uint8')
#             extra = np.full_like(extra, np.max(load_mask_labels) + 1)
#             load_mask_labels = np.append(load_mask_labels, extra)
#
#         setted_labels = np.zeros((num_object), dtype='uint8')
#         labels = setted_labels + load_mask_labels
#         # print(labels)
#         interecteds = torch.from_numpy(all_to_onehot(interecteds, labels)).float()  ## [3, 4, 12, 0 0 0 0 ,,, 0]
#         interecteds = aggregate_sbg(interecteds, keep_bg=True, hard=True)
#
#         if binary_mask:
#             frames = (frames > 128).astype(np.uint8)
#
#     return frames, interecteds

# def load_video(path, min_side=None):
#     frame_list = []
#     cap = cv2.VideoCapture(path)
#     while(cap.isOpened()):
#         _, frame = cap.read()
#         if frame is None:
#             break
#         frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
#         frame_list.append(frame)
#     frames = np.stack(frame_list, axis=0)
#     return frames

def load_masks(path, num_frames, num_object, min_side=None):

    fnames = sorted(glob.glob(os.path.join(path, '*.png')))
    image = Image.open(fnames[0])

    w, h = image.size
    frame_list = []
    interected_list = []
    index = []

    if len(fnames) != num_frames: # Not full masks loaded
        index = index_from_filename(fnames)
        non_masks = np.zeros((h, w), dtype=np.uint8)
        first_frame = np.array(Image.open(fnames[0]))
        binary_mask = (first_frame.max() == 255)
        j = 0

        for i in range(num_frames):

            if i in index:
                if min_side:
                    image = Image.open(fnames[j])
                    w, h = image.size
                    new_w = (w * min_side // min(w, h))
                    new_h = (h * min_side // min(w, h))
                    frame_list.append(np.array(image.resize((new_w, new_h), Image.NEAREST), dtype=np.uint8))
                    ## TODO : Interected Mask (min_side)
                    j += 1
                else:
                    frame_list.append(np.array(Image.open(fnames[j]), dtype=np.uint8))
                    interected_list.append(np.array(Image.open(fnames[j]).convert('P'), dtype=np.uint8))
                    j += 1

            else:
                frame_list.append(non_masks)
                # interected_list.append(non_masks)


        frames = np.stack(frame_list, axis=0)
        interecteds = np.stack(interected_list, axis=0)

        # labels[0] = 1e-7
        # labels = labels[labels != 0]

        load_mask_labels = np.unique(interecteds[0])  # [0, 1, 2, 3]

        if len(load_mask_labels) < num_object:
            load_mask_labels = load_mask_labels[load_mask_labels != 0]  # [1, 2, 3]
            # extra = np.zeros((num_object - len(load_mask_labels) + 1), dtype='uint8')  # 10 - 3 + 1 = 8
            # extra = np.full_like(extra, np.max(load_mask_labels) + 1)
            extra = np.zeros((num_object - len(load_mask_labels)), dtype='uint8')  # 10 - 3 = 7
            extra = np.full_like(extra, np.max(load_mask_labels) + 1)
            load_mask_labels = np.append(load_mask_labels, extra)

        setted_labels = np.zeros((num_object), dtype='uint8')
        # print(load_mask_labels)
        # print(setted_labels)
        labels = setted_labels + load_mask_labels
        # print(labels)
        interecteds = torch.from_numpy(all_to_onehot(interecteds, labels)).float() ## [3, 4, 12, 0 0 0 0 ,,, 0]
        # interecteds = aggregate_sbg(interecteds, keep_bg=True, hard=True)

        # print("interecteds after sbg")
        # print(interecteds.shape)

        # labels = np.unique(interecteds[0])
        # print(labels)
        # labels = labels[labels != 0]
        # print(labels)
        # interecteds = torch.from_numpy(all_to_onehot(interecteds, labels)).float()


        if binary_mask:
            frames = (frames > 128).astype(np.uint8)

    else: # full maks loaded
        first_frame = np.array(Image.open(fnames[0]))
        binary_mask = (first_frame.max() == 255)

        for i, fname in enumerate(fnames):
            print(i)
            if min_side:
                image = Image.open(fname)
                w, h = image.size
                new_w = (w * min_side // min(w, h))
                new_h = (h * min_side // min(w, h))
                frame_list.append(np.array(image.resize((new_w, new_h), Image.NEAREST), dtype=np.uint8))
                ## TODO : Interected Mask (min_side)
            else:
                frame_list.append(np.array(Image.open(fname), dtype=np.uint8))
                interected_list.append(np.array(Image.open(fname).convert('P'), dtype=np.uint8))

        frames = np.stack(frame_list, axis=0)
        interecteds = np.stack(interected_list, axis=0)

        labels = np.unique(interecteds[0])
        labels = labels[labels != 0]
        interecteds = torch.from_numpy(all_to_onehot(interecteds, labels)).float()

        if binary_mask:
            frames = (frames > 128).astype(np.uint8)

    # cursor = index[0]
    cursor = 0

    # print(frames)
    # torch.set_printoptions(profile="full")
    test = interecteds[0]
    # print(test)
    # print(test.shape)
    return frames, interecteds, cursor

def all_to_onehot(masks, labels):
    Ms = np.zeros((len(labels), masks.shape[0], masks.shape[1], masks.shape[2]), dtype=np.uint8)
    for k, l in enumerate(labels):
        Ms[k] = (masks == l).astype(np.uint8)
    return Ms

def resize_video(path):
    frame_list = []
    cap = cv2.VideoCapture(path)
    print('Resize Input Media')
    while(cap.isOpened()):
        _, frame = cap.read()
        if frame is None:
            break
        frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
        # ratio = 640.0 / cap.get(3)
        # size = (640, int(cap.get(4) * ratio))
        ratio = 429.0 / cap.get(4)
        size = (int(cap.get(3) * ratio), 429)
        frame = cv2.resize(frame, size, interpolation=cv2.INTER_AREA)
        frame_list.append(frame)

    return frame_list

def load_video(path, re_size=False):
    frame_list = []
    cap = cv2.VideoCapture(path)
    # frames = None

    if re_size:
        if cap.get(3) > 640 or cap.get(4) > 540:
            frames = np.stack(resize_video(path), axis=0)
            print('Resolution is changed')
    else:
        while(cap.isOpened()):
            _, frame = cap.read()
            if frame is None:
                break
            frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
            frame_list.append(frame)
        frames = np.stack(frame_list, axis=0)
    return frames

# def load_images(path, min_side=None):
#     frame_list = []
#
#     for filename in glob.glob(path + '*.png') or glob.glob(path + '*.jpg'):
#         frame = cv2.imread(filename)
#         frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
#         frame_list.append(frame)
#     frames = np.stack(frame_list, axis=0)
#
#     return frames

def select_files(path, re_size):
    file_path, file_name = os.path.split(path)
    fname, ext = os.path.splitext(path)
    print(fname)
    print(ext)
    print("Loading File : {}.{}".format(file_name, ext))
    if ext == '.mov' or ext == '.mp4' or ext == '.avi' and ext is not None:
        print("Load Video ...")
        rgb_images = load_video(path, re_size)

        return rgb_images

    else:
        print("Load Images ...")
        rgb_images, _ = load_images(path, None)

        return rgb_images

def text_read(path):
    file = open(path)
    lines = file.readlines()
    instance_frames_list = []
    for line in lines:
        line = int(line.strip())
        instance_frames_list.append(line)
    return instance_frames_list

def dir_read(path):
    dir_list = glob.glob(path)
    for path in dir_list:
        print(path)

def input_status(path):
    # filename =
    cap = cv2.VideoCapture(path)
    if cap.isOpened():
        width = cap.get(3)
        height = cap.get(4)

    return width, height

def _pascal_color_map(N=256, normalized=False):
    """
    Python implementation of the color map function for the PASCAL VOC data set.
    Official Matlab version can be found in the PASCAL VOC devkit
    http://host.robots.ox.ac.uk/pascal/VOC/voc2012/index.html#devkit
    """

    def bitget(byteval, idx):
        return (byteval & (1 << idx)) != 0

    dtype = 'float32' if normalized else 'uint8'
    cmap = np.zeros((N, 3), dtype=dtype)
    for i in range(N):
        r = g = b = 0
        c = i
        for j in range(8):
            r = r | (bitget(c, 0) << 7 - j)
            g = g | (bitget(c, 1) << 7 - j)
            b = b | (bitget(c, 2) << 7 - j)
            c = c >> 3

        cmap[i] = np.array([r, g, b])

    cmap = cmap / 255 if normalized else cmap
    return cmap


color_map = [
    [0, 0, 0],
    [255, 50, 50],
    [50, 255, 50],
    [50, 50, 255],
    [255, 50, 255],
    [50, 255, 255],
    [255, 255, 50],

    # # TEMPORARY ADD
    # [255, 0, 0],
    # [0, 255, 0],
    # [0, 0, 255],
    # [255, 50, 0],
    # [0, 255, 50],
    # [50, 0, 255],
    # [255, 50, 50],
    # [50, 255, 50],
    # [50, 50, 255],
    # [255, 0, 100],
    # [100, 255, 0],
    # [0, 100, 255],
    # [255, 50, 100],
    # [100, 255, 50],
    # [50, 100, 255],
    # [255, 100, 100],
    # [100, 255, 100],
    # [100, 100, 255],
    # [255, 0, 150],
    # [150, 255, 0],
    # [0, 150, 255],
    # [255, 50, 150],
    # [150, 255, 50],
    # [50, 150, 255],
    # [255, 100, 150],
    # [150, 255, 100],
    # [100, 150, 255],
    # [255, 0, 200],
    # [200, 255, 0],
    # [0, 200, 255],
    # [255, 50, 200],
    # [200, 255, 50],
    # [50, 200, 255],
    # [255, 100, 200],
    # [200, 255, 100],
    # [100, 200, 255],
]

color_map_np = np.array(color_map)

def overlay_image(image, mask):
    mask = np.array(mask)
    width, height, channel = image.shape
    image_map = np.array(image)

    for i in range(width):
        for j in range(height):
            if mask[i, j] == 0:
                for c in range(channel):
                    image_map[i, j, c] = image_map[i, j, c] * 0

    return image_map

# def mask_num_object(mask, path):
#     num_object = np.max(mask)
#
#     for i in range(num_object):
#         mask_dir = path + '{:03d}'.format(i)
#         os.makedirs(mask_dir, exist_ok=True)
#
#         sg_mask = get_single_mask(mask, i)
#
#     return

def convertRGBtoRGBA(image, mask):
    alpha = np.array(mask)
    rgba = cv2.cvtColor(image, cv2.COLOR_RGB2RGBA)
    rgba[:, :, 3] = alpha
    rgba_arr = np.array(rgba)
    return rgba_arr

def overlay_davis(image, mask, alpha=0.5):
    """ Overlay segmentation on top of RGB image. from davis official"""
    im_overlay = image.copy()

    colored_mask = color_map_np[mask]
    foreground = image*alpha + (1-alpha)*colored_mask
    binary_mask = (mask > 0)
    # Compose image
    im_overlay[binary_mask] = foreground[binary_mask]
    countours = binary_dilation(binary_mask) ^ binary_mask
    im_overlay[countours,:] = 0
    return im_overlay.astype(image.dtype)

def overlay_davis_fade(image, mask, alpha=0.5):
    im_overlay = image.copy()

    colored_mask = color_map_np[mask]
    foreground = image*alpha + (1-alpha)*colored_mask
    binary_mask = (mask > 0)
    # Compose image
    im_overlay[binary_mask] = foreground[binary_mask]
    countours = binary_dilation(binary_mask) ^ binary_mask
    im_overlay[countours,:] = 0
    im_overlay[~binary_mask] = im_overlay[~binary_mask] * 0.6
    return im_overlay.astype(image.dtype)