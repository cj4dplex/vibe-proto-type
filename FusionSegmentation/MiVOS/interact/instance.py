import torch
import argparse
import numpy as np
import glob
import multiprocessing as mp
from PIL import Image
import os, glob
import time
import cv2
import tqdm

from detectron2.data.detection_utils import read_image, convert_PIL_to_numpy
from detectron2.utils.logger import setup_logger
from detectron2.engine.defaults import DefaultPredictor

from demo.predictor import VisualizationDemo
from adet.config import get_cfg
from model.aggregate import aggregate_sbg

from interact.interactive_utils import *
from vos_util.palette import *
from interact.label_rotoscope import *
from interact.coco_class import *
from object_tracking import VideoTracker

palette = pal_color_map()

confidence_threshold = 0.3
fusionSegmentationPath = './FusionSegmentation/MiVOS/'

def setup_cfg(args):
    # load config from file and command-line arguments
    cfg = get_cfg()
    cfg.merge_from_file(fusionSegmentationPath + "interact/config/R_101_dcni3_5x.yaml")
    cfg.merge_from_list(['MODEL.WEIGHTS', fusionSegmentationPath + 'interact/model/R_101_dcni3_5x.pth'])
    # Set score_threshold for builtin models
    cfg.MODEL.RETINANET.SCORE_THRESH_TEST = confidence_threshold
    cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = confidence_threshold
    cfg.MODEL.FCOS.INFERENCE_TH_TEST = confidence_threshold
    cfg.MODEL.MEInst.INFERENCE_TH_TEST = confidence_threshold
    cfg.MODEL.PANOPTIC_FPN.COMBINE.INSTANCES_CONFIDENCE_THRESH = confidence_threshold
    cfg.freeze()

    return cfg

def get_parser():
    parser = argparse.ArgumentParser(description="Detectron2 Demo")
    parser.add_argument(
        "--config-file",
        default="interact/config/R_101_dcni3_5x.yaml",
        metavar="FILE",
        help="path to config file",
    )
    parser.add_argument(
        "--confidence-threshold",
        type=float,
        default=0.4,
        help="Minimum score for instance predictions to be shown",
    )
    parser.add_argument(
        "--opts",
        help="Modify config options using the command-line 'KEY VALUE' pairs",
        default=[],
        nargs=argparse.REMAINDER,
    )
    parser.add_argument(
        "--class_id",
        type=int,
        default=None,
        help="Class id for get unique object"
    )
    return parser


def load_instance_model(gpu_id):
    # Load Instance Model
    if gpu_id == -1:
        cuda_device = 'cpu'
    else:
        cuda_device = 'cuda:{}'.format(gpu_id)

    mp.set_start_method("spawn", force=True)
    args = get_parser().parse_args()
    cfg = setup_cfg(args)
    cfg = cfg.clone()
    cfg.defrost()
    cfg.MODEL.DEVICE = cuda_device
    demo = VisualizationDemo(cfg)

    return demo


def all_to_onehot(masks, labels):
    Ms = np.zeros((len(labels), masks.shape[0], masks.shape[1], masks.shape[2]), dtype=np.uint8)
    for k, l in enumerate(labels):
        Ms[k] = (masks == l).astype(np.uint8)
    return Ms

def frame_in_max_iou(frame_iou_array):
    max = [0.0]
    frame_number = 0
    for i in range(len(frame_iou_array)):
        if frame_iou_array[i] != []:
            frame_iou_val = frame_iou_array[i]
            # print("FRAME_IOU_VAL :", frame_iou_val)
            if max < frame_iou_val:
                max = frame_iou_val
                frame_number = i

    return frame_number

def select_image(num_frames, shot_div, iou_values):
    shot_iou = []  ## shot_div = [0, 8, 78, 110]
    selected_image_array = []
    print("SHOT DIV :", shot_div)
    for i in range(len(shot_div)):
        if i == len(shot_div) - 1:
            shot_div[i]
            end = num_frames - 1
        else:
            start = shot_div[i]
            end = shot_div[i + 1] - 1

        for j in range(start, end):
            shot_iou.append(iou_values[j])
        # print("SHOT IOU : ", shot_iou)
        max_iou_frame_num = frame_in_max_iou(shot_iou)
        selected_image_array.append(max_iou_frame_num)


    print("SELECTED IMAGE ARRAY", selected_image_array)
    return selected_image_array


# TODO : IF USE THIS FUNCTION -> input : array(matched t list) to array(processed t list, which was removed by shown frame cnt(23))
def label_image_instace(array, f_id):
    total_label_mask = np.zeros_like(array[0])
    for i in range(len(array)):
        if i >= len(f_id):
            obj_mask = array[i]

            label_mask = obj_mask * 0
            total_label_mask += label_mask
        else:
            obj_mask = array[i]
            label = f_id[i] + 1

            label_mask = obj_mask * label
            total_label_mask += label_mask

    total_label_mask = Image.fromarray(total_label_mask)
    total_label_mask.putpalette(palette)

    return total_label_mask


# def run_image_instance(input, num_object, cursor, this_box, this_mask, seleteced_classes, debug, cuda=True):
#     """
#     input: RGB image
#
#     Returns: mask(segmentation) result, base on class (temporary id : 0 -> person)
#     """
#     mp.set_start_method("spawn", force=True)
#     args = get_parser().parse_args()
#     cfg = setup_cfg(args)
#     cfg = cfg.clone()
#     cfg.defrost()
#     if cuda:
#         cfg.MODEL.DEVICE = "cuda:0"
#     demo = VisualizationDemo(cfg)
#
#     classes_idx = []  # 찾으려는 class index (ex) [0, 2] -> person, car)
#     if seleteced_classes:
#         for selected_class in seleteced_classes:
#             if selected_class in coco_class_list:
#                 class_idx = coco_class_list.index(selected_class)
#                 classes_idx.append(class_idx)
#     else:
#         classes_idx = ['all']
#     # print("Search This Classes idx : ", classes_idx)
#
#     fn, h, w, ch = input.shape  # Frames / height / width / channel
#
#     input_array = Image.fromarray(input[cursor])
#     frame = convert_PIL_to_numpy(input_array, format="BGR")
#
#     # cv2.imshow("frame", frame)
#     # cv2.waitKey(0)
#
#     # voc_masks -> 모든 객체가 한 프레임 내 있는 마스크 (1개)
#     # voc_arrays -> 각 객체에 대한 마스크가 있는 프레임들 (n개)
#     # _, voc_masks, voc_arrays, boxes, _, _, _ = demo.run_on_image(frame, classes_idx)
#     #
#     # cls_id = None
#     # if boxes is not None:
#     #     for cnt, box in enumerate(boxes):
#     #         box = box.astype(int)
#     #         box = box.tolist()
#     #         if box == this_box:
#     #             cls_id = cnt
#
#     frame_list = []
#     interected_list = []
#     non_masks = np.zeros((h, w), dtype=np.uint8)
#
#     # for i in range(fn):
#     #     if i == cursor and cls_id is not None:
#     #         # frame_list.append(np.array(voc_masks))
#     #         # interected_list.append(np.array(voc_masks))
#     #         frame_list.append(np.array(voc_arrays[cls_id]))
#     #         interected_list.append(np.array(voc_arrays[cls_id]))
#     #     else:
#     #         frame_list.append(non_masks)
#
#     interected_list.append(np.array(this_mask))
#     frame_list.append(np.array(this_mask))
#
#     frames = np.stack(frame_list, axis=0)
#     interecteds = np.stack(interected_list, axis=0)
#
#     load_mask_labels = np.unique(interecteds[0])
#     # print(load_mask_labels, "np.unique")
#
#     if len(load_mask_labels) < num_object:
#         load_mask_labels = load_mask_labels[load_mask_labels != 0]
#         # print(load_mask_labels)
#         extra = np.zeros((num_object - len(load_mask_labels)), dtype='uint8')
#         extra = np.full_like(extra, np.max(load_mask_labels) + 1)
#         load_mask_labels = np.append(load_mask_labels, extra)
#
#     setted_labels = np.zeros((num_object), dtype='uint8')
#     # print(setted_labels, load_mask_labels)
#     labels = setted_labels + load_mask_labels
#     interecteds = torch.from_numpy(all_to_onehot(interecteds, labels)).float()  ## [3, 4, 12, 0 0 0 0 ,,, 0]
#     mask_shape = interecteds.shape
#
#     # interecteds = aggregate_sbg(interecteds, keep_bg=True, hard=True)
#     interecteds = interecteds.unsqueeze(2)
#
#     if debug:
#         print("[DEBUG in run_image_instance]")
#         # class_list = np.unique(voc_array[0])
#         # print("Class List :", class_list)
#         print("Mask Tensor Shape :", mask_shape)
#
#     return frames, interecteds


def run_image_instance(frame, model, seleteced_classes=[], gpu_id=None):
    """
    input: RGB image or images(video)

    Returns: Instance mask(segmentation) result, base on class (temporary id : 0 -> person)
    """

    # if not dynamic:  # Object Segmentation
    classes_idx = []  # 찾으려는 class index (ex) [0, 2] -> person, car)
    if seleteced_classes:
        for selected_class in seleteced_classes:
            if selected_class in coco_class_list:
                class_idx = coco_class_list.index(selected_class)
                classes_idx.append(class_idx)
    else:
        classes_idx = ['all']

    h, w, _ = frame.shape
    vis_outputs, voc_masks, voc_arrays, boxes, labels, class_in_frame, scores, ds_masks, all_info = model.run_on_image(frame, classes_idx)

    if ds_masks is None:
        empty_mask = np.zeros((h, w), dtype="uint8")
        ds_masks = Image.fromarray(empty_mask)
        ds_masks.putpalette(palette)

    all_boxes, all_labels = all_info

    if voc_masks is not None:
        full_mask = voc_masks.copy()

        for aBox, aLabel in zip(all_boxes, all_labels):
            x0, y0, x1, y1 = aBox
            xpoint = int(x0 + 30)
            ypoint = int(y0 + 10)
            info = ImageDraw.Draw(full_mask)
            info.text((xpoint, ypoint), aLabel, fill=(255, 255, 255))
    else:
        full_mask = None

    int_boxes = []
    new_boxes = []
    new_arrays = []
    new_scores = []
    new_labels = []

    if voc_arrays is not None and voc_masks is not None:
        for cnt, b in enumerate(boxes):
            box = b.astype(int)
            box = box.tolist()
            int_boxes.append(box)
        boxes = int_boxes
        # boxes = update_box_area(boxes, voc_arrays)

        object_cnt = 0

        for box, label, class_name, score in zip(boxes, labels, class_in_frame, scores):
            voc_array = voc_arrays[object_cnt]
            x0, y0, x1, y1 = box

            array = np.array(voc_array)
            mask = array[y0:y1, x0:x1]
            area = np.count_nonzero(mask)
            new_box = [x0, y0, x1, y1, area]

            width = x1 - x0
            height = y1 - y0

            if width <= 300 or height <= 300:
                small_mask = True
            else:
                new_boxes.append(new_box)
                new_arrays.append(voc_array)
                new_scores.append(score)
                new_labels.append(label)
            object_cnt += 1

    return new_arrays, voc_masks, new_labels, new_scores, new_boxes, class_in_frame, ds_masks, full_mask


# def run_video_instance(input, seleteced_classes=[], gpu_id=None):
def run_video_instance(input, output_path, seleteced_classes=[], gpu_id=None, onebyone=True):
    """
    input: RGB image or images(video)

    Returns: Instance mask(segmentation) result, base on class (temporary id : 0 -> person)
    """
    if gpu_id == -1:
        cuda_device = 'cpu'
    else:
        cuda_device = 'cuda:{}'.format(gpu_id)

    mp.set_start_method("spawn", force=True)
    args = get_parser().parse_args()
    cfg = setup_cfg(args)
    cfg = cfg.clone()
    cfg.defrost()
    cfg.MODEL.DEVICE = cuda_device
    demo = VisualizationDemo(cfg)

    # if not dynamic:  # Object Segmentation
    classes_idx = []  # 찾으려는 class index (ex) [0, 2] -> person, car)
    if seleteced_classes:
        for selected_class in seleteced_classes:
            if selected_class in coco_class_list:
                class_idx = coco_class_list.index(selected_class)
                classes_idx.append(class_idx)
    else:
        classes_idx = ['all']
    # else:  # dynamic / static Segmentation
    #     classes_idx = ['all']

    # print("Search This Classes idx : ", classes_idx)

    one_frame_masks = []
    masks_frames = []
    ds_masks_frames = []

    frames_classes = []
    frames_boxes = []
    frames_labels = []
    frames_scores = []
    frame_idxs = []
    full_masks = []
    for i, frame in enumerate(input):
        # print("Frame num : {}".format(i))

        if onebyone:
            ### image load one by one
            # imgName -> 00000.png
            imgName = (os.path.basename(frame))
            # name -> 00000 , ext -> .png
            name, _ = os.path.splitext(imgName)

            image = Image.open(frame).convert('RGB')
            frame = np.array(image, dtype=np.uint8)

        input_array = Image.fromarray(frame)
        frame = convert_PIL_to_numpy(input_array, format="BGR")
        h, w, _ = frame.shape

        # voc_masks, voc_arrays, boxes, labels, class_in_frame = demo.run_on_image(frame, classes_idx)
        vis_outputs, voc_masks, voc_arrays, boxes, labels, class_in_frame, scores, ds_masks, all_info = demo.run_on_image(frame, classes_idx)

        all_boxes, all_labels = all_info
        # print("All Boxes : ", all_boxes)
        # print("All labels : ", all_labels)

        if onebyone:
            if voc_masks is not None:
                full_mask = voc_masks.copy()

                for aBox, aLabel in zip(all_boxes, all_labels):
                    x0, y0, x1, y1 = aBox
                    xpoint = int(x0 + 30)
                    ypoint = int(y0 + 10)
                    info = ImageDraw.Draw(full_mask)
                    info.text((xpoint, ypoint), aLabel, fill=(255, 255, 255))
            else:
                full_mask = None

            fMask_save_path = output_path + "/full_mask/"
            os.makedirs(fMask_save_path, exist_ok=True)

            if voc_masks is None:
                fMask = np.zeros((h, w), dtype="uint8")
                fMask = Image.fromarray(fMask)
            else:
                fMask = full_mask
            # fMask.save(fMask_save_path + '/{:05d}.png'.format(name))
            fMask.save(fMask_save_path + '/%s.png' %name)

            dsMask_save_path = output_path + "/dynamic_mask/"
            os.makedirs(dsMask_save_path, exist_ok=True)
            if ds_masks is None:
                mask = np.zeros((h, w), dtype="uint8")
                mask = Image.fromarray(mask)
            else:
                mask = ds_masks
            # mask.save(dsMask_save_path + '/{:05d}.png'.format(name))
            mask.save(dsMask_save_path + '/%s.png' %name)
        else:
            if voc_masks is not None:
                full_mask = voc_masks.copy()

                for aBox, aLabel in zip(all_boxes, all_labels):
                    x0, y0, x1, y1 = aBox
                    xpoint = int(x0 + 30)
                    ypoint = int(y0 + 10)
                    info = ImageDraw.Draw(full_mask)
                    info.text((xpoint, ypoint), aLabel, fill=(255, 255, 255))

                full_masks.append(full_mask)
            else:
                full_masks.append(None)

            if ds_masks is None:
                empty_mask = np.zeros((h, w), dtype="uint8")
                ds_masks = Image.fromarray(empty_mask)
                ds_masks.putpalette(palette)
                if onebyone == False:
                    ds_masks_frames.append(ds_masks)
            else:
                if onebyone == False:
                    ds_masks_frames.append(ds_masks)

        if voc_arrays is not None and voc_masks is not None:
            int_boxes = []
            new_boxes = []
            new_arrays = []
            new_scores = []
            new_labels = []

            for cnt, b in enumerate(boxes):
                box = b.astype(int)
                box = box.tolist()
                int_boxes.append(box)
            boxes = int_boxes
            # boxes = update_box_area(boxes, voc_arrays)

            object_cnt = 0
            full_mask = None

            for box, label, class_name, score in zip(boxes, labels, class_in_frame, scores):
                voc_array = voc_arrays[object_cnt]
                x0, y0, x1, y1 = box

                array = np.array(voc_array)
                mask = array[y0:y1, x0:x1]
                area = np.count_nonzero(mask)
                new_box = [x0, y0, x1, y1, area]

                width = x1 - x0
                height = y1 - y0

                if width <= 300 or height <= 300:  # Small Class 제외
                    small_mask = True
                else:
                    new_boxes.append(new_box)
                    new_arrays.append(voc_array)
                    new_scores.append(score)
                    new_labels.append(label)

                    if class_name not in frames_classes:
                        frames_classes.append(class_name)
                object_cnt += 1

            if new_arrays != []:
                frame_idxs.append(i)
                one_frame_masks.append(voc_masks)
                masks_frames.append(new_arrays)

                frames_labels.append(new_labels)
                frames_scores.append(new_scores)
                frames_boxes.append(new_boxes)

            # print("Labels : ", labels)
            # print("Boxes : ", boxes)
            #
            # cv2.imshow("VIS OUTPUT", vis_outputs.get_image()[:, :, ::-1])
            # cv2.waitKey(0)

            # print("Instance Result")
            # mask_frame = Image.fromarray(np.array(voc_masks))
            # mask_frame.putpalette(palette)
            # result = np.array(mask_frame) * 100
            # cv2.imshow("Instance REsult", result)
            # cv2.waitKey(0)
            # mask_frame.show()

        # frames = np.stack(one_frame_masks, axis=0)

    # return frames, frame_list, frames_labels, frames_boxes
    return masks_frames, one_frame_masks, frame_idxs, frames_labels, frames_scores, frames_boxes, frames_classes, ds_masks_frames, full_masks

def update_box_area(boxes, voc_arrays):
    new_boxes = []
    for array, box in zip(voc_arrays, boxes):
        x0, y0, x1, y1 = box
        array = np.array(array)
        mask = array[y0:y1, x0:x1]
        area = np.count_nonzero(mask)
        new_box = [x0, y0, x1, y1, area]
        new_boxes.append(new_box)
    return new_boxes

def run_images_instances_TEST(input, cursor, num_object, ids):
    """
    input: RGB image or images(video)

    Returns: mask(segmentation) result, base on class (temporary id : 0 -> person)
    """
    mp.set_start_method("spawn", force=True)
    args = get_parser().parse_args()

    cfg = setup_cfg(args)
    demo = VisualizationDemo(cfg)

    fn, h, w, ch = input.shape  # Frames / height / width / channel
    # input_array = Image.fromarray(input[cursor])
    # print("##################################")
    # print(ids)
    # print(len(ids))
    # id = ids[cursor]
    # print(id)
    # frame = convert_PIL_to_numpy(input_array, format="BGR")
    # # predictions, voc_masks = demo.run_on_image(frame)
    # predictions, voc_masks, voc_array = demo.run_on_image(frame)
    #
    # print("VOC_ARRAY ", len(voc_array))
    # mask_result = label_image_instace(voc_array, id)
    # voc_masks = mask_result

    for c in range(len(input)):
        input_array = Image.fromarray(input[c])
        print("##################################")
        print(ids)
        print(len(ids))
        id = ids[c]
        print(id)
        frame = convert_PIL_to_numpy(input_array, format="BGR")
        # predictions, voc_masks = demo.run_on_image(frame)
        predictions, voc_masks, voc_array = demo.run_on_image(frame)

        mask_result = label_image_instace(voc_array, id)
        voc_masks = mask_result
        voc_masks.save('test_result/{:05d}_2.png'.format(c))


    frame_list = []
    interected_list = []
    non_masks = np.zeros((h, w), dtype=np.uint8)

    for i in range(fn):
        if i == cursor:
            frame_list.append(np.array(voc_masks))
            interected_list.append(np.array(voc_masks))
        else:
            frame_list.append(non_masks)

    frames = np.stack(frame_list, axis=0)
    interecteds = np.stack(interected_list, axis=0)

    load_mask_labels = np.unique(interecteds[0])
    # print(load_mask_labels, "np.unique")

    if len(load_mask_labels) < num_object:
        load_mask_labels = load_mask_labels[load_mask_labels != 0]
        print(load_mask_labels)
        extra = np.zeros((num_object - len(load_mask_labels)), dtype='uint8')
        extra = np.full_like(extra, np.max(load_mask_labels) + 1)
        load_mask_labels = np.append(load_mask_labels, extra)

    setted_labels = np.zeros((num_object), dtype='uint8')
    print(setted_labels, load_mask_labels)
    labels = setted_labels + load_mask_labels
    interecteds = torch.from_numpy(all_to_onehot(interecteds, labels)).float()  ## [3, 4, 12, 0 0 0 0 ,,, 0]
    interecteds = aggregate_sbg(interecteds, keep_bg=True, hard=True)

    return frames, interecteds

def run_video_instance_TEST(input, objects, instance_frame):
    """
    input: RGB image or images(video)

    Returns: mask(segmentation) result, base on class (temporary id : 0 -> person)
    """

    # TODO : Using Tracking -> Get Frames Divide (Shot0 [frame0, frame1 ...], Shot1 ..)
    # TODO : Frame Divide Algorithm + Object Noise Remover (object which shown under 24 frames)
    # TODO : Returns : Frames Divide -> Forward to ProgressBar

    mp.set_start_method("spawn", force=True)
    args = get_parser().parse_args()

    cfg = setup_cfg(args)
    demo = VisualizationDemo(cfg)

    frames = []
    frame_length = 0
    shot_frames = []
    shot_interacteds = []

    # input_array = Image.fromarray(input[cursor])
    # frame = convert_PIL_to_numpy(input_array, format="BGR")
    # predictions, voc_masks = demo.run_on_image(frame)

    for i in range(len(input)):
        input_array = Image.fromarray(input[i])
        frame = convert_PIL_to_numpy(input_array, format="BGR")
        frames.append(frame)
        frame_length += 1
        run_image_instance(frame)

    # TEMPORARY NOT USE #
    box_info, iou_info, id_info = demo.run_on_video(frames)
    # print("Box info : ", box_info)

    # selected_image = select_image(frame_length, box_info, iou_info)
    # for i in selected_image:
    #     shot_frame, shot_interacted = run_images_instances(input, i, objects)
    #     shot_frames.append(shot_frame)
    #     shot_interacteds.append(shot_interacted)

    # shot_frames, shot_interacteds = run_images_instances(input, instance_frame, objects)
    shot_frames, shot_interacteds = run_image_instance(input, instance_frame, objects, id_info)

    # print("SELELCTED_IMAGE :", selected_image)
    # return shot_frames, shot_interacteds, selected_image
    return shot_frames, shot_interacteds


def run_tracking(input, frame_idxs, labels, scores, stats, debug):
    # mp.set_start_method("spawn", force=True)
    # args = get_parser().parse_args()
    #
    # cfg = setup_cfg(args)
    # demo = VisualizationDemo(cfg)

    frames = []
    for idx in frame_idxs:
        input_array = Image.fromarray(input[idx])
        frame = convert_PIL_to_numpy(input_array, format="BGR")
        frames.append(frame)

    # shot_cut_list, label_seed_infs, frames_id_infs, frames_box_infs = demo.run_on_video_tracking(frames, labels, scores, stats, frame_idxs, debug)
    shot_cut_list, label_seed_infs, frames_id_infs, frames_box_infs = VideoTracker().run_on_video_tracking(frames, labels, scores, stats, frame_idxs, debug)

    # return shot_cut_list, label_seed_infs, frames_id_infs, frames_box_infs
    return shot_cut_list, label_seed_infs, frames_box_infs

    # mask_result = label_image_instace(voc_array, id)

# def run_image_instance_for_video(input, seleted_cursor, num_object):
#     """
#     input: RGB image or images(video)
#            Selected image frame number (Which max iou values in each shot)
#
#     Returns: mask(segmentation) result, base on class (temporary id : 0 -> person)
#     """
#     mp.set_start_method("spawn", force=True)
#     args = get_parser().parse_args()
#
#     cfg = setup_cfg(args)
#     demo = VisualizationDemo(cfg)
#
#     fn, h, w, ch = input.shape  # Frames / height / width / channel
#
#     interected_list = []
#     non_masks = np.zeros((h, w), dtype=np.uint8)
#     for i in range(fn):
#         if i in seleted_cursor:
#             input_array = Image.fromarray(input[i])
#             frame = convert_PIL_to_numpy(input_array, format="BGR")
#             predictions, voc_masks = demo.run_on_image(frame)
#
#             interected_list.append(np.array(voc_masks))
#         else:
#             interected_list.append(non_masks)
#
#     interecteds = np.stack(interected_list, axis=0)
#
#     load_mask_labels = np.unique(interecteds[0])  # [0, 1, 2, 7, 8, 11]
#     print(load_mask_labels)
#
#     if len(load_mask_labels) < num_object:
#         load_mask_labels = load_mask_labels[load_mask_labels != 0]
#         print(load_mask_labels)
#         extra = np.zeros((num_object - len(load_mask_labels)), dtype='uint8')
#         extra = np.full_like(extra, np.max(load_mask_labels) + 1)
#         load_mask_labels = np.append(load_mask_labels, extra)
#
#     setted_labels = np.zeros((num_object), dtype='uint8')
#     labels = setted_labels + load_mask_labels
#     interecteds = torch.from_numpy(all_to_onehot(interecteds, labels)).float()  ## [3, 4, 12, 0 0 0 0 ,,, 0]
#     interecteds = aggregate_sbg(interecteds, keep_bg=True, hard=True)
#
#     return interecteds