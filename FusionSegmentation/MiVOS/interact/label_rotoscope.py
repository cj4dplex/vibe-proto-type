import cv2
import os, glob
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
from vos_util.palette import get_binary_map, pal_color_map

palette = pal_color_map()

def convertRGBtoRGBA(image, mask):
    alpha = mask
    rgba = cv2.cvtColor(image, cv2.COLOR_RGB2RGBA)
    rgba[:, :, 3] = alpha

    rgba_arr = np.array(rgba)

    return rgba_arr

def get_roi_area(mask):
    binary_mask = get_binary_map(mask)
    cnt, labels, stats, centroids = cv2.connectedComponentsWithStats(binary_mask)
    # stats = [x, y, w, h]
    # print("---------------------------------------")
    # print(stats)

    max_size = 0
    for i in range(1, len(stats)):
        size = stats[i][4]
        if max_size < size:
            max_size = size
            get_label = i

    box_area = stats[get_label]
    # print("---------------------------------------")
    # print("USE This stat", box_area)
    x = box_area[0]
    y = box_area[1]
    w = box_area[2]
    h = box_area[3]
    s = box_area[4]

    centroid = centroids[get_label]

    new_box_area = [x, y, w, h]
    size = s

    return new_box_area

def crop_roi(mask, image, box):
    x = box[0]
    y = box[1]
    w = box[2]
    h = box[3]

    crop_mask = mask[y: y + h, x: x + w]
    crop_img = image[y: y + h, x: x + w]

    return crop_mask, crop_img

def get_masks_images_labels(mask_path, image_path):
    mask_files = sorted(glob.glob(os.path.join(mask_path, '*.png')))
    img_files = sorted(glob.glob(os.path.join(image_path, '*.png')))

    masks = []
    images = []

    for m in range(len(mask_files)):
        mask_file = mask_files[m]
        masks.append(np.array(Image.open(mask_file)))

        img_file = img_files[m]
        images.append(np.array(Image.open(img_file)))

    stack_masks = np.stack(masks, 0)
    labels = np.unique(stack_masks)
    labels = list(labels[labels != 0])

    return masks, images, labels

# TODO : Get label mask fuction -> make more fast
def get_label_mask(masks, label):
    label_masks = []
    label_mask_frames = []

    copy_masks = np.copy(masks)
    height, width = copy_masks[0].shape

    for i, copy_mask in enumerate(copy_masks):
        if label in copy_mask:
            # print("Label :", label, " in this mask frame :", i)
            for h in range(height):
                for w in range(width):
                    if copy_mask[h][w] != label:
                        copy_mask[h][w] = 0

            label_masks.append(copy_mask)
            label_mask_frames.append(i)

        if i % 10 == 0:
            print("Processing get_label_mask : (", i, "/", len(masks), ")")

    return label_masks, label_mask_frames

def label_in_frame(masks, frames, label, save_dir):
    boxes = []
    print("------------------------------------------")
    print("Processing Get Label :", label)
    label_str = str(label)

    print("Frames list input : ", len(frames))
    print("Masks list input :", len(masks))
    label_masks, label_mask_frames = get_label_mask(masks, label)
    print("label_masks length", len(label_masks))
    print("------------------------------------------")
    print("Processing Get ROI")
    for label_mask in label_masks:
        box = get_roi_area(label_mask)
        boxes.append(box)
    print("boxes length", len(boxes))
    print("------------------------------------------")
    print("Processing Crop ROI and Save")

    rgba_dir = os.path.join(save_dir, label_str + '_single')
    os.makedirs(rgba_dir, exist_ok=True)

    for i in range(len(label_masks)):
        label_mask = label_masks[i]
        label_img = frames[i]
        box = boxes[i]

        crop_mask, crop_img = crop_roi(label_mask, label_img, box)

        binary_mask = get_binary_map(crop_mask)
        rgba_data = convertRGBtoRGBA(crop_img, binary_mask)
        plt.imsave(os.path.join(save_dir, '{:05d}.png'.format(i)), rgba_data)

    boxes.clear()
    # label_masks.clear()
    # label_mask_frames.clear()

def label_in_frame_loaded(masks_dir, images_dir, save_dir):
    masks, images, labels = get_masks_images_labels(masks_dir, images_dir)
    print("Labels :", labels)

    for i, label in enumerate(labels):
        boxes = []
        print("------------------------------------------")
        print("Processing Get Label :", label)
        label_str = str(label)
        rgba_dir = os.path.join(save_dir, 'test_labeled_rgba/' + label_str)
        os.makedirs(rgba_dir, exist_ok=True)

        print("Masks list input :", len(masks))
        label_masks, label_mask_frames = get_label_mask(masks, label)
        print("label_masks length", len(label_masks))
        print("------------------------------------------")
        print("Processing Get ROI")
        for i, label_mask in enumerate(label_masks):
            box = get_roi_area(label_mask)
            boxes.append(box)
        print("boxes length", len(boxes))
        print("------------------------------------------")
        print("Processing Crop ROI and Save")
        for i, f in enumerate(label_mask_frames):
            label_mask = label_masks[i]
            label_img = images[f]
            box = boxes[i]

            crop_mask, crop_img = crop_roi(label_mask, label_img, box)

            binary_mask = get_binary_map(crop_mask)
            rgba_data = convertRGBtoRGBA(crop_img, binary_mask)
            plt.imsave(os.path.join(rgba_dir, '{:05d}.png'.format(f)), rgba_data)

        boxes.clear()
        # label_masks.clear()
        # label_mask_frames.clear()

if __name__ == '__main__':
    images_dir = 'T:/MGD/Clip/segmentation/47/origin/'
    masks_dir = 'T:/MGD/Clip/segmentation/47/mask/'
    save_dir = 'T:/MGD/Clip/segmentation/47/'

    label_in_frame_loaded(masks_dir, images_dir, save_dir)
