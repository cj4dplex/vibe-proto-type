
def get_index(custom_list, coco_list):
    coco_custom_idx = []
    for custom in custom_list:
        if custom in coco_list:
            idx = coco_list.index(custom)
            coco_custom_idx.append(idx)
    return coco_custom_idx

coco_class_list = ['person', 'bicycle', 'car', 'motorcycle', 'airplane',
                   'bus', 'train', 'truck', 'boat', 'traffic light',
                   'fire hydrant', 'stop sign','parking meter', 'bench', 'bird',
                   'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant',
                   'bear', 'zebra', 'giraffe', 'backpack', 'umbrella',
                   'handbag', 'tie', 'suitcase', 'frisbee', 'skis',
                   'snowboard', 'sports ball', 'kite', 'baseball bat', 'baseball glove',
                   'skateboard', 'surfboard', 'tennis racket', 'bottle', 'wine glass',
                   'cup', 'fork', 'knife', 'spoon', 'bowl',
                   'banana', 'apple', 'sandwich', 'orange', 'broccoli',
                   'carrot', 'hot dog', 'pizza', 'donut', 'cake',
                   'chair', 'couch', 'potted plant', 'bed', 'dining table',
                   'toilet', 'tv', 'laptop', 'mouse', 'remote', 'keyboard',
                   'cell phone', 'microwave', 'oven', 'toaster', 'sink',
                   'refrigerator', 'book', 'clock', 'vase', 'scissors',
                   'teddy bear', 'hair drier', 'toothbrush']

coco_dynamic_list = ['person', 'bicycle', 'car', 'motorcycle', 'airplane', 'bus', 'train', 'truck', 'boat', 'bird',
                     'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant', 'bear', 'zebra', 'giraffe', 'backpack',
                     'umbrella', 'handbag', 'tie', 'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball', 'kite',
                     'baseball bat', 'baseball glove', 'skateboard', 'surfboard', 'tennis racket', 'bottle',
                     'wine glass', 'cup', 'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple', 'sandwich', 'orange',
                     'broccoli', 'carrot', 'hot dog', 'pizza', 'donut', 'cake', 'laptop', 'mouse', 'remote', 'keyboard',
                     'cell phone', 'book', 'clock', 'vase', 'scissors', 'teddy bear', 'hair drier', 'toothbrush']

coco_static_list = ['traffic light', 'fire hydrant', 'stop sign','parking meter', 'bench', 'chair', 'couch',
                   'potted plant', 'bed', 'dining table', 'toilet', 'tv', 'microwave', 'oven', 'toaster', 'sink',
                   'refrigerator']

coco_dynamic_idx = get_index(coco_dynamic_list, coco_class_list)
coco_static_idx = get_index(coco_static_list, coco_class_list)
