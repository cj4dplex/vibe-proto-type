===============================================================================================================================
[FusionSegmentation] - InputPath / OutputPath / Mode(GUI off / Display / GUI on) / Save Mode / Debug Mode(Debug off / Debug on)

[Input Arg]
#    ImgPath (비디오 경로) : EX) imgPath = 'D:/MGD_TEST_DATA/RND_TST_47.mov' [Single Video File]
#    * TODO_LIST : RGB Images Read Func 추가

[Output Arg]
#    outputPath (Mask 결과물 and RGBA 결과물 저장 경로) : EX) 레이블 결과 -> [1, 2]인 경우
#        |
#        |----- mask
#        |       |----- all
#        |       |       |----- 1 (Label 1이 검출된 구간 내 모든 labels의 mask) [mask format -> binary(0, 255)]
#        |       |       |----- 2 -> outputPath/mask/all/2
#        |       |
#        |       |----- single
#        |               |----- 1 (Label 1이 검출된 구간 내 Label 1의 mask) [mask format -> binary(0, 255)]
#        |               |----- 2 -> outputPath/mask/single/2
#        |
#        |----- rgba
#                |----- all
#                |       |----- 1 -> outputPath/rgba/all/1
#                |       |----- 2 (Label 2가 검출된 구간 내 모든 Labels의 rotoscope result)
#                |
#                |----- single
#                        |----- 1 -> outputPath/rgba/single/1
#                        |----- 2 (Label 2가 검출된 구간 내 Label 2의 rotoscope result)

[GUI Mode Arg]
#    mode = 0 (Non-GUI Mode : GUI 비활성화, 결과물 자동 생성)
#    mode = 1 (Display Mode : GUI 활성화, 결과물 자동 생성)
#    mode = 2 (GUI Mode : GUI 활성화, 결과물 수동 생성)

[Save Arg]
#    save_mode = 0 -> 저장 결과물 : (mask-single, rgba-single)                         [Only Save Single Object]
#    save_mode = 1 -> 저장 결과물 : (mask-all, rgba-all)                               [Only Save All Objects]
#    save_mode = 2 -> 저장 결과물 : (mask-all, mask-single, rgba-all, rgba-single)     [Save Single + All Objects]

[Debug Mode Arg]
#    debug_mode = 0 (Debug 비활성화 -> Non Debug Print)
#    debug_mode = 1 (Debug 활성화 -> Debug Print)
===============================================================================================================================

[Patch Note]
* 2021.08.17
  (Update)
  - GUI 모드 기능만 추가
  - Debug 모드 추가
  - Mask Save Default Format 변경 (Pascal VOC -> Binary)
  (TO-DO)
  : Display 모드 전용 GUI 추가
  : 다른 Scene 추가 테스트 진행
    - Instance Segmentation 재확인 필요
    - Tracking Result 재확인 필요
* 2021.08.19
  : Tracking 알고리즘 개선 필요 (TLD, GOTURN 등 객체 소멸 시 재추적 가능한 알고리즘 구현 or 적용)
  : Tracking 알고리즘 구현 완료 (타 알고리즘 사용 X -> 자체 구현 [IOU 활용])

* 2022.01.12
  : Face Search 알고리즘 추가 (환경설정 관련 -> FaceSearch/README.txt 참조)
  : FusionSegmentation 알고리즘 수정
    - PyQT5 기반 알고리즘 (Auto Segmentation 작업 후, 다음 경로로 이동을 위한 pass 추가)

* 2022.02.09
  : Face Search 알고리즘 처리 속도 기록
    - 282개 폴더 (000개 이미지) 처리 -> 218분 (13081초)