import os
import numpy as np
import time, math

# import interactive_gui as fs
from folder_util import _make_imageFolderMap
# import run_faceSearch
# from query_util import read_query_tables

def fusionSegmentation(imgPath, outputPath, mode, save_mode, debug_mode, exe_mode):
    path_result = None

    # Create FolderMap
    folderMap = []
    additional_forbidden_words = [".mxf", ".DS_Store", ".aaf"]
    input_folderMap = _make_imageFolderMap(imgPath, folderMap, additional_forbidden_words)
    print("### Make Input FolderMap is Finished...!")

    # Run Fusion Segmentation -> Face Searching
    all_start = time.time()
    seg_times = 0
    face_times = 0
    img_cnts = 0

    cnt = 0
    extended_scenes = []
    for i, scene_path in enumerate(input_folderMap):
        if len(os.listdir(scene_path)) > 100000000000000:
            # 개수 초과하는 경우, 해당 경로 리스트업할 것
            print("Warning : OOM Occur (Over than 120 frames) !!")
            print("Except scene path : {} (img cnt : {})".format(scene_path, len(os.listdir(scene_path))))
            extended_scenes.append(scene_path)
            pass
        else:
            print("# Processing scene path : {} (img cnt : {})".format(scene_path, len(os.listdir(scene_path))))

            ################# Face Segmentation 시작 ######################
            seg_s = time.time()
            scene_name = os.path.basename(os.path.dirname(scene_path))
            fusionSeg_result_path = outputPath + "/" + scene_name + "/" + os.path.basename(scene_path)
            # instance + tracking / VOS 모듈 분리할 것
            run_fusionSegmentation(scene_path, fusionSeg_result_path, mode, save_mode, debug_mode, exe_mode)
            seg_time = time.time() - seg_s
            print(f"{seg_time:.5f} sec")
            print("### Fusion Segmentation is Finished ...! ###")
            ################# Face Segmentation 종료 ######################

            ## FACESEARCH ##
            # input : Fusion Segmentation Result Path
            #         Face Search save Result Path
            ################# Face Search 시작 ######################

            face_s = time.time()
            faceSearch_result_path = outputPath + "/searching_result"

            yaw_table = [-90, -45, 0, 45, 90]
            pitch_table = [-45, 0, 45]
            deviation = 15
            angle_table = [yaw_table, pitch_table, deviation]

            # Create FolderMap
            folderMap = []
            additional_forbidden_words = ["chroma_key", "mask", "file_list_summery", "Sorted", "Sorted_Sample", ".txt"]
            input_folderMap = _make_imageFolderMap(fusionSeg_result_path, folderMap, additional_forbidden_words)
            print("### Make Input FolderMap is Finished...!")
            os.makedirs(faceSearch_result_path, exist_ok=True)

            if input_folderMap:
                if cnt <= 0:
                    query_tables = []
                    query_table = [None for i in range(len(yaw_table) * len(pitch_table))]
                    result_hash_table = run_faceSearch.main(input_folderMap, faceSearch_result_path,
                                                             query_tables, query_table, angle_table)
                else:
                    query_tables = read_query_tables(faceSearch_result_path)
                    query_table = [None for i in range(len(yaw_table) * len(pitch_table))]
                    result_hash_table = run_faceSearch.main(input_folderMap, faceSearch_result_path,
                                                             query_tables, query_table, angle_table)

                cnt += 1
                # Convert List -> Numpy -> CSV file (Result File)
                # result = np.array(result_hash_table)
                result = result_hash_table
                print("###########################")
                print("### RESULT ###")
                print(result)
                os.makedirs(faceSearch_result_path + "/temp", exist_ok=True)
                if result:
                    np.save(faceSearch_result_path + "/temp/path_result_{}".format(scene_name + "_" + os.path.basename(scene_path)), result)
                print("### path RESULT ###")
                print(path_result)
                if path_result is None and result == []:
                    path_result = None
                elif path_result is None and result != []:
                    path_result = result
                elif result:
                    # path_result = np.concatenate((path_result, result), axis=0)
                    path_result = np.vstack((path_result, result))
                    # path_result = path_result.extend(result)

                print("###########################")
                print("### PATH RESULT ###")
                print(path_result)

                if path_result is not None:
                    # np_path_result = np.array(path_result)
                    # np.savetxt(faceSearch_result_path + "/" + scene_name + "_result.csv", result, fmt="%s", delimiter=",")
                    np.savetxt(faceSearch_result_path + "/path_result.csv", path_result, fmt="%s", delimiter=",")


                ################# Face Search 임시 주석 종료 ######################

                # 알고리즘 시간 측정용 (단일 이미지 폴더에 대한 값)

                face_time = time.time() - face_s
                print(f"{face_time:.5f} sec")

            seg_times += seg_time
            face_times += face_times

            img_cnts += len(os.listdir(scene_path))
    # run_faceSearch.get_classes_in_shot(outputPath)
    # run_faceSearch.relabeling_data(outputPath)
    print("### All Folders Finish Time")
    all_time = time.time() - all_start
    print(f"{all_time:.5f} sec")

    # 시간 측정용
    # time_records = [str(img_cnts), f"{seg_times:.5f} sec", f"{face_times:.5f} sec", f"{all_time:.5f} sec"]
    # np.savetxt(outputPath + "/all_time_record.txt", time_records, delimiter=" ", fmt="%s")
    if extended_scenes:
        np.savetxt(outputPath + "/over_120frames_scenes.txt", extended_scenes, delimiter=" ", fmt="%s")


def run_fusionSegmentation(imgPath, outputPath, mode, save_mode, debug_mode, exe_mode):
    # Arguments parsing
    parser = fs.argparse.ArgumentParser()
    parser.add_argument('--prop_model', default=fs.fusionSegmentationPath + 'saves/propagation_model.pth')  # propagation model (Video Segmentation)
    parser.add_argument('--fusion_model', default=fs.fusionSegmentationPath + 'saves/fusion.pth')  # fusion model
    parser.add_argument('--s2m_model', default=fs.fusionSegmentationPath + 'saves/s2m.pth')  # s2m model
    parser.add_argument('--fbrs_model', default=fs.fusionSegmentationPath + 'saves/resnet50_dh128_lvis.pth')  # fbrs model (interactive)

    parser.add_argument('--mode', default=mode)
    parser.add_argument('--debug_mode', default=debug_mode)
    parser.add_argument('--save_mode', default=save_mode)
    parser.add_argument('--exe_mode', default=exe_mode)

    parser.add_argument('--images', default=imgPath, help='Folders containing input images.')
    parser.add_argument('--output', default=outputPath, help='Folders containing output masks.')

    parser.add_argument('--num_objects', help='Default: 1 if no masks provided, masks.max() otherwise', type=int, default=10)
    parser.add_argument('--mem_freq', default=5, type=int)
    parser.add_argument('--masks', help='Optional, Ground truth masks')
    parser.add_argument('--no_amp', help='Turn off AMP', action='store_true')
    args = parser.parse_args()

    # Segmentation
    fs.main(args)
