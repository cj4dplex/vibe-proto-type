import glob
import os.path

import cv2
import numpy as np
from tabulate import tabulate


def load_mask(mask_dir):
    mask_files = glob.glob(os.path.join(mask_dir, "*.png"))
    if len(mask_files) == 0:
        mask_files = glob.glob(os.path.join(mask_dir, "*.jpg"))

    masks = []
    for mask_file in mask_files:
        mask = cv2.imread(mask_file, 0)
        if np.unique(mask).tolist() != [0, 255]:
            np.where(mask > 0, 255, mask)
        masks.append(mask)
    return masks


def mask_to_boundary(mask, ratio=1):
    """
    Convert binary mask to boundary mask.
    :param mask (numpy array, uint8): binary mask
    :param dilation_ratio (float): ratio to calculate dilation = dilation_ratio * image_diagonal
    :return: boundary mask (numpy array)
    """
    new_mask = mask.copy()
    if ratio == 0:
        return new_mask, new_mask

    else:
        if ratio > 1:
            k = 3 * ratio
        else:
            k = 3

        kernel = np.ones((k, k), dtype=np.uint8)
        new_mask_dilate = cv2.dilate(new_mask, kernel)
        new_mask_erode = cv2.erode(new_mask, kernel)
        return new_mask_dilate, new_mask_erode


def boundary_iou(gt, dt, stage):
    """
    Compute boundary iou between two binary masks.
    :param gt (numpy array, uint8): binary mask
    :param dt (numpy array, uint8): binary mask
    :param dilation_ratio (float): ratio to calculate dilation = dilation_ratio * image_diagonal
    :return: boundary iou (float)
    """
    gt_dilate_boundary, gt_erode_boundary = mask_to_boundary(gt, ratio=stage)
    dt_boundary, _ = mask_to_boundary(dt, ratio=0)

    # gt_rgb = cv2.cvtColor(gt_dilate_boundary, cv2.COLOR_GRAY2BGR)
    # gt_rgb = cv2.cvtColor(gt_erode_boundary, cv2.COLOR_GRAY2BGR)
    # gt_rgb[np.where((gt_rgb==[255, 255, 255]).all(axis=2))] = [0, 0, 255]
    # dt_rgb = cv2.cvtColor(dt_boundary, cv2.COLOR_GRAY2BGR)

    # cv2.imshow("GT", gt_rgb)
    # cv2.imshow("DT", dt_rgb)
    # cv2.imshow("MERGE", gt_rgb + dt_rgb)
    # cv2.waitKey()

    dilate_intersection = ((gt_dilate_boundary * dt_boundary) > 0).sum()

    erode_intersection = ((gt_erode_boundary * dt_boundary) > 0).sum()
    gt_erode_size = np.count_nonzero(gt_erode_boundary)
    inner_error_size = gt_erode_size - erode_intersection

    union = ((gt_dilate_boundary + dt_boundary) > 0).sum()

    # return dilate_intersection, erode_intersection
    return dilate_intersection, inner_error_size


def boundary_rate_verification(gt_masks, pred_masks, stages):
    prev_intersections = {}
    prev_inner_error_sizes = {}

    inner_errors = 0
    for idx, stage in enumerate(stages):
        intersections = []
        inner_error_sizes = []

        if idx == 0:
            pred_mask_sizes = []

        for gt_mask, pred_mask in zip(gt_masks, pred_masks):
            dilate_intersection, inner_error_size = boundary_iou(gt_mask, pred_mask, stage)
            if idx == 0:
                pred_mask_sizes.append(np.count_nonzero(pred_mask))

            inner_error_sizes.append(inner_error_size)
            intersections.append(dilate_intersection)

        prev_intersections[idx] = intersections
        prev_inner_error_sizes[idx] = inner_error_sizes

    boundary_iou_results = []
    outer_boundary_results = []
    inner_boundary_results = []
    for idx, stage in enumerate(stages):
        back_idx = len(stages) - 1 - idx

        out_boundary_rates = 0
        inner_boundary_error_rates = 0
        intersections = prev_intersections[idx]
        inner_error_sizes = prev_inner_error_sizes[back_idx]
        total_inner_error_size = prev_inner_error_sizes[0]

        for cnt, inner_error in enumerate(inner_error_sizes):

            if back_idx < len(stages) - 1:
                prev_inner_error_size = prev_inner_error_sizes[back_idx + 1][cnt]
            else:
                prev_inner_error_size = None

            if prev_inner_error_size is None:
                if inner_error == 0 or total_inner_error_size[cnt] == 0:
                    inner_boundary_error_rate = 0
                else:
                    inner_boundary_error_rate = inner_error / total_inner_error_size[cnt]
            else:
                if (inner_error - prev_inner_error_size) == 0 or total_inner_error_size[cnt] == 0:
                    inner_boundary_error_rate = 0
                else:
                    inner_boundary_error_rate = (inner_error - prev_inner_error_size) / total_inner_error_size[cnt]

            inner_boundary_error_rates += inner_boundary_error_rate

        for cnt, intersection in enumerate(intersections):
            pred_size = pred_mask_sizes[cnt]
            if idx > 0:
                prev_intersection = prev_intersections[idx - 1][cnt]
            else:
                prev_intersection = None

            if prev_intersection is None:
                out_boundary_rate = intersection / pred_size
            else:
                out_boundary_rate = (intersection - prev_intersection) / pred_size

            out_boundary_rates += out_boundary_rate


            # inner_error_rate = round((inner_errors / len(intersections)), 3)

        out_results = round((out_boundary_rates / len(intersections)), 3)
        inner_results = round((inner_boundary_error_rates / len(inner_error_sizes)), 3)
        outer_boundary_results.append([idx, out_results])
        inner_boundary_results.append(inner_results)

    print("=====" * 12)
    # print(tabulate(boundary_iou_results, headers=['Dilation', 'Boundary IoU Rate']))
    print("{:<10} {:<20} {:<10} {:<10}".format('Dilation', 'Boundary Outer ', 'Erosion', 'Boundary Inner'))
    print("=====" * 12)

    idx = 0
    for outer_results, inner_results in zip(outer_boundary_results, inner_boundary_results):
        stage, outer_boundary = outer_results
        inner_boundary = inner_boundary_results[len(inner_boundary_results) - 1 - idx]
        print("{:<10} {:<20} {:<10} {:<10}".format(stage, outer_boundary, stage, inner_boundary))
        if stage == 0:
            print("-----" * 12)
        idx += 1
    print("=====" * 12)

    # print("Inner Error Rate : {}".format(inner_error_rate))
    # print("=====" * 6)


def boundary_iou_verification(gt_masks, pred_masks, dilations):
    prev_intersections = {}
    prev_unions = {}

    for idx, dilation in enumerate(dilations):
        intersections = []
        unions = []
        for gt_mask, pred_mask in zip(gt_masks, pred_masks):
            intersection, union = boundary_iou(gt_mask, pred_mask, dilation)
            intersections.append(intersection)
            unions.append(union)

        prev_intersections[idx] = intersections
        prev_unions[idx] = unions

    sum_it = 0
    unions = prev_unions[len(dilations) - 1]
    for idx, dilation in enumerate(dilations):
        iou_scores = 0
        intersections = prev_intersections[idx]

        for cnt, intersection in enumerate(intersections):
            print(cnt)
            union = unions[cnt]
            if idx > 0:
                prev_intersection = prev_intersections[idx - 1][cnt]
            else:
                prev_intersection = None

            if prev_intersection is None:
                iou_score = intersection / union
            else:
                iou_score = (intersection - prev_intersection) / union
            iou_scores += iou_score

            print("#####" * 10)
            print("Dilation : {}".format(dilation))
            # result = round((iou_scores / cnt) * 100, 3)
            result = iou_scores / (cnt + 1)
            sum_it += result
            print("Boundary IoU : {}".format(result))
    # print("TOTAL : ", sum_it)



def main():
    # pred_mask_dir = r"E:\DATASET\SEG\DAVID\sample\PRED\blackswan"
    pred_mask_dir = r"E:\DATASET\SEG\DAVID\sample\PRED_NUKEY\blackswan"
    # pred_mask_dir = r"E:\DATASET\SEG\DAVID\sample\GT\blackswan"
    gt_mask_dir = r"E:\DATASET\SEG\DAVID\sample\GT\blackswan"
    pred_masks = load_mask(pred_mask_dir)
    gt_masks = load_mask(gt_mask_dir)

    # full_pred_dir = r"E:\DATASET\SEG\DAVID\PRED\blackswan"
    # full_gt_dir = r"E:\DATASET\SEG\DAVID\GT\blackswan"
    # pred_masks = load_mask(full_pred_dir)
    # gt_masks = load_mask(full_gt_dir)

    stages = [0, 1, 2, 3, 4, 5]
    # boundary_iou_verification(gt_masks, pred_masks, dilations)
    boundary_rate_verification(gt_masks, pred_masks, stages)

main()
