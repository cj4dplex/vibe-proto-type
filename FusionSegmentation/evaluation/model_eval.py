import os
import cv2
import numpy as np

from seg_eval import batch
from interact.instance import load_instance_model as load_adelaidet


def load_images(path, mode):
    files = os.listdir(path)
    imgs = []
    for file in files:
        if file == "Thumbs.db":
            pass
        else:
            imgPath = os.path.join(path, file)
            img = cv2.imread(imgPath, mode)
            imgs.append(img)
    return imgs


def eval_pixcelAcc(model, inputs, trues):
    saveDir = "D:/pred_sample/blackswan"

    classes_idx = ['all']
    files = []
    cnt = 0
    for input, true in zip(inputs, trues):
        _, voc_masks, voc_arrays, boxes, labels, _, _, _, _ = model.run_on_image(input, classes_idx)
        np_masks = np.array(voc_masks)
        pred = np.where(np_masks != 0, 255, np_masks)

        file = (true, pred)
        files.append(file)

        cv2.imwrite(saveDir + "/{:05d}.png".format(cnt), pred)
        cnt += 1

    batch_result = batch(files)
    print(batch_result)


if __name__ == '__main__':
    inputDir = r"V:\0_Dataset\1_CommonDataset\DAVID\DAVIS2016\JPEGImages\1080p\blackswan"
    truesDir = r"V:\0_Dataset\1_CommonDataset\DAVID\DAVIS2016\Annotations\1080p\blackswan"

    inputs = load_images(inputDir, 1)
    y_trues = load_images(truesDir, 0)

    # Load Adelaidet Model to Eval
    model = load_adelaidet(0)

    eval_pixcelAcc(model, inputs, y_trues)
