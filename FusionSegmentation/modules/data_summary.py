import csv
import json
import os.path

import numpy as np


def read_csv(csvPath):
    # CSV 파일 Read -> List 타입
    np_csv = []
    f = open(csvPath, 'r', encoding='utf-8')
    rdr = csv.reader(f)
    for line in rdr:
        np_csv.append(line)
    f.close()
    return np_csv


def read_json(jsonPath):
    # Json 파일 Read -> 저장 형태
    with open(jsonPath, 'r') as f:
        json_data = json.load(f)
    return json_data


def add_box_frame_info(pathData, infoPath):
    boxDataPath = infoPath + "/object_box.json"
    frameDataPath = infoPath + "/object_frames.json"

    boxData = read_json(boxDataPath)
    frameData = read_json(frameDataPath)
    new_result = []

    for pd in pathData:
        cls, pth = pd
        pth_cls = os.path.basename(pth)
        print("#", pth_cls)

        minSize, maxSize = boxData[pth_cls]
        if minSize is None:
            minW = None
            minH = None
        else:
            minW, minH = minSize
        if maxSize is None:
            maxW = None
            maxH = None
        else:
            maxW, maxH = maxSize

        frames = frameData[pth_cls]
        if len(frames) > 1:
            startFrame = frames[0][0]
            endFrame = frames[len(frames) - 1][1]
        else:
            startFrame = frames[0][0]
            endFrame = frames[0][1]

        new_data = [cls, minW, minH, maxW, maxH, startFrame, endFrame, pth]
        new_result.append(new_data)
    return new_result

def data_sum(path, dataPath):
    boxDataPath = dataPath + "/object_box.json"
    frameDataPath = dataPath + "/object_frames.json"
    csvPath = path + "/path_result.csv"

    np_csv = read_csv(csvPath)
    boxData = read_json(boxDataPath)
    frameData = read_json(frameDataPath)

    new_csv = []
    category = ["Classified",
                "min width", "min height",
                "max width", "max height",
                "start frame", "end frame",
                "Image Path"]
    new_csv.append(category)

    for data in np_csv:
        cls, pth = data
        pth_cls = os.path.basename(pth)
        print("#", pth_cls)

        minSize, maxSize = boxData[pth_cls]
        if minSize is None:
            minW = None
            minH = None
        else:
            minW, minH = minSize
        if maxSize is None:
            maxW = None
            maxH = None
        else:
            maxW, maxH = maxSize

        frames = frameData[pth_cls]
        if len(frames) > 1:
            startFrame = frames[0][0]
            endFrame = frames[len(frames) - 1][1]
        else:
            startFrame = frames[0][0]
            endFrame = frames[0][1]

        new_data = [cls, minW, minH, maxW, maxH, startFrame, endFrame, pth]
        new_csv.append(new_data)

    new_csv = np.array(new_csv)
    np.savetxt(dataPath + '/sample_fix.csv', new_csv, delimiter=",", fmt="%s")

# path = r"V:\99_sample\b-roll_sample\JW_task\trailer_instance\deepface_test\searching_result"
# dataPath = r"V:\99_sample\b-roll_sample\JW_task\trailer_instance_fixTest\bohemian"
# data_sum(path, dataPath)