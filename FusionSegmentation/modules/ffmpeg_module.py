import ffmpeg
import numpy as np


def ffmpeg_video2imageArray(videoPath):
    # Get Video Info
    probe = ffmpeg.probe(videoPath)
    video_stream = next((stream for stream in probe['streams'] if stream['codec_type'] == 'video'), None)
    width = int(video_stream['width'])
    height = int(video_stream['height'])

    # Convert Video to Numpy Array
    out, _ = (
        ffmpeg
        .input(videoPath)
        .output('pipe:', format='rawvideo', pix_fmt='rgb24')
        .run(capture_stdout=True)
    )
    frames = (
        np.frombuffer(out, np.uint8)
        .reshape([-1, height, width, 3])
    )
    # frame (np.array -> RGB Format)
    return frames
