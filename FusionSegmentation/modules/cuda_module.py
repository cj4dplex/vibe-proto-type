import pynvml as nvml

nvml.nvmlInit()


def get_cuda_device(gpu_list):
    # 메모리 여유가 가장 큰 cuda device 선택
    max_free = 50
    device_idx = 0
    for idx in gpu_list:
        h = nvml.nvmlDeviceGetHandleByIndex(idx)
        info = nvml.nvmlDeviceGetMemoryInfo(h)

        # print(f'total : {info.total * 10 ** -9}')  # GPU Total Memory (GB)
        # print(f'free  : {info.free * 10 ** -9}')   # GPU Free Memory (GB)
        # print(f'used  : {info.used * 10 ** -9}')   # GPU Using Memory (GB)

        free_memory = info.free * 10 ** -9  # GB 단위로 변경
        if max_free > free_memory:
            device_idx = idx
            max_free = free_memory
    return device_idx


def cuda_check(cuda_device=None):
    if cuda_device is not None:
        gpu_cnt = nvml.nvmlDeviceGetCount()
        gpu_list = [i for i in range(gpu_cnt)]

        if cuda_device in gpu_list:
            # 지정한 cuda device 번호가 있는 경우
            device = cuda_device
        else:
            # 지정한 cuda device 번호가 없는 경우
            device_id = get_cuda_device(gpu_list)
            # 메모리 사용이 제일 적은 cuda device 할당
            device = device_id
    else:
        # cuda device 지정을 하지 않은 경우
        device = -1  # CPU 사용

    return device
