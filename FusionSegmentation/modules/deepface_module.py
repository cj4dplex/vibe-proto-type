import time
import os
import glob
from pathlib import Path
import numpy as np
import cv2
from datetime import datetime

from folder_util import _make_imageFolderMap
import run_faceSearch

import featureSearch
from query_util import read_query_tables

from img_util import read_images, get_class_faces
from utility.deepface_utils import build_model, get_feature
from face_util import get_face_information, get_face_angle
from data_summary import add_box_frame_info

from deepface.detectors import FaceDetector

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'  # Tensorflow print Only "Error" Message
import tensorflow as tf


def deepface_build_model(model_name):
    # Build DeepFace Model (based on Model Name)
    model = build_model(model_name)
    model_info = [model, model_name]
    return model_info


def faceDetect_build_model(model_name):
    face_detector = FaceDetector.build_model(model_name)
    return face_detector


def featureExtract(inputPath, model, model_name, gpu_id, face_model=None):
    # RGBA -> Face Detect -> Face Feature (save as npy)
    savePath = inputPath + "/cache"
    os.makedirs(savePath, exist_ok=True)

    images = read_images(inputPath)
    # print("Load images is finished!")

    # Detect Face Image
    # faces_imgs, faces_rects, num_face = get_face_information(images, mode='deepface', face_model=face_model)
    faces_imgs, faces_rects, num_face = get_face_information(images, mode=None)
    cls_faces_imgs, _ = get_class_faces(faces_imgs, faces_rects, num_face)

    # Remove Miss-Detection
    faces_imgs = []
    threshold = len(images) * 0.7
    for cls_face_imgs in cls_faces_imgs:
        if len(cls_face_imgs) >= threshold:
            faces_imgs.append(cls_face_imgs)

    # Tensorflow -> Set GPU Device
    if gpu_id == -1:
        device_is = '/CPU'  # For Tensorflow
    else:
        device_is = '/device:GPU:{}'.format(gpu_id)  # For Tensorflow
    device_id = gpu_id  # For Pytorch

    # Extract Features
    with tf.device(device_is):  # 해당 작업에서만 지정된 GPU 사용
        for idx, faces in enumerate(faces_imgs):
            features = []
            npfaces = []
            for num, face in enumerate(faces):
                if face is not None:
                    npfaces.append(face)
                    # bgr_face = cv2.cvtColor(face.copy(), cv2.COLOR_RGB2BGR)
                    # cv2.imwrite(savePath + "/{:05d}.png".format(num), bgr_face)

                    feature = get_feature(face, model=model, model_name=model_name, detector_backend=None)
                    angle = get_face_angle(face, device_id=device_id)

                    data = [feature, angle]
                    features.append(data)

            if not features:
                print("No Face to extract features in {}".format(savePath))
            else:
                # Save Face Images (npy format)
                np.save(savePath + "/face_{}_images.npy".format(idx), np.array(npfaces))
                # Save Features (npy format)
                print("Save Features in {}".format(savePath))
                np.save(savePath + "/face_{}_features.npy".format(idx), np.array(features))

    if len(os.listdir(savePath)) == 0:
        push_path = False  # No Face Detected --> No Features / Face Images
    else:
        push_path = True
    print("Use This Path? :", push_path)
    return push_path

def run_featureSearch(inputPath, outputPath, model, model_name, gpu_id, flip_mode=False):
    # Make Save Path Folder
    search_resultPath = outputPath + "/searching_result"
    os.makedirs(search_resultPath, exist_ok=True)

    # if "path_result.csv" in os.listdir(search_resultPath):
    csvFiles = glob.glob(os.path.join(search_resultPath, "*_result.csv"))
    cnt_csvFiles = len(csvFiles)
    if cnt_csvFiles > 0:
        # 가장 최신의 result.csv 읽기
        latest = 0
        for idx, f in enumerate(csvFiles):
            csvName, _ = os.path.basename(f).split(".")
            str_time = csvName[:17]
            time_obj = datetime.strptime(str_time, '%y-%m-%d_%H-%M-%S')
            if idx == 0:
                latest = time_obj
            else:
                if latest < time_obj:
                    latest = time_obj
        str_latest = latest.strftime('%y-%m-%d_%H-%M-%S')
        readFile_name = str_latest + "_result.csv"
        path_result = np.loadtxt(search_resultPath + "/" + readFile_name, dtype=str, delimiter=",")
    else:
        category = ["Classified",
                    "min width", "min height",
                    "max width", "max height",
                    "start frame", "end frame",
                    "Image Path"]
        path_result = [category]

    dataPath = inputPath + "/cache"
    print("Process -> {}".format(inputPath))
    if len(os.listdir(dataPath)) > 0:  # path 내 Feature 정보가 있을 경우

        # Initial Query Table
        yaw_table = [-90, -45, 0, 45, 90]
        # pitch_table = [-45, 0, 45]
        pitch_table = None
        deviation = 30
        angle_table = [yaw_table, pitch_table, deviation]

        if pitch_table is None:
            len_pitch_table = 1
        else:
            len_pitch_table = len(pitch_table)

        # Check Verify or First Register
        if len(glob.glob(os.path.join(search_resultPath, "*.npy"))) != 0:
            verify = True  # Verify with Exist Class Query
        else:
            verify = False  # Registering First Class Query

        # Load Class Queries
        if verify:
            # Load Exist Class Query
            print("Load Exist Class Query ...")
            query_tables, query_names = read_query_tables(search_resultPath)
        else:
            # Create First Class Query
            print("Create New Class Query ...")
            query_tables = None
            query_names = []

        query_table = [None for i in range(len(yaw_table) * len_pitch_table)]
        result = featureSearch.run(inputPath, search_resultPath, query_tables, query_names,
                                   query_table, angle_table, model, model_name, gpu_id, flip_mode=flip_mode)

    else:  # Path 내 Feature 정보가 없는 경우 == Face Detection 되지 않음
        print("No Face Detected ...")
        result = [["Unknown", inputPath]]

    if result:
        inputPath = inputPath.replace('/', '\\')
        index = inputPath.split("\\").index('instance_rgba')
        scene_path = "/".join(inputPath.split("\\")[:index])
        # box 정보, frame 정보 추가
        result = add_box_frame_info(result, scene_path)
        path_result = np.vstack((path_result, result))

    # print("# FaceSearch RESULT #")
    # print(path_result)

    saveFile_name = datetime.now().strftime('%y-%m-%d_%H-%M-%S') + '_result.csv'
    if path_result is not None:
        np.savetxt(search_resultPath + "/" + saveFile_name, path_result, fmt="%s", delimiter=",")
        # 이상 없이 저장 후, 기존에 있던 result 파일 삭제
        saved_csvFiles = sorted(glob.glob(os.path.join(search_resultPath, "*_result.csv")))
        if cnt_csvFiles < len(saved_csvFiles):
            for n, f in enumerate(saved_csvFiles):
                if n != len(saved_csvFiles) - 1:
                    os.remove(f)


def run_faceRecog(inputPath, outputPath, model):
    scene_name = os.path.basename(inputPath)
    faceSearch_result_path = outputPath + "/searching_result"
    os.makedirs(faceSearch_result_path, exist_ok=True)

    yaw_table = [-90, -45, 0, 45, 90]
    # yaw_table = [-90, 0, 90]
    # pitch_table = [-45, 0, 45]
    pitch_table = None
    deviation = 15
    angle_table = [yaw_table, pitch_table, deviation]

    # Create FolderMap
    # folderMap = []
    # additional_forbidden_words = ["vos", "mask", "file_list_summery", "enhance", ".txt", ".json", "instance_full_rgba"]
    # input_folderMap = _make_imageFolderMap(inputPath, folderMap, additional_forbidden_words)
    # print("### Make Input FolderMap is Finished...!")
    input_folderMap = [r'D:\TEST3\3\instance_rgba\person_0',
               r'D:\TEST3\3\instance_rgba\person_1',
               r'D:\TEST3\3\instance_rgba\person_2',
               r'D:\TEST3\3\instance_rgba\person_3',
               r'D:\TEST3\3\instance_rgba\person_4',
               r'D:\TEST3\3\instance_rgba\person_5',
               r'D:\TEST3\3\instance_rgba\person_6',
               r'D:\TEST3\3\instance_rgba\person_7',
               r'D:\TEST3\3\instance_rgba\person_8',
               r'D:\TEST3\3\instance_rgba\person_9',
               r'D:\TEST3\3\instance_rgba\person_10',]

    # print("@@@@@@@@@@@@@")
    # print(input_folderMap)

    # Class_*.npy 파일 확인
    if len(glob.glob(os.path.join(faceSearch_result_path, "*.npy"))) != 0:
        first = False
    else:
        first = True

    # path_result.csv 파일 확인
    csvFile_path = Path(faceSearch_result_path + "/path_result.csv")
    if csvFile_path.exists():
        path_result = np.loadtxt(faceSearch_result_path + "/path_result.csv", dtype=str, delimiter=",")
    else:
        category = ["Classified",
                    "min width", "min height",
                    "max width", "max height",
                    "start frame", "end frame",
                    "Image Path"]
        path_result = [category]

    if pitch_table is None:
        len_pitch_table = 1
    else:
        len_pitch_table = len(pitch_table)

    if inputPath is not None:
        if first:
            print("Create New Class Query ...")
            query_tables = None
            query_names = []
        else:
            print("Load Exist Class Query ...")
            query_tables, query_names = read_query_tables(faceSearch_result_path)

        query_table = [None for i in range(len(yaw_table) * len_pitch_table)]
        result_hash_table = run_faceSearch.main(input_folderMap, faceSearch_result_path,
                                                query_tables, query_names, query_table,
                                                angle_table, model)

        # Convert List -> Numpy -> CSV file (Result File)
        # result = np.array(result_hash_table)

        # print("###########################")
        # print("### RESULT ###")
        # print(result)
        os.makedirs(faceSearch_result_path + "/temp", exist_ok=True)
        if result_hash_table:  # [[cls name, path]]
            np.save(faceSearch_result_path + "/temp/path_result_{}".format(scene_name), result_hash_table)

            # box 정보, frame 정보 추가
            result = add_box_frame_info(result_hash_table, inputPath)
            path_result = np.vstack((path_result, result))

        # print("# FaceSearch RESULT #")
        # print(path_result)

        if path_result is not None:
            np.savetxt(faceSearch_result_path + "/path_result.csv", path_result, fmt="%s", delimiter=",")

def main(inputPath, outputPath, model):
    run_faceRecog(inputPath, outputPath, model)