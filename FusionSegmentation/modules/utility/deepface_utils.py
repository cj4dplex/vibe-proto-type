import numpy as np

from deepface.basemodels import VGGFace, OpenFace, Facenet, Facenet512, FbDeepFace, DeepID, DlibWrapper, ArcFace, Boosting, SFaceWrapper
from deepface.extendedmodels import Age, Gender, Race, Emotion
from deepface.commons import functions, distance as dst
from deepface.detectors import FaceDetector


def build_model(model_name):
    global model_obj  #singleton design pattern

    models = {
        'VGG-Face': VGGFace.loadModel,
        'OpenFace': OpenFace.loadModel,
        'Facenet': Facenet.loadModel,
        'Facenet512': Facenet512.loadModel,
        'DeepFace': FbDeepFace.loadModel,
        'DeepID': DeepID.loadModel,
        'Dlib': DlibWrapper.loadModel,
        'ArcFace': ArcFace.loadModel,
        'SFace': SFaceWrapper.load_model,
        'Emotion': Emotion.loadModel,
        'Age': Age.loadModel,
        'Gender': Gender.loadModel,
        'Race': Race.loadModel
    }

    if not "model_obj" in globals():
        model_obj = {}

    if not model_name in model_obj.keys():
        model = models.get(model_name)
        if model:
            model = model()
            model_obj[model_name] = model
            #print(model_name," built")
        else:
            raise ValueError('Invalid model_name passed - {}'.format(model_name))

    return model_obj[model_name]


def deepface_detect(img, face_model, detector_backend='retinaface', align=True):
    img_region = [0, 0, img.shape[0], img.shape[1]]

    # ----------------------------------------------
    # people would like to skip detection and alignment if they already have pre-processed images
    if detector_backend == 'skip':
        return img, img_region

    # ----------------------------------------------

    # detector stored in a global variable in FaceDetector object.
    # this call should be completed very fast because it will return found in memory
    # it will not build face detector model in each call (consider for loops)
    if face_model is None:
        face_detector = FaceDetector.build_model(detector_backend)
    else:
        face_detector = face_model

    try:
        detected_face, img_region = FaceDetector.detect_face(face_detector, detector_backend, img, align)
    except:  # if detected face shape is (0, 0) and alignment cannot be performed, this block will be run
        detected_face = None

    if (isinstance(detected_face, np.ndarray)):
        return detected_face, img_region
    else:
        img_region = None
        return detected_face, img_region



def represent(img_path, model_name='VGG-Face', model=None, enforce_detection=True, detector_backend='opencv', align=True, normalization='base'):
    if model is None:
        model = build_model(model_name)

    #decide input shape
    input_shape_x, input_shape_y = functions.find_input_shape(model)

    #detect and align
    img = functions.preprocess_face(img=img_path
        , target_size=(input_shape_y, input_shape_x)
        , enforce_detection=enforce_detection
        , detector_backend=detector_backend
        , align=align)

    #custom normalization
    img = functions.normalize_input(img=img, normalization=normalization)

    #represent
    embedding = model.predict(img)[0].tolist()

    return embedding


def get_feature(image, model=None, model_name="Facenet512", detector_backend=None):
    """
    [input]
    * Face Image (np.array)
    [output]
    * feature (list)
    """

    if detector_backend is None:
        detector_backend = "skip"
    enforce_detection = True
    align = True
    normalization = "base"

    img_feature = represent(img_path=image, model_name=model_name, model=model, enforce_detection=enforce_detection,
                            detector_backend=detector_backend, align=align, normalization=normalization)
    return img_feature


def feature_verify(feat1, feat2, model_name="Facenet512", metrics="euclidean_l2"):
    if metrics == 'cosine':
        distance = dst.findCosineDistance(feat1, feat2)
    elif metrics == "euclidean":
        distance = dst.findEuclideanDistance(feat1, feat2)
    elif metrics == "euclidean_l2":
        distance = dst.findEuclideanDistance(dst.l2_normalize(feat1), dst.l2_normalize(feat2))
    else:
        raise ValueError("Invalid distance_metric passed - ", metrics)

    threshold = dst.findThreshold(model_name, metrics)
    if distance <= threshold:
        identified = True
    else:
        identified = False

    predict = {
        "verified": identified,
        "distance": distance,
        "threshold": threshold
    }
    return predict