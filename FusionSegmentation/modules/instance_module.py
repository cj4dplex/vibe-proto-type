import torch
import json
import numpy as np
import glob, os
import time
import cv2
from PIL import Image

from interact.instance import run_image_instance, run_video_instance
from interact.instance import run_tracking
from interact.interactive_utils import load_image, load_images

# from util.Logger import dprint
image_num_thresh = 1000

class InstanceModule:
    def __init__(self, imgPath, outputPath, selected_classes, dynamic, gpu_id):
        super().__init__()

        if isinstance(imgPath, np.ndarray):  # imgPath == np.arrays
            images = imgPath.copy()
            images_name = []
            for i in range(len(images)):
                images_name.append(i)

        else:  # imgPath == str path
            print("Processing : ", imgPath)
            files = sorted(glob.glob(os.path.join(imgPath, '*.jpg')))
            if len(files) == 0:
                files = sorted(glob.glob(os.path.join(imgPath, '*.png')))
                if len(files) == 0:
                    files = sorted(glob.glob(os.path.join(imgPath, '*.tif')))

            self.files = files

            try:
                if files < image_num_thresh:
                    images, images_name = load_images(files, None)
                    print("# Image Loading is Finished ...!")
                    self.files = []
                    self.height, self.width, _ = images[0].shape
                    self.images = images
                    self.images_name = images_name
                    tooManyImagesFlag = False
                else:
                    tooManyImagesFlag = True
            except:
                tooManyImagesFlag = True

        if tooManyImagesFlag == True:
            print("# Cannot load images, load image one by one in instance segmentation module")
            self.height, self.width = -1, -1
            self.images = []
            self.images_name = []

        self.outputPath = outputPath
        self.selected_class = selected_classes
        self.dynamic = dynamic

        self.current_labels = None
        self.current_scores = None
        self.current_boxes = None
        self.class_frames = None

        self.current_mask = None
        self.masks_array = None

        self.debug_mode = None

        try:
            # instance_s = time.time()
            # print("# Instance Module Start ...!")
            self.run_instanceSeg(gpu_id)
            # instance_time = time.time() - instance_s
            # print("# Instance is Finished ...!")
            # print("# Instance Time : {}".format(instance_time))
        except Exception as e:
            # print("Instance Segmentation -> ValueError")
            # dprint("[Error] Instance Module : {}".format(e))
            print("[Error] Instance Module : {}".format(e))
            self.current_mask = []
            instance_time = 0

        # print("INSTANCE TIME : {} / Second Per Image : {}".format(instance_time, instance_time / len(images)))
        print("# Instance Module is Finished ...!")

        try:
            if self.current_mask:
                # Tracking 진행
                # tracking_s = time.time()
                self.run_trackingObj()
                # tracking_time = time.time() - tracking_s
                print("# Tracking is Finished ...!")
                # print("# Tracking Time : {}".format(tracking_time))
                if self.labelInfo:
                    # save_s = time.time()
                    self.save_tracking_info()
                    # save_time = time.time() - save_s
                    print("# Result save is Finished ...!")
                    # print("Result Save Time : {}".format(save_time))
                elif not self.labelInfo:
                    print("Tracking Result is wrong")
                pass
            else:
                f = open(self.outputPath + "/NO_OBJECT.txt", 'w')
                f.close()
                pass
        except Exception as e:
            # dprint("[Error] Tracking Module : {}".format(e))
            print("[Error] Tracking Module : {}".format(e))

    def run_instanceSeg(self, gpu_id):
        if len(self.files) != 0:
            onebyone = True
            masks_frames, frame_masks, class_frames, class_labels, class_scores, class_boxes, classes, ds_masks_frames, full_masks = \
                run_video_instance(self.files, self.outputPath, seleteced_classes=self.selected_class, gpu_id=gpu_id, onebyone=True)
        else:
            onebyone = False
            masks_frames, frame_masks, class_frames, class_labels, class_scores, class_boxes, classes, ds_masks_frames, full_masks = \
                run_video_instance(self.images, "", seleteced_classes=self.selected_class, gpu_id=gpu_id, onebyone=False)

        instanced_classes = []
        for labels in class_labels:
            for label in labels:
                if label not in instanced_classes:
                    instanced_classes.append(label)
        classes_info = {}
        true_class = []
        false_class = []
        for cls in classes:
            if cls not in instanced_classes:
                false_class.append(cls)
            else:
                true_class.append(cls)
        classes_info["True"] = true_class
        classes_info["False"] = false_class

        if true_class == [] and false_class == []:
            print("[Instance Result] : Object is Nothing in here ..!")
        else:
            with open(self.outputPath + '/classes.json', 'w') as f:
                json.dump(classes_info, f)

        self.current_labels = class_labels
        self.current_scores = class_scores
        self.current_boxes = class_boxes
        self.class_frames = class_frames

        self.current_mask = frame_masks
        self.masks_array = masks_frames

        torch.cuda.empty_cache()

        if onebyone == False:
            if self.current_mask == [] and self.dynamic is False:
                print("Warning : NO Objects in this Frames ...")
            else:
                fMask_save_path = self.outputPath + "/full_mask/"
                os.makedirs(fMask_save_path, exist_ok=True)
                frame_names = self.images_name.copy()
                for idx, full_mask in enumerate(full_masks):
                    if full_mask is None:
                        fMask = np.zeros((self.height, self.width), dtype="uint8")
                        fMask = Image.fromarray(fMask)
                    else:
                        fMask = full_mask
                    frame_name = int(frame_names[idx])
                    fMask.save(fMask_save_path + '/{:05d}.png'.format(frame_name))

                dsMask_save_path = self.outputPath + "/dynamic_mask/"
                os.makedirs(dsMask_save_path, exist_ok=True)
                for idx, ds_masks_frame in enumerate(ds_masks_frames):
                    if ds_masks_frame is None:
                        mask = np.zeros((self.height, self.width), dtype="uint8")
                        mask = Image.fromarray(mask)
                    else:
                        mask = ds_masks_frame
                    frame_name = int(frame_names[idx])
                    mask.save(dsMask_save_path + '/{:05d}.png'.format(frame_name))


    def run_trackingObj(self):
        shot_info, seed_info, tracked_box_info = run_tracking(self.images, self.class_frames, self.current_labels,
                                                              self.current_scores, self.current_boxes, self.debug_mode)
        # print("shot info  : ", shot_info)
        # print("seed info  : ", seed_info)
        # print("traced box : ", tracked_box_info)

        self.trackingInfo = [shot_info, seed_info, tracked_box_info]

        print("Labels in Tracking Results")
        labels = list(shot_info.keys())
        print("This labels : ", labels)

        false_cnt = 0
        if false_cnt == 0:
            self.labelInfo = labels
        else:
            self.labelInfo = []
        torch.cuda.empty_cache()

    def save_tracking_info(self):
        print("# Save Tracking Information ...!")
        labels = self.labelInfo
        shot_info, seed_info, tracked_box_info = self.trackingInfo

        masks_arrays = self.masks_array.copy()
        class_labels = self.current_labels.copy()
        class_boxes = self.current_boxes.copy()

        # print("Labels :", labels)
        # print("seed infoes :", seed_info)
        # print("shot cut list : ", shot_info)
        # print("class boxes : ", class_boxes)
        # print("traced boxes : ", tracked_box_info)

        # print("seed info Key : ", list(seed_info.keys()))
        # print("shot cut list Key : ", list(shot_info.keys()))

        tracking_info = {}
        box_info = {}
        obj_names = []
        remove_labels = []
        for num, label in enumerate(labels):
            if not seed_info[label]:
                remove_labels.append(label)
        for remove_label in remove_labels:
            labels.remove(remove_label)

        for num, label in enumerate(labels):
            obj_frame = shot_info[label]
            obj_frame_list = self.get_frameRange(obj_frame)
            seed_frame = seed_info[label][0][0]
            tracked_box = tracked_box_info[seed_frame][label][0]
            class_box = class_boxes[seed_frame]
            class_idx = class_box.index(tracked_box)
            class_name = class_labels[seed_frame][class_idx]
            obj_name = str(class_name) + "_" + str(num)
            tracking_info[obj_name] = obj_frame_list

            obj_boxInfo = self.get_boxSize(label, tracked_box_info)
            box_info[obj_name] = obj_boxInfo
            obj_names.append(obj_name)

        self.obj_names = obj_names
        print("obj names : ", obj_names)
        with open(self.outputPath + '/object_frames.json', 'w') as f:
            json.dump(tracking_info, f)
        with open(self.outputPath + '/object_box.json', 'w') as f:
            json.dump(box_info, f)
        with open(self.outputPath + '/object_classes.json', 'w') as f:
            json.dump(obj_names, f)

        frame_names = self.images_name
        frame_images = self.images
        for cnt, label in enumerate(labels):
            obj_name = obj_names[cnt]
            instance_save_path = self.outputPath + "/instance_mask/" + obj_name
            rgba_save_path = self.outputPath + "/instance_rgba/" + obj_name

            os.makedirs(instance_save_path, exist_ok=True)
            os.makedirs(rgba_save_path, exist_ok=True)

            shot_range = shot_info[label]
            label_boxes = []
            for frame_box in tracked_box_info:
                keys = list(frame_box.keys())
                if label in keys:
                    label_boxes.append(frame_box[label])
            # print("shot range : {} / label boxes : {}".format(len(shot_range), len(label_boxes)))
            instance_boxes = []
            for frame_idx, label_box, in zip(shot_range, label_boxes):
                for masks_array, cls_box in zip(masks_arrays, class_boxes):
                    boxes = cls_box.copy()
                    for idx, box in enumerate(boxes):
                        if label_box[0] == box:
                            # print("Index : ", idx)
                            mask = masks_array[idx].copy()
                            frame_name = int(frame_names[frame_idx])
                            # frame_name = frame_idx
                            mask.save(instance_save_path + '/{:05d}.png'.format(frame_name))

                            # Crop Image, Crop Mask -> RGBA
                            mask_array = np.array(mask) * 1
                            binary_mask = mask_array * 255

                            frame = frame_images[frame_idx]
                            # frame = frame_images[frame_name]

                            # Get Cropped RGBA
                            # crop_mask, crop_img = self.crop_image(binary_mask, frame, box)
                            # rgba_img = self.convertRGBtoRGBA(crop_img, crop_mask)

                            # Get None Cropped RGBA
                            rgba_img = self.convertRGBtoRGBA(frame, binary_mask)
                            cv2.imwrite(rgba_save_path + '/{:05d}.png'.format(frame_name), rgba_img)

                            instance_boxes.append(box)
            np_boxes = np.array(instance_boxes)
            np.save(instance_save_path + '/box_info', np_boxes)
            seed_infoes = np.array(seed_info[label])
            np.save(instance_save_path + '/seed_info', seed_infoes)

    def get_frameRange(self, obj_frames):
        start = obj_frames[0] + self.images_name[0]
        end = obj_frames[len(obj_frames) - 1] + self.images_name[0]
        frame_cnt = len(obj_frames)

        frames_range = []
        if frame_cnt != end - start + 1:
            tmp_start = start
            for i, obj_frame in enumerate(obj_frames):
                frame_range = None
                if i < len(obj_frames) - 1:
                    if obj_frame + 1 != obj_frames[i + 1]:
                        tmp_end = obj_frames[i]
                        frame_range = [tmp_start, tmp_end]
                        tmp_start = obj_frames[i + 1]
                else:
                    frame_range = [tmp_start, obj_frame]

                if frame_range is not None:
                    frames_range.append(frame_range)

        elif frame_cnt == end - start + 1:
            frames_range.append([start, end])

        return frames_range

    def get_boxSize(self, label, tracked_box_info):
        label_boxes = []
        for tracked_box in tracked_box_info:
            keys = tracked_box.keys()
            if label in keys:
                value = tracked_box[label]
                label_boxes.append(value)

        min_size = 0
        max_size = 0
        min_wh = None
        max_wh = None
        for cnt, label_box in enumerate(label_boxes):
            x0, y0, x1, y1, _ = label_box[0]
            size = (y1 - y0) * (x1 - x0)
            if cnt == 0:
                min_size = size
                min_height = y1 - y0
                min_width = x1 - x0
                min_wh = [min_width, min_height]

                max_size = size
                max_height = y1 - y0
                max_width = x1 - x0
                max_wh = [max_width, max_height]
            else:
                if size > max_size:
                    max_size = size
                    max_height = y1 - y0
                    max_width = x1 - x0
                    max_wh = [max_width, max_height]
                    # max_box = [x0, y0, x1, y1, max_size]
                if size < min_size:
                    min_size = size
                    min_height = y1 - y0
                    min_width = x1 - x0
                    min_wh = [min_width, min_height]
                    # min_box = [x0, y0, x1, y1, min_size]

        obj_boxInfo = [min_wh, max_wh]
        return obj_boxInfo

    def crop_image(self, mask, image, box):
        x0, y0, x1, y1, _ = box
        crop_mask = mask[y0:y1, x0:x1]
        crop_img = image[y0:y1, x0:x1]
        return crop_mask, crop_img

    def convertRGBtoRGBA(self, image, mask):
        alpha = np.array(mask)
        rgba = cv2.cvtColor(image, cv2.COLOR_RGB2BGRA)
        rgba[:, :, 3] = alpha
        rgba_arr = np.array(rgba)
        return rgba_arr


def main(imgPath, outputPath, selected_classes, dynamic, gpu_id):
    # instance_start = time.time()
    # print("# Instance Module Start")
    InstanceModule(imgPath, outputPath, selected_classes, dynamic, gpu_id)
    # instance_end = time.time() - instance_start
    # print("# Instance Module Processing Time : {}".format(instance_end))
