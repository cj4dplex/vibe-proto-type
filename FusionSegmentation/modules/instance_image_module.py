import torch
import json
import numpy as np
import glob, os
import time
import cv2
from PIL import Image

from interact.instance import run_image_instance, run_video_instance, load_instance_model
from interact.instance import run_tracking
from interact.interactive_utils import load_image, load_images
from FusionSegmentation.object_image_tracking import get_image_tracking, run_on_image_tracking

# from util.Logger import dprint

class InstanceModule:
    def __init__(self, imgPath, outputPath, selected_classes, dynamic, gpu_id, min_frames=24):
        super().__init__()

        files = None
        if isinstance(imgPath, np.ndarray):  # imgPath == np.arrays
            images = imgPath.copy()
            images_name = []
            for i in range(len(images)):
                images_name.append(i)

        else:  # imgPath == str path
            print("Processing : ", imgPath)
            files = sorted(glob.glob(os.path.join(imgPath, '*.jpg')))
            if len(files) == 0:
                files = sorted(glob.glob(os.path.join(imgPath, '*.png')))
                if len(files) == 0:
                    files = sorted(glob.glob(os.path.join(imgPath, '*.tif')))

        self.imageFiles = files
        self.first_frame_name = None

        self.outputPath = outputPath
        self.selected_class = selected_classes
        self.dynamic = dynamic

        self.current_labels = None
        self.current_scores = None
        self.current_boxes = None
        self.class_frames = None

        self.current_mask = None
        self.masks_array = None

        self.debug_mode = None

        instance_model = load_instance_model(gpu_id)

        ### Instance & Tracking -> Image by Image
        if files is not None:
            frame_idxs = []
            one_frame_masks = []
            masks_frames = []
            frames_labels = []
            frames_scores = []
            frames_boxes = []
            frames_classes = []
            instanced_classes = []

            obj_id_in_frames = []
            tracked_boxes = []

            tracker = {}
            for idx, imageFile in enumerate(files):
                print("Instance Processing --> ({}/{})".format(idx, len(files) - 1))
                image, image_name = load_image(imageFile)
                if idx == 0:
                    self.first_frame_name = int(image_name)

                self.height, self.width, _ = image.shape

                frame_masks, voc_mask, frame_labels, frame_scores, frame_boxes, class_in_frame = self.run_image_instanceSeg(image, image_name, gpu_id, instance_model)
                torch.cuda.empty_cache()

                for label in frame_labels:
                    if label not in instanced_classes:
                        instanced_classes.append(label)

                if frame_masks != []:
                    frame_idxs.append(idx)
                    one_frame_masks.append(frame_masks)
                    masks_frames.append(voc_mask)

                    frames_labels.append(frame_labels)
                    frames_scores.append(frame_scores)
                    frames_boxes.append(frame_boxes)
                # TEST FOR JW
                else:
                    frame_idxs.append(None)
                    one_frame_masks.append(None)
                    masks_frames.append(None)

                    frames_labels.append(None)
                    frames_scores.append(None)
                    frames_boxes.append(None)

                for class_name in class_in_frame:
                    if class_name not in frames_classes:
                        frames_classes.append(class_name)

                if idx == 0:
                    tracker = []

                tracker, obj_id_in_frame, new_tracked_box = get_image_tracking(tracker, frame_labels, frame_scores, frame_boxes)
                obj_id_in_frames.append(obj_id_in_frame)
                tracked_boxes.append(new_tracked_box)

            classes_info = {}
            true_class = []
            false_class = []
            for cls in frames_classes:
                if cls not in self.selected_class:
                    false_class.append(cls)
                else:
                    true_class.append(cls)
            classes_info["True"] = true_class
            classes_info["False"] = false_class

            if true_class == [] and false_class == []:
                print("[Instance Result] : Object is Nothing in here ..!")
            else:
                with open(self.outputPath + '/classes.json', 'w') as f:
                    json.dump(classes_info, f)

            shot_info, seed_info, tracked_box_info = run_on_image_tracking(obj_id_in_frames, tracked_boxes, frame_idxs, self.height, self.width, min_frames)
            self.trackingInfo = [shot_info, seed_info, tracked_box_info]

            self.current_labels = frames_labels
            self.current_scores = frames_scores
            self.current_boxes = frames_boxes
            self.class_frames = frame_idxs

            self.current_mask = masks_frames
            self.masks_array = one_frame_masks
            self.labelInfo = list(shot_info.keys())

        self.save_tracking_info()

        print("!!!!")
                # Tracking 모듈 -> Inputs
                # self.images, self.class_frames, self.current_labels, self.current_scores, self.current_boxes, self.debug_mode

    def run_image_instanceSeg(self, image, imageName, gpu_id, model):
        new_arrays, voc_masks, class_labels, class_scores, class_boxes, class_in_frame, ds_masks, full_mask = run_image_instance(image, model, seleteced_classes=self.selected_class, gpu_id=gpu_id)

        # 외부로 나가야 할 변수들 시작
        # self.current_labels = class_labels
        # self.current_scores = class_scores
        # self.current_boxes = class_boxes
        # self.class_frames = class_frames
        #
        # self.current_mask = frame_masks
        # self.masks_array = masks_frames
        # 외부로 나가야 할 변수들 끝

        height, width, _ = image.shape
        frame_name = int(imageName)

        # Create Dynamic & Full Mask
        if self.dynamic is True:
            fMask_save_path = self.outputPath + "/full_mask/"
            os.makedirs(fMask_save_path, exist_ok=True)

            dsMask_save_path = self.outputPath + "/dynamic_mask/"
            os.makedirs(dsMask_save_path, exist_ok=True)

            empty_mask = np.zeros((height, width), dtype="uint8")

            if full_mask is None:
                fMask = Image.fromarray(empty_mask)
            else:
                fMask = full_mask
            fMask.save(fMask_save_path + '/{:05d}.png'.format(frame_name))

            if ds_masks is None:
                mask = Image.fromarray(empty_mask)
            else:
                mask = ds_masks
            mask.save(dsMask_save_path + '/{:05d}.png'.format(frame_name))

        return new_arrays, voc_masks, class_labels, class_scores, class_boxes, class_in_frame

    def run_trackingObj(self):
        shot_info, seed_info, tracked_box_info = run_tracking(self.images, self.class_frames, self.current_labels,
                                                              self.current_scores, self.current_boxes, self.debug_mode)
        # print("shot info  : ", shot_info)
        # print("seed info  : ", seed_info)
        # print("traced box : ", tracked_box_info)

        self.trackingInfo = [shot_info, seed_info, tracked_box_info]

        print("Labels in Tracking Results")
        labels = list(shot_info.keys())
        print("This labels : ", labels)

        false_cnt = 0
        if false_cnt == 0:
            self.labelInfo = labels
        else:
            self.labelInfo = []
        torch.cuda.empty_cache()

    def save_tracking_info(self):
        print("# Save Tracking Information ...!")
        labels = self.labelInfo
        shot_info, seed_info, tracked_box_info = self.trackingInfo

        masks_arrays = self.masks_array.copy()
        class_labels = self.current_labels.copy()
        class_boxes = self.current_boxes.copy()

        # print("Labels :", labels)
        # print("seed infoes :", seed_info)
        # print("shot cut list : ", shot_info)
        # print("class boxes : ", class_boxes)
        # print("traced boxes : ", tracked_box_info)

        # print("seed info Key : ", list(seed_info.keys()))
        # print("shot cut list Key : ", list(shot_info.keys()))

        tracking_info = {}
        box_info = {}
        obj_names = []
        remove_labels = []
        for num, label in enumerate(labels):
            if not seed_info[label]:
                remove_labels.append(label)
        for remove_label in remove_labels:
            labels.remove(remove_label)

        for num, label in enumerate(labels):
            obj_frame = shot_info[label]
            obj_frame_list = self.get_frameRange(obj_frame)
            seed_frame = seed_info[label][0][0]
            tracked_box = tracked_box_info[seed_frame][label][0]
            class_box = class_boxes[seed_frame]
            class_idx = class_box.index(tracked_box)
            class_name = class_labels[seed_frame][class_idx]
            obj_name = str(class_name) + "_" + str(num)
            tracking_info[obj_name] = obj_frame_list

            obj_boxInfo = self.get_boxSize(label, tracked_box_info)
            box_info[obj_name] = obj_boxInfo
            obj_names.append(obj_name)

        self.obj_names = obj_names
        print("obj names : ", obj_names)
        with open(self.outputPath + '/object_frames.json', 'w') as f:
            json.dump(tracking_info, f)
        with open(self.outputPath + '/object_box.json', 'w') as f:
            json.dump(box_info, f)
        with open(self.outputPath + '/object_classes.json', 'w') as f:
            json.dump(obj_names, f)

        for cnt, label in enumerate(labels):
            obj_name = obj_names[cnt]
            instance_save_path = self.outputPath + "/instance_mask/" + obj_name
            rgba_save_path = self.outputPath + "/instance_rgba/" + obj_name

            os.makedirs(instance_save_path, exist_ok=True)
            os.makedirs(rgba_save_path, exist_ok=True)

            shot_range = shot_info[label]
            label_boxes = []
            for frame_box in tracked_box_info:
                keys = list(frame_box.keys())
                if label in keys:
                    label_boxes.append(frame_box[label])
            # print("shot range : {} / label boxes : {}".format(len(shot_range), len(label_boxes)))
            instance_boxes = []
            for frame_idx, label_box, in zip(shot_range, label_boxes):
                for masks_array, cls_box in zip(masks_arrays, class_boxes):
                    # TEST for JW
                    if cls_box is not None:
                        boxes = cls_box.copy()
                        for idx, box in enumerate(boxes):
                            if label_box[0] == box:
                                # print("Index : ", idx)
                                mask = masks_array[idx].copy()
                                frame, frame_name = load_image(self.imageFiles[frame_idx], mode="RGB")
                                # frame_name = frame_idx
                                mask.save(instance_save_path + '/{:05d}.png'.format(int(frame_name)))

                                # Crop Image, Crop Mask -> RGBA
                                mask_array = np.array(mask) * 1
                                binary_mask = mask_array * 255

                                # Get None Cropped RGBA
                                rgba_img = self.convertRGBtoRGBA(frame, binary_mask)
                                cv2.imwrite(rgba_save_path + '/{:05d}.png'.format(int(frame_name)), rgba_img)

                                instance_boxes.append(box)
            np_boxes = np.array(instance_boxes)
            np.save(instance_save_path + '/box_info', np_boxes)
            seed_infoes = np.array(seed_info[label])
            np.save(instance_save_path + '/seed_info', seed_infoes)

    def get_frameRange(self, obj_frames):
        start = obj_frames[0] + self.first_frame_name
        end = obj_frames[len(obj_frames) - 1] + self.first_frame_name
        frame_cnt = len(obj_frames)

        frames_range = []
        if frame_cnt != end - start + 1:
            tmp_start = start
            for i, obj_frame in enumerate(obj_frames):
                frame_range = None
                if i < len(obj_frames) - 1:
                    if obj_frame + 1 != obj_frames[i + 1]:
                        tmp_end = obj_frames[i]
                        frame_range = [tmp_start, tmp_end]
                        tmp_start = obj_frames[i + 1]
                else:
                    frame_range = [tmp_start, obj_frame]

                if frame_range is not None:
                    frames_range.append(frame_range)

        elif frame_cnt == end - start + 1:
            frames_range.append([start, end])

        return frames_range

    def get_boxSize(self, label, tracked_box_info):
        label_boxes = []
        for tracked_box in tracked_box_info:
            keys = tracked_box.keys()
            if label in keys:
                value = tracked_box[label]
                label_boxes.append(value)

        min_size = 0
        max_size = 0
        min_wh = None
        max_wh = None
        for cnt, label_box in enumerate(label_boxes):
            x0, y0, x1, y1, _ = label_box[0]
            size = (y1 - y0) * (x1 - x0)
            if cnt == 0:
                min_size = size
                min_height = y1 - y0
                min_width = x1 - x0
                min_wh = [min_width, min_height]

                max_size = size
                max_height = y1 - y0
                max_width = x1 - x0
                max_wh = [max_width, max_height]
            else:
                if size > max_size:
                    max_size = size
                    max_height = y1 - y0
                    max_width = x1 - x0
                    max_wh = [max_width, max_height]
                    # max_box = [x0, y0, x1, y1, max_size]
                if size < min_size:
                    min_size = size
                    min_height = y1 - y0
                    min_width = x1 - x0
                    min_wh = [min_width, min_height]
                    # min_box = [x0, y0, x1, y1, min_size]

        obj_boxInfo = [min_wh, max_wh]
        return obj_boxInfo

    def crop_image(self, mask, image, box):
        x0, y0, x1, y1, _ = box
        crop_mask = mask[y0:y1, x0:x1]
        crop_img = image[y0:y1, x0:x1]
        return crop_mask, crop_img

    def convertRGBtoRGBA(self, image, mask):
        alpha = np.array(mask)
        rgba = cv2.cvtColor(image, cv2.COLOR_RGB2BGRA)
        rgba[:, :, 3] = alpha
        rgba_arr = np.array(rgba)
        return rgba_arr


def main(imgPath, outputPath, selected_classes, dynamic, gpu_id, viewport=False):
    # instance_start = time.time()
    # print("# Instance Module Start")
    min_frames = 24
    if viewport:
        min_frames = 1

    InstanceModule(imgPath, outputPath, selected_classes, dynamic, gpu_id, min_frames=min_frames)
    # instance_end = time.time() - instance_start
    # print("# Instance Module Processing Time : {}".format(instance_end))