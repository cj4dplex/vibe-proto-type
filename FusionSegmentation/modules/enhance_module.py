import cv2
import torch.cuda.amp
from PIL import Image
import numpy as np
import time
import glob, os
import matplotlib.pyplot as plt
import segmentation_refinement as refine

model_path = "FusionSegmentation/modules/saves"

def load_images(path):
    imgs = []
    file_names = []
    image_files = glob.glob(os.path.join(path, '*.png'))
    if not image_files:
        image_files = glob.glob(os.path.join(path, '*.jpeg'))
    if not image_files:
        image_files = glob.glob(os.path.join(path, '*.tif'))

    print("# Loading Images ...")
    for file in image_files:
        imgName = (os.path.basename(file))
        name, ext = os.path.splitext(imgName)
        file_names.append(int(name))

        img = cv2.imread(file, 1)
        imgs.append(img)
    print("# Loaded Images : {}".format(len(imgs)))
    return imgs, file_names


def load_masks(path, mode):
    masks = []
    file_names = []
    image_files = glob.glob(os.path.join(path, '*.png'))
    if not image_files:
        image_files = glob.glob(os.path.join(path, '*.jpeg'))
    if not image_files:
        image_files = glob.glob(os.path.join(path, '*.tif'))

    print("# Loading Masks ...")
    for file in image_files:
        imgName = (os.path.basename(file))
        name, ext = os.path.splitext(imgName)
        file_names.append(int(name))

        if mode == 0:  # VOS mask (Binary load)
            img = cv2.imread(file, 0)
        elif mode == 1:  # instance mask (All One Hot load -> Need to make binary)
            image = Image.open(file).convert("L")
            img_array = np.array(image)
            img_one = img_array * 1
            img = img_one * 255
        masks.append(img)

        # cv2.imshow("mask", img)
        # cv2.waitKey(0)
    print("# Loaded Masks")
    return masks, file_names


def create_RGBA(rgb_img, mask):
    bgra = cv2.cvtColor(rgb_img, cv2.COLOR_BGR2BGRA)  # BGR -> BGRA Convert
    b, g, r, a = cv2.split(bgra)  # split Channels
    a = mask
    bgra = cv2.merge((b, g, r, a))
    return bgra


def visualization_result(image, old_mask, new_mask, old_rgba, new_rgba):
    fig = plt.figure()
    img1 = fig.add_subplot(3, 2, 1)
    img1.imshow(image)
    img1.set_title("Original")
    img1.axis("off")

    img1 = fig.add_subplot(3, 2, 3)
    img1.imshow(old_mask)
    img1.set_title("Before Mask")
    img1.axis("off")

    img2 = fig.add_subplot(3, 2, 4)
    img2.imshow(new_mask)
    img2.set_title("After Mask")
    img2.axis("off")

    img1 = fig.add_subplot(3, 2, 5)
    img1.imshow(old_rgba)
    img1.set_title("Before RGBA")
    img1.axis("off")

    img2 = fig.add_subplot(3, 2, 6)
    img2.imshow(new_rgba)
    img2.set_title("After RGBA")
    img2.axis("off")

    plt.show()


def PSP_run(imgPath, maskPath, outputPath, gpu_id):
    if gpu_id == -1:
        cuda_device = 'cpu'
    else:
        cuda_device = 'cuda:{}'.format(gpu_id)

    mode = 0
    mfolerName = "/enhanced_vos_mask/"
    rfolerName = "/enhanced_vos_rgba/"
    if os.path.basename(maskPath) == "instance_mask":
        mode = 1
        mfolerName = "/enhanced_instance_mask/"
        rfolerName = "/enhanced_instance_rgba/"

    with torch.cuda.amp.autocast(enabled=True):
        # model_path = "saves"
        print("# Loading Model ...")
        refiner = refine.Refiner(device=cuda_device, model_folder=model_path)  # device can also be 'cpu'
        print("# Loaded Model")

        images, img_names = load_images(imgPath)

        maskFolders = os.listdir(maskPath)
        for folder in maskFolders:
            mask_path = maskPath + "/" + folder
            print("Load Mask Path : ", mask_path)
            cls_name = os.path.basename(mask_path)
            masks, mask_names = load_masks(mask_path, mode)

            start_time = time.time()
            cnt = 0
            for mask, name in zip(masks, mask_names):
                img_idx = img_names.index(name)
                image = images[img_idx]

                # Fast - Global step only.
                # Smaller L -> Less memory usage; faster in fast mode.
                output = refiner.refine(image, mask, fast=False, L=900)
                # CUDA memory Release
                torch.cuda.empty_cache()

                # Create RGBA image
                print("Save Blended RGBA Image ({} / {})".format(cnt, len(masks)))
                mask_save_path = outputPath + mfolerName + cls_name
                rgba_save_path = outputPath + rfolerName + cls_name
                os.makedirs(mask_save_path, exist_ok=True)
                os.makedirs(rgba_save_path, exist_ok=True)

                # old_mtd = create_RGBA(image, mask)
                new_mtd = create_RGBA(image, output)
                cv2.imwrite(mask_save_path + "/{:05d}.png".format(int(name)), output)
                cv2.imwrite(rgba_save_path + "/{:05d}.png".format(int(name)), new_mtd)

                ## BGR to RGB (for Visualization)
                # rgb_img = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
                ## Show Results
                # visualization_result(rgb_img, mask, output, old_mtd, new_mtd)

                cnt += 1
            end_time = time.time() - start_time
            print("Processing Time : {} / Second per Image : {}".format(end_time, end_time / len(masks)))

            print("PSP Finished ...")


def main(imgPath, savePath, mode, gpu_id):
    if mode == "instance":
        maskFolderName = "instance_mask"
    else:
        maskFolderName = "mask"

    maskPath = savePath + "/" + maskFolderName
    PSP_run(imgPath, maskPath, savePath, gpu_id)