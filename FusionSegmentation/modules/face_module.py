import time
import os, glob
import numpy as np

from folder_util import _make_imageFolderMap
import run_faceSearch
from query_util import read_query_tables


def run_faceRecog(inputPath, outputPath):
    scene_name = os.path.basename(inputPath)
    faceSearch_result_path = outputPath + "/searching_result"
    os.makedirs(faceSearch_result_path, exist_ok=True)

    yaw_table = [-90, -45, 0, 45, 90]
    pitch_table = [-45, 0, 45]
    deviation = 15
    angle_table = [yaw_table, pitch_table, deviation]

    # Create FolderMap
    folderMap = []
    additional_forbidden_words = ["vos", "mask", "file_list_summery", "enhance", ".txt", ".json"]
    input_folderMap = _make_imageFolderMap(inputPath, folderMap, additional_forbidden_words)
    print("### Make Input FolderMap is Finished...!")

    # print("@@@@@@@@@@@@@")
    # print(input_folderMap)

    if len(glob.glob(os.path.join(faceSearch_result_path, "*.npy"))) != 0:
        first = False
    else:
        first = True

    path_result = None
    if inputPath is not None:
        if first:
            path_result = None
            query_tables = None
            query_names = []
            query_table = [None for i in range(len(yaw_table) * len(pitch_table))]
            result_hash_table = run_faceSearch.main(input_folderMap, faceSearch_result_path,
                                                    query_tables, query_names, query_table,
                                                    angle_table)
        else:
            query_tables, query_names = read_query_tables(faceSearch_result_path)
            query_table = [None for i in range(len(yaw_table) * len(pitch_table))]
            result_hash_table = run_faceSearch.main(input_folderMap, faceSearch_result_path,
                                                    query_tables, query_names, query_table,
                                                    angle_table)

        # Convert List -> Numpy -> CSV file (Result File)
        # result = np.array(result_hash_table)
        result = result_hash_table
        print("###########################")
        print("### RESULT ###")
        print(result)
        os.makedirs(faceSearch_result_path + "/temp", exist_ok=True)
        if result:
            np.save(faceSearch_result_path + "/temp/path_result_{}".format(scene_name), result)

        if path_result is None and result == []:
            path_result = None
        elif path_result is None and result != []:
            path_result = result
        elif result:
            path_result = np.vstack((path_result, result))

        print("# FaceSearch RESULT #")
        print(path_result)

        if path_result is not None:
            np.savetxt(faceSearch_result_path + "/path_result.csv", path_result, fmt="%s", delimiter=",")

def main(inputPath, outputPath):
    run_faceRecog(inputPath, outputPath)