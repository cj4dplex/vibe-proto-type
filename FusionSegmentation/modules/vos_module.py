import time
import torch
import numpy as np
import os, glob
import cv2
from PIL import Image

from inference_core import InferenceCore
from interact.s2m_controller import S2MController
from interact.fbrs_controller import FBRSController
from model.propagation.prop_net import PropagationNetwork
from model.fusion_net import FusionNet
from model.s2m.s2m_network import deeplabv3plus_resnet50 as S2M

from interact.interactive_utils import load_images, images_to_torch, all_to_onehot
from model.aggregate import aggregate_sbg
from vos_util.tensor_util import pad_divide_by
from interact.labeling import crop_roi_in_labeling_image, save_all, save_single, save_all_singel


class vocModule:
    def __init__(self, modelInfo, imagePath, instancePath):
        super().__init__()
        prop_net, fuse_net, s2m, fbrs = modelInfo
        with torch.cuda.amp.autocast(enabled=True):
            # Load our checkpoint
            prop_saved = torch.load(prop_net)
            prop_model = PropagationNetwork().cuda().eval()
            prop_model.load_state_dict(prop_saved)

            fusion_saved = torch.load(fuse_net)
            fusion_model = FusionNet().cuda().eval()
            fusion_model.load_state_dict(fusion_saved)

            self.prop_model = prop_model
            self.fusion_model = fusion_model
            self.num_objects = 10
            self.mem_freq = 5

            self.outputPath = instancePath
            self.save_mode = 0

            print("PATH ", imagePath)
            files = sorted(glob.glob(os.path.join(imagePath, '*.jpg')))
            if len(files) == 0:
                files = sorted(glob.glob(os.path.join(imagePath, '*.png')))
                if len(files) == 0:
                    files = sorted(glob.glob(os.path.join(imagePath, '*.tif')))

            img_load_s = time.time()
            images, images_name = load_images(files, None)
            img_load = time.time() - img_load_s
            print("Image Loading Finished ... (images : {})".format(len(images)))
            print("image load time : {} / Second per Image : {}".format(img_load, img_load / len(images)))

            self.images = images
            self.images_name = images_name
            self.masks = None

            self.maskPath = instancePath + "/instance_mask"
            self.obj_names = os.listdir(self.maskPath)

    def get_npSaved(self, path, mode):
        data = None
        if mode == "box":
            if "box_info.npy" in os.listdir(path):
                npfile = path + "/box_info.npy"
                data = np.load(npfile)
                data = data.tolist()
        elif mode == "seed":
            if "seed_info.npy" in os.listdir(path):
                npfile = path + "/seed_info.npy"
                data = np.load(npfile)
                data = data.tolist()
                for d in data:
                    frame, _ = d
                    f = int(frame)
                    d[0] = f
        return data

    def list_chunk(self, lst, n):
        return [lst[i:i+n] for i in range(0, len(lst), n)]

    def get_new_instance_frame(self, seed_frame, shot_list, seed_info, oom_frame):
        start = shot_list[0]
        end = shot_list[len(shot_list) - 1]

        new_instance_frame = seed_frame
        if oom_frame:
            mid = start + int((end - start + 1) / 2)  # 중간 값 찾기
            min_gap = 100
            for info in seed_info:
                instance_frame, score = info
                if instance_frame not in oom_frame:
                    gap = abs(mid - instance_frame)
                    if min_gap > gap:
                        min_gap = gap
                        new_instance_frame = instance_frame
        else:
            forward_length = seed_frame - start + 1
            backward_length = end - seed_frame + 1
            # seed frame 위치 변경 (oom이 발생하지 않는 범위 & Score가 높은 순서)
            if forward_length > 54 or backward_length > 54:
                max_score = 0
                for info in seed_info:
                    instance_frame, score = info
                    if instance_frame - start + 1 < 54 and end - instance_frame + 1 < 54:
                        if score > max_score:
                            new_instance_frame = instance_frame
                            max_score = score
            else:
                new_instance_frame = seed_frame

        if seed_frame not in shot_list:
            print("@@ seed frame not in shot list ..!")
            max_score = 0
            min_gap = 100
            mid = start + int((end - start + 1) / 2)  # 중간 값 찾기
            for info in seed_info:
                instance_frame, score = info
                if instance_frame in shot_list:
                    if instance_frame - start + 1 < 54 and end - instance_frame + 1 < 54:
                        if score > max_score:
                            new_instance_frame = instance_frame
                            max_score = score
                    else:
                        if instance_frame not in oom_frame:
                            gap = abs(mid - instance_frame)
                            if min_gap > gap:
                                min_gap = gap
                                new_instance_frame = instance_frame
            print("new seed frame (0) : ", new_instance_frame)

        if seed_frame not in shot_list:
            print("@@ seed frame not in shot list ..!")
            mid = start + int((end - start + 1) / 2)
            new_instance_frame = mid
            print("new seed frame (0) : ", new_instance_frame)

        return new_instance_frame

    def get_frameNumber(self, obj_name):
        obj_maskPath = self.maskPath + "/" + obj_name
        fileNames = sorted(glob.glob(os.path.join(obj_maskPath, "*.png")))
        frameNumber = []
        masks = []
        for f in fileNames:
            # mask = cv2.imread(f, 0)
            mask = Image.open(f)
            np_mask = np.array(mask)
            # binary_mask = np_mask * 1
            # binary_mask = np_mask * 255
            binary_mask = np_mask
            fileName = os.path.basename(f)
            name, _ = os.path.splitext(fileName)
            num = int(name)
            frameNumber.append(num)
            masks.append(binary_mask)
        self.masks = masks
        return frameNumber

    def get_masks(self, this_mask):
        num_object = self.num_objects

        interected_list = []
        frame_list = []

        interected_list.append(np.array(this_mask))
        frame_list.append(np.array(this_mask))

        frames = np.stack(frame_list, axis=0)
        interecteds = np.stack(interected_list, axis=0)

        load_mask_labels = np.unique(interecteds[0])
        # print(load_mask_labels, "np.unique")

        if len(load_mask_labels) < num_object:
            load_mask_labels = load_mask_labels[load_mask_labels != 0]
            # print(load_mask_labels)
            extra = np.zeros((num_object - len(load_mask_labels)), dtype='uint8')
            extra = np.full_like(extra, np.max(load_mask_labels) + 1)
            load_mask_labels = np.append(load_mask_labels, extra)

        setted_labels = np.zeros((num_object), dtype='uint8')
        labels = setted_labels + load_mask_labels
        interecteds = torch.from_numpy(all_to_onehot(interecteds, labels)).float()  ## [3, 4, 12, 0 0 0 0 ,,, 0]
        interecteds = aggregate_sbg(interecteds, keep_bg=True, hard=True)
        # interecteds = interecteds.unsqueeze(2)

        return frames, interecteds

    def labeling_mask(self, current_masks, tracked_boxes, frames, fnames, label_range, label_name, outputPath, save_mode):
        # def labeling_mask(current_masks, tracked_boxes, frames, save):
        """
        Args:
            current_masks: VOS segmentation result masks(0 , 1)
            tracked_boxes : label coordinates ([{1: [x0, y0, x1, y1], 2: [x0, y0, x1, y1] ... }, {1: [x0, y0, x1, y1], 2: [x0, y0, x1, y1], ... } ... ])

        Returns: current_masks are colored by label id (0, 1, 2, ... n)
        """
        print("Output Path : ", outputPath)

        save = False
        if outputPath is not None:
            save = True

        if len(label_range) > 0:
            print("=======================================")
            print("Run Labeling Mask (Label : {})".format(label_name))
            print("Save Frames : ", label_range)
            label_frames = []
            label_num = len(label_range)

            for i, num in enumerate(label_range):
                current_mask = current_masks[num - label_range[0]]  # // num -> i로 수정
                tracked_id_box = tracked_boxes[i]
                single_label_mask = np.copy(current_mask)
                total_label_mask = np.copy(current_mask)

                frame = frames[num - label_range[0]]
                fname = fnames[num - label_range[0]]  # 이미지 이름

                frame_height = current_masks.shape[1]
                frame_width = current_masks.shape[2]

                if save_mode == 0:
                    # Labeling (Only input Label Value)
                    single_label_mask = single_label_mask * int(1)

                else:
                    ids = list(tracked_id_box.keys())
                    # Labeling (All labels in frame)
                    for id in ids:
                        total_label_mask = total_label_mask * int(id)

                # print("label box : ", tracked_id_box)
                label_box = tracked_id_box
                crop_mask, crop_img = crop_roi_in_labeling_image(single_label_mask, frame, label_box, frame_height)

                if save:
                    str_label = label_name
                    if save_mode == 0:
                        save_single(outputPath, str_label, single_label_mask, crop_mask, crop_img, fname)
                    elif save_mode == 1:
                        save_all(outputPath, str_label, total_label_mask, frames, num)
                    elif save_mode == 2:
                        save_all_singel(outputPath, str_label, single_label_mask, crop_mask, crop_img, total_label_mask,
                                        frames, num)
                    print('\r', "Save Frame : " + outputPath + "({} / {})".format(i, label_num - 1), end='')

    def run_propagate(self):
        obj_names = self.obj_names

        for n, obj_name in enumerate(obj_names):
            if n >= 0:
                shot_cut_list = self.get_frameNumber(obj_name)
                length = len(shot_cut_list)
                start = shot_cut_list[0]
                end = shot_cut_list[length - 1] + 1

                propagate_range = shot_cut_list

                obj_savePath = self.maskPath + "/" + obj_name
                tracked_box_info = self.get_npSaved(obj_savePath, "box")
                seed_info = self.get_npSaved(obj_savePath, "seed")

                # TODO : seed info 데이터 가져오기 (instance 모듈에서 생성해야함)
                instance_frame, score = seed_info[0]
                # TODO

                if len(propagate_range) <= 1:
                    pass
                else:
                    print("")
                    print("---------------------------------------")
                    print("Object name :", obj_name)
                    print("Instance Seed Frame :", instance_frame)
                    print("Propagate_range : ({} ~ {})".format(start, end - 1))
                    print("---------------------------------------")

                    remain_frames = end - start
                    print("Propagation is started (Label : {})".format(n))
                    print("Remain_frames : ", remain_frames)
                    done = True
                    oom_frame = []
                    reduce = 0
                    shot_cut_frames = shot_cut_list.copy()
                    job_frames = 50

                    while done:
                        # Frame 분할 (100 이하로)
                        if end - start + 1 > job_frames - reduce:
                            print("# Divide Frames")
                            # if job_frames - reduce <= 10:
                            #     div = int((job_frames - reduce) / 2)
                            #     div_lists = self.list_chunk(shot_cut_frames, div)
                            # else:
                            div_lists = self.list_chunk(shot_cut_frames, job_frames - reduce)
                        else:
                            print("# Non-Divide Frames")
                            div_lists = [shot_cut_frames]

                        print("# Divide Lists : ", div_lists)
                        for shot_list in div_lists:
                            print("# Shot List : ", shot_list)
                            start = shot_list[0]
                            end = shot_list[len(shot_list) - 1]

                            img_start = self.images_name.index(shot_list[0])
                            img_end = self.images_name.index(shot_list[len(shot_list) - 1])

                            print("img start ", img_start)
                            print("img end ", img_end)

                            images = self.images[img_start: img_end + 1]
                            images_name = self.images_name[img_start:img_end + 1]
                            self.torch_images = images_to_torch(images, device='cpu')
                            self.processor = InferenceCore(self.prop_model, self.fusion_model, self.torch_images,
                                                           self.num_objects, mem_freq=self.mem_freq, mem_profile=0)

                            masks = self.masks
                            # OOM 발생이 나지 않는 Seed Frame 선택
                            new_instance_frame = self.get_new_instance_frame(instance_frame, shot_list, seed_info, oom_frame)
                            print("new seed frame : ", new_instance_frame)
                            self.cursur = new_instance_frame - start
                            print(self.cursur)

                            # mask_idx = shot_cut_list.index(new_instance_frame)
                            # seed_mask = masks[mask_idx]
                            instance_idx = shot_cut_list.index(new_instance_frame)

                            seed_mask = masks[instance_idx]
                            fMask, iMask = self.get_masks(seed_mask)

                            frame_masks = fMask
                            instance_masks = iMask

                            self.current_mask = frame_masks
                            interected, padd = pad_divide_by(instance_masks, 16, instance_masks.shape[-2:])
                            self.interacted_mask = interected
                            torch.cuda.empty_cache()

                            # Forward / Backward 분할 진행
                            forward_range = [new_instance_frame - start, end + 1 - start]
                            backward_range = [start - start, new_instance_frame - start]
                            print("## Self.cursur : ", self.cursur)
                            try:
                                forward_masks = self.processor.interact_div(self.interacted_mask, self.cursur, None,
                                                                            None, forward_range, True)
                                self.processor.interact_clear(self.torch_images, self.num_objects)
                                if forward_masks is not None:
                                    backward_masks = self.processor.interact_div(self.interacted_mask, self.cursur,
                                                                                 None, None, backward_range, False)
                                    self.processor.interact_clear(self.torch_images, self.num_objects)
                                else:
                                    print("No Mask in Forward")
                                    backward_masks = None
                                    self.processor.interact_clear(self.torch_images, self.num_objects)

                                if forward_masks is not None and backward_masks is not None:
                                    self.current_mask = np.concatenate((backward_masks, forward_masks), axis=0)
                                    self.processor.interact_clear(self.torch_images, self.num_objects)
                                    not_oom = True
                                else:
                                    self.processor.interact_clear(self.torch_images, self.num_objects)
                                    not_oom = False
                                    oom_frame.append(new_instance_frame)
                                    reduce += 10
                            except RuntimeError:
                                print('\033[31m' + 'Warning : Out of Memory !!' + '\033[0m')
                                self.processor.interact_clear(self.torch_images, self.num_objects)
                                not_oom = False
                                oom_frame.append(new_instance_frame)
                                reduce += 10
                            if not_oom:
                                propagate_range = shot_list
                                self.labeling_mask(self.current_mask, tracked_box_info, images, images_name,
                                                   propagate_range, obj_name, self.outputPath, self.save_mode)
                                finish_frames = len(shot_list)
                                remain_frames -= finish_frames
                                if remain_frames != 0:
                                    for shot in shot_list:
                                        shot_cut_frames.remove(shot)
                                    if shot_cut_frames == []:
                                        remain_frames = 0
                                print("DO Job : {} / Remain : {}".format(finish_frames, remain_frames))
                            else:
                                break

                            del self.interacted_mask
                            del self.current_mask
                            torch.cuda.empty_cache()

                        print("@@ Remain Frames : ", remain_frames)
                        if remain_frames <= 0:
                            done = False
                        if job_frames - reduce <= 0:
                            done = False
                        print("Done Status : ", done)

        torch.cuda.empty_cache()

def main(imagePath, instancePath):
    fs = "FusionSegmentation/modules/"
    prop_net = fs + 'saves/propagation_model.pth'
    fusion_model = fs + 'saves/fusion.pth'
    s2m_model = fs + 'saves/s2m.pth'
    fbrs_model = fs + 'saves/resnet50_dh128_lvis.pth'
    modelInfo = [prop_net, fusion_model, s2m_model, fbrs_model]

    vos_processor = vocModule(modelInfo, imagePath, instancePath)
    vos_processor.run_propagate()