import cv2
import numpy as np
import argparse
import face_recognition
import math

import torch
import torchvision.transforms as transforms
from FaceSearch_utils.ddfa import ToTensor, Normalize
from model_building import SynergyNet
from FaceSearch_utils.inference import predict_pose

from FaceSearch_utils.inference import crop_img
from box_util import remove_overlap_bbox, sort_bbox

IMG_SIZE = 120

import run_faceSearch as run_fs

from scipy.signal import savgol_filter
import matplotlib.pyplot as plt  # 임시 사용 지울거임
from FusionSegmentation.modules.utility.deepface_utils import deepface_detect


###################################################################################################################
# FACE DETECTION PART
###################################################################################################################

def face_distance_to_conf(face_distance, face_match_threshold=0.6):
    if face_distance > face_match_threshold:
        range = (1.0 - face_match_threshold)
        linear_val = (1.0 - face_distance) / (range * 2.0)
        return linear_val
    else:
        range = face_match_threshold
        linear_val = 1.0 - (face_distance / (range * 2.0))
        return linear_val + ((1.0 - linear_val) * math.pow((linear_val - 0.5) * 2, 0.2))


def face_detection(img):
    face_locations = face_recognition.face_locations(img, number_of_times_to_upsample=1, model="cnn")  # face_location = (top, right, bottom, left)
    face_locations = remove_overlap_bbox(face_locations)  # Remove Overlap Detection Results

    faces = []
    rects = []
    for face_location in face_locations:
        ymin, xmax, ymax, xmin = face_location
        score = float(1.0)
        rect = [xmin, ymin, xmax, ymax, score]

        roi_box = rect.copy()

        # enlarge the bbox a little and do a square crop
        HCenter = (rect[1] + rect[3]) / 2
        WCenter = (rect[0] + rect[2]) / 2
        side_len = roi_box[3] - roi_box[1]
        margin = side_len * 1.2 // 2
        roi_box[0], roi_box[1], roi_box[2], roi_box[3] = WCenter - margin, HCenter - margin, \
                                                         WCenter + margin, HCenter + margin

        face = crop_img(img, roi_box)
        face = cv2.resize(face, dsize=(IMG_SIZE, IMG_SIZE), interpolation=cv2.INTER_LINEAR)
        rects.append(rect)
        faces.append(face)
    return rects, faces


def deepface_detection(img, face_model):
    detected_face, img_region = deepface_detect(img, face_model, detector_backend='retinaface', align=True)
    if detected_face is not None:
        x, y, w, h = img_region
        rect = [x, y, x + w, y + h]
        face = cv2.resize(detected_face, dsize=(IMG_SIZE, IMG_SIZE), interpolation=cv2.INTER_LINEAR)
        rects = [rect]
        faces = [face]
    else:
        rects = []
        faces = []
    return rects, faces


def get_face_information(images, mode=None, face_model=None):
    faces_in_frames = []
    rects_in_frames = []

    max_num_face = 0
    pre_rects = None
    for idx, image in enumerate(images):
        # print("Processing : ({} / {})".format(idx, len(query_imgs) - 1))
        # print("image count : ", idx)
        if mode is None:
            locations, faces_img = face_detection(image)  # Input (single object) --> Output (single face)
        else:
            locations, faces_img = deepface_detection(image, face_model)

        # print("pre locations : ", pre_rects)
        # print("locations     : ", locations)

        num_face = len(faces_img)
        if num_face > max_num_face:
            max_num_face = num_face

        # Location index match, up-to pre-location information (Class Miss Match 방지)

        # initialize storage
        if pre_rects is None:
            rects = locations.copy()
            faces = faces_img.copy()
        else:
            rects = [None for i in range(len(pre_rects))]
            faces = [None for i in range(len(pre_rects))]

        if idx == 0:
            pre_rects = rects.copy()
        elif not pre_rects:
            rects = locations
        else:
            rects_idx = []
            for location in locations:
                rect_idx = sort_bbox(pre_rects, location)
                rects_idx.append(rect_idx)
            for n, rect_idx in enumerate(rects_idx):
                rects[rect_idx] = locations[n]
                faces[rect_idx] = faces_img[n]
        pre_rects = rects.copy()

        # print("rects         : ", rects)
        # print("    ")

        faces_in_frames.append(faces)
        rects_in_frames.append(rects)
    return faces_in_frames, rects_in_frames, max_num_face


###################################################################################################################
# FACE POSE ESTIMATION PART
###################################################################################################################

def get_face_angle(face, device_id=0):
    # load pre-trained model
    parser = argparse.ArgumentParser()
    args = parser.parse_args()
    # 추후, 모델관리를 위한 모델 경로 수정 및 위치 일원화 필요
    checkpoint_fp = run_fs.faceSearchPath + 'pretrained/best.pth.tar'
    args.arch = 'mobilenet_v2'
    args.devices_id = [device_id]
    args.batch_size = 1
    args.img_size = 120

    checkpoint = torch.load(checkpoint_fp, map_location=lambda storage, loc: storage)['state_dict']

    model = SynergyNet(args)
    model_dict = model.state_dict()

    # because the model is trained by multiple gpus, prefix 'module' should be removed
    for k in checkpoint.keys():
        model_dict[k.replace('module.', '')] = checkpoint[k]

    model.load_state_dict(model_dict, strict=False)
    model = model.cuda()
    model.eval()

    # preparation
    transform = transforms.Compose([ToTensor(), Normalize(mean=127.5, std=128)])
    input = transform(face).unsqueeze(0)
    with torch.no_grad():
        input = input.cuda()
        param = model.forward_test(input)
        param = param.squeeze().cpu().numpy().flatten().astype(np.float32)

    # inferences
    angles = predict_pose(param)  # yaw, pitch, roll
    return angles


def face_pose_estimation(faces):
    # load pre-trained model
    parser = argparse.ArgumentParser()
    args = parser.parse_args()
    # 추후, 모델관리를 위한 모델 경로 수정 및 위치 일원화 필요
    checkpoint_fp = run_fs.faceSearchPath + 'pretrained/best.pth.tar'
    args.arch = 'mobilenet_v2'
    args.devices_id = [0]
    args.batch_size = 1
    args.img_size = 120

    checkpoint = torch.load(checkpoint_fp, map_location=lambda storage, loc: storage)['state_dict']

    model = SynergyNet(args)
    model_dict = model.state_dict()

    # because the model is trained by multiple gpus, prefix 'module' should be removed
    for k in checkpoint.keys():
        model_dict[k.replace('module.', '')] = checkpoint[k]

    model.load_state_dict(model_dict, strict=False)
    model = model.cuda()
    model.eval()

    # preparation
    transform = transforms.Compose([ToTensor(), Normalize(mean=127.5, std=128)])

    poses = []
    for idx, face in enumerate(faces):
        # roi_box = rects_in_frame[idx]

        if face is not None:
            input = transform(face).unsqueeze(0)

            with torch.no_grad():
                input = input.cuda()
                param = model.forward_test(input)
                param = param.squeeze().cpu().numpy().flatten().astype(np.float32)

            # inferences
            angles = predict_pose(param)
            poses.append([angles, face])

    # face orientation
    faces_info = []
    for idx, pose in enumerate(poses):
        angles, face = pose
        # angle ==> [yaw, pitch, roll]
        yaw = round(angles[0], 3)
        pitch = round(angles[1], 3)

        face_info = [idx, yaw, pitch, face]
        faces_info.append(face_info)

    return faces_info


def face_pose_normalize(faces_infoes):
    yaws = []
    pitches = []
    idxs = []
    for faces_info in faces_infoes:
        for idx, face_info in enumerate(faces_info):
            id, yaw, pitch, _ = face_info
            yaws.append(yaw)
            pitches.append(pitch)

    # 다항식 차수 -> 3으로 갈 것

    smooth_yaws1 = savgol_filter(yaws, len(yaws), 3)
    smooth_yaws2 = savgol_filter(yaws, len(yaws), 9)
    smooth_pitches1 = savgol_filter(pitches, len(pitches), 3)
    smooth_pitches2 = savgol_filter(pitches, len(pitches), 9)

    plt.subplot(1, 2, 1)
    plt.plot(yaws, color='blue')
    plt.plot(smooth_yaws1, color='red')
    plt.plot(smooth_yaws2, color="green")
    plt.title('Yaw Values')

    plt.subplot(1, 2, 2)
    plt.plot(pitches, color='blue')
    plt.plot(smooth_pitches1, color="red")
    plt.plot(smooth_pitches2, color="green")
    plt.title('Pitch Values')

    plt.show()
