import glob

import numpy as np
import cv2
import os
import deepfind

from img_util import read_npy, get_query_table_image, get_query_table_feature
from face_util import face_pose_estimation, face_distance_to_conf
from query_util import register_query_table, best_match_query_table, get_query_feature

from deepface import DeepFace
from utility.deepface_utils import feature_verify

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'  # Tensorflow print Only "Error" Message
import tensorflow as tf


def face_query_register(face_imgs, face_features, query_table, angle_table):
    new_query_table = None

    # Get Face Angles (Yaw, Pitch) -> Registering Face Query Table
    # Face Angles Information = [[class id, yaw, pitch, face image], ...]
    # faces_info = face_pose_estimation(face_imgs)

    # for faces_info in normalized_faces_infoes:
    # for face_info in faces_info:
    #     new_query_table = register_query_table(face_info, query_table, angle_table)

    for face, feat in zip(face_imgs, face_features):
        _, angle = feat
        yaw, pitch, _ = angle
        face_info = [yaw, pitch, face]
        new_query_table = register_query_table(face_info, query_table, angle_table, mode="F")
        query_table = new_query_table.copy()

    if query_table is not None:
        if len(query_table) == query_table.count(None):
            query_table = None

    return query_table


def face_query_updater(query_table, face_imgs, face_features, angle_table):
    updated_query_table = None

    for face, feat in zip(face_imgs, face_features):
        _, angle = feat
        yaw, pitch, _ = angle
        face_info = [yaw, pitch, face]

        updated_query_table = register_query_table(face_info, query_table, angle_table, mode="F")
        query_table = updated_query_table.copy()

    return query_table


def run(input_path, sort_path, query_tables, query_names, query_table, angle_table, model=None, model_name=None, gpu_id=None, flip_mode=False):
    if gpu_id != -1:
        device_is = '/device:GPU:{}'.format(gpu_id)  # For Tensorflow
    else:
        device_is = '/CPU'  # For Tensorflow

    view_info = False  # True -> "yaw" data on Class Query Image

    if model is None or model_name is None:
        model_name = "Facenet512"
        model = deepfind.build_model(model_name)

    if query_tables is None:
        query_tables = []
        querys_features = []
    else:
        querys_features = list(np.load(sort_path + "/querys_features.npy", allow_pickle=True))

    dataPath = input_path + "/cache"
    featureFiles = glob.glob(os.path.join(dataPath, "*_features.npy"))
    imagesFiles = glob.glob(os.path.join(dataPath, "*_images.npy"))

    # Load Features Data
    faces_feats = read_npy(dataPath, featureFiles)
    # Load Images Data
    faces_imgs = read_npy(dataPath, imagesFiles)
    print("Load Features / Images is Finished ...")

    empty_query_table = query_table.copy()
    hash_table = []

    with tf.device(device_is):
        for face_imgs, face_features in zip(faces_imgs, faces_feats):
            if not query_tables:  # Query Register (Initialize)
                print("Query Tables are not Exist -> Creating...")
                new_query_table = face_query_register(face_imgs, face_features, empty_query_table, angle_table)

                if new_query_table is not None:
                    query_tables.append(new_query_table)
                    query_table_feature = get_query_table_feature(query_tables[0], model, model_name)
                    querys_features.append(query_table_feature)

                    query_index = len(query_tables) - 1
                    class_name = "Class_" + str(query_index)

                    query_table_image = get_query_table_image(query_tables[0], view_info=view_info)

                    np.save(sort_path + "/" + class_name, query_tables[0])
                    cv2.imwrite(sort_path + "/" + class_name + ".png", query_table_image)

                    hash_element = [class_name, input_path]
                    hash_table.append(hash_element)
                else:
                    print("Face is not Detection, Classification to Unknown")
                    class_name = "Unknown"
                    hash_element = [class_name, input_path]
                    hash_table.append(hash_element)

            else:  # Query Update OR New Face Register
                update_lists, register = deepfeature_recognizer(face_features, querys_features, query_tables, flip_mode, model=model)
                if update_lists:
                    print("Update query table index : ", update_lists)
                    query_index = update_lists[0]
                    print("Query Index : ", query_index)
                    updated_query_table = face_query_updater(query_tables[query_index], face_imgs, face_features, angle_table)

                    if updated_query_table is not None:
                        query_tables[query_index] = updated_query_table
                        query_table_feature = get_query_table_feature(query_tables[query_index], model, model_name)
                        querys_features[query_index] = query_table_feature

                        class_name = "Class_" + str(query_index)
                        query_table_image = get_query_table_image(query_tables[query_index], view_info=view_info)
                        np.save(sort_path + "/" + class_name, query_tables[query_index])
                        cv2.imwrite(sort_path + "/" + class_name + ".png", query_table_image)

                        hash_element = [class_name, input_path]
                        hash_table.append(hash_element)

                elif register:
                    print("New Face Detected Registering ...!")
                    new_query_table = face_query_register(face_imgs, face_features, empty_query_table, angle_table)
                    if new_query_table is not None:
                        query_tables.append(new_query_table)

                        query_index = len(query_tables) - 1

                        query_table_feature = get_query_table_feature(query_tables[query_index], model, model_name)
                        querys_features.append(query_table_feature)

                        class_name = "Class_" + str(query_index)
                        query_table_image = get_query_table_image(query_tables[query_index], view_info=view_info)
                        np.save(sort_path + "/" + class_name, query_tables[query_index])
                        cv2.imwrite(sort_path + "/" + class_name + ".png", query_table_image)

                        hash_element = [class_name, input_path]
                        hash_table.append(hash_element)

                else:
                    print("Face is not Detection, Classification to Unknown")
                    class_name = "Unknown"
                    hash_element = [class_name, input_path]
                    hash_table.append(hash_element)

        np.save(sort_path + "/querys_features.npy", np.array(querys_features))

        return hash_table


def deepfeature_recognizer(face_features, querys_features, query_tables, flip_mode, model=None):
    update_list = []
    update_candidate_list = []
    register_list = False

    # [Deepface Verify]
    # for query_idx, query_features in enumerate(querys_features):

    query_idx = 0
    for query_table, query_features in zip(query_tables, querys_features):
        match_score = 0
        distance_score = 0
        face_cnt = 0

        match_rate = 0
        distance_rate = 0
        # for face_img in face_imgs:
        for face_feature in face_features:
            face_feat, angle = face_feature
            yaw, pitch, _ = angle
            if face_feat is not None:
                face_cnt += 1
                if flip_mode == True:
                    query_feat = get_query_feature(query_features, yaw, query_table=query_table, model=model, flip=True)
                else:
                    query_feat = get_query_feature(query_features, yaw, flip=False)

                if query_feat is None:  # query_face가 없는 경우, face verify를 진행하지 않음
                    face_cnt -= 1  # match_rate & distance_rate 계산 오류 방지
                else:
                    match_result = feature_verify(query_feat, face_feat,
                                                  model_name="Facenet512", metrics="euclidean_l2")

                    match = match_result['verified']
                    threshold = match_result['threshold']
                    face_distance = match_result['distance']

                    distance_cof = face_distance_to_conf(face_distance, face_match_threshold=threshold)
                    if match:
                        match_score += 1.0
                        distance_score += distance_cof

        if match_score == 0 or face_cnt <= 0:
            match_rate = 0
        elif distance_score == 0 or face_cnt <= 0:
            distance_rate = 0
        else:
            match_rate = round(match_score / face_cnt, 2)
            distance_rate = round(distance_score / face_cnt, 3)

        print("Query Table : ", query_idx, "/ match_rate : ", match_rate, "/ distance_rate : ", distance_rate)

        if match_rate >= 0.8:
            update_candidate_list.append([query_idx, distance_rate])
        else:
            register_list = True

        query_idx += 1

    # Find Best Matched Query Table (This Query Table will be updated)
    update_query_table_idx = best_match_query_table(update_candidate_list)
    if update_query_table_idx is not None:
        update_list.append(update_query_table_idx)

    return update_list, register_list
