import torch
import torchvision.transforms as transforms
import numpy as np
import cv2
from utils.ddfa import ToTensor, Normalize
from model_building import SynergyNet
from utils.inference import crop_img, predict_sparseVert, draw_landmarks, predict_denseVert, predict_pose, draw_axis
import argparse
import torch.backends.cudnn as cudnn

cudnn.benchmark = True
import os
import os.path as osp
import glob
# from FaceBoxes import FaceBoxes
from utils.render import render

import face_recognition
from box_util import *

# Following 3DDFA-V2, we also use 120x120 resolution
IMG_SIZE = 120

def read_images(path, mode=None):
    print("Load Images...")
    imgs = []
    files_list = os.listdir(path)
    for file in files_list:
        name, ext = os.path.splitext(file)
        if ext ==".tif" or ext == ".png" or ext == ".jpg":
            img_path = path + "/" + file
            img = face_recognition.load_image_file(img_path, mode='RGB')
            if mode == 1:
                name_img = [name, img]
                imgs.append(name_img)
            else:
                imgs.append(img)
    print("Done")
    return imgs


def read_query_images(path):
    print("Load Images...")
    imgs = []
    files_list = os.listdir(path)
    for file in files_list:
        name, ext = os.path.splitext(file)
        if ext == ".tif" or ext == ".png" or ext == ".jpg":
            img_path = path + "/" + file
            img = face_recognition.load_image_file(img_path, mode='RGB')
            h, w, _ = img.shape
            if h < 128 or w < 128:
                img = cv2.resize(img, dsize=(128, 128), interpolation=cv2.INTER_LINEAR)
            name_img = [name, img]
            imgs.append(name_img)
    print("Done")
    return imgs


def get_face_location(img):
    face_locations = face_recognition.face_locations(img, model="cnn")  # face_location = (top, right, bottom, left)
    rects = []
    for face_location in face_locations:
        ymin, xmax, ymax, xmin = face_location
        score = float(1.0)
        bbox = [xmin, ymin, xmax, ymax, score]
        rects.append(bbox)
    return rects


def recognize_faces(target_imgs, gallery_img, parameter):
    model, jitters, lerance = parameter

    target_face_encodings = []
    target_face_names = []
    for target_name, target_img in target_imgs:
        target_h, target_w, _ = target_img.shape
        target_face_locations = [(0, target_w, target_h, 0)]
        # target_face_locations = face_recognition.face_locations(target_img, model="cnn")
        # print("target location", target_face_locations)
        target_face_encoding = face_recognition.face_encodings(target_img, target_face_locations, num_jitters=jitters)[0]
        target_face_encodings.append(target_face_encoding)
        # target_face_encodings.append(target_img)
        target_face_names.append(target_name)

    if model == "cnn":
        gallery_locations = face_recognition.face_locations(gallery_img, model="cnn")
    else:
        gallery_locations = face_recognition.face_locations(gallery_img)

    gallery_encodings = face_recognition.face_encodings(gallery_img, gallery_locations, num_jitters=jitters)

    face_names = []
    scores = []
    for gallery_encoding in gallery_encodings:
        matches = face_recognition.compare_faces(target_face_encodings, gallery_encoding, tolerance=lerance)

        name = "Unknown"

        face_distances = face_recognition.face_distance(target_face_encodings, gallery_encoding)
        best_match_index = np.argmin(face_distances)
        if matches[best_match_index]:
            name = target_face_names[best_match_index]

        face_names.append(name)
        scores.append(face_distances)

    # return gallery_locations, face_names

    result_image = cv2.cvtColor(gallery_img.copy(), cv2.COLOR_RGB2BGR)

    for (top, right, bottom, left), name, score in zip(gallery_locations, face_names, scores):
        score = str(score)
        if name != "Unknown":
            cv2.rectangle(result_image, (left, top), (right, bottom), (0, 255, 0), 2)
            cv2.putText(result_image, name, (left, top - 10), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 1)
            cv2.putText(result_image, score, (left - 150, top - 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 1)
        else:
            cv2.rectangle(result_image, (left, top), (right, bottom), (0, 0, 255), 2)
            cv2.putText(result_image, name, (left, top - 10), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 1)
            cv2.putText(result_image, score, (left - 150, top - 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 1)

    return result_image

def main(args):
    # load pre-trained model
    checkpoint_fp = 'pretrained/best.pth.tar'
    args.arch = 'mobilenet_v2'
    args.devices_id = [0]

    checkpoint = torch.load(checkpoint_fp, map_location=lambda storage, loc: storage)['state_dict']

    model = SynergyNet(args)
    model_dict = model.state_dict()

    # because the model is trained by multiple gpus, prefix 'module' should be removed
    for k in checkpoint.keys():
        model_dict[k.replace('module.', '')] = checkpoint[k]

    model.load_state_dict(model_dict, strict=False)
    model = model.cuda()
    model.eval()

    # face detector
    # TODO : Compile "FaceBoxes" (not official to Windows) // Temporary USE "Face_Recognition"
    # face_boxes = FaceBoxes()

    # preparation
    transform = transforms.Compose([ToTensor(), Normalize(mean=127.5, std=128)])
    if osp.isdir(args.files):
        print("files is directory")
        if not args.files[-1] == '/':
            args.files = args.files + '/'
        files = sorted(glob.glob(args.files + '*.jpg'))
        if not files:
            files = sorted(glob.glob(args.files + '*.png'))
        if not files:
            files = sorted(glob.glob(args.files + '*.tif'))
    else:
        files = [args.files]

    print("Load files : ", files)

    pre_boxes = None
    num_objs = 0
    frames_faces = []

    for idx_fp, img_fp in enumerate(files):
        print("Process the image: ", img_fp)

        img_ori = cv2.imread(img_fp, 1)

        # crop faces
        # TODO : Compile "FaceBoxes" Error // Temporary USE "Face_Recognition"
        # rects = face_boxes(img_ori)  # rect = [xmin, ymin, xmax, ymax, score]
        boxes = get_face_location(img_ori)  # face_recognition

        rects = boxes.copy()
        if idx_fp == 0:
            pre_boxes = boxes.copy()
        else:
            boxes_idx = []
            for box in boxes:
                box_idx = sort_bbox(pre_boxes, box)
                boxes_idx.append(box_idx)
            for n, b_idx in enumerate(boxes_idx):
                rects[b_idx] = boxes[n]

        pre_boxes = rects.copy()

        # storage
        pts_res = []
        poses = []
        vertices_lst = []

        for idx, rect in enumerate(rects):
            if idx + 1 > num_objs:
                num_objs = idx + 1

            roi_box = rect

            # enlarge the bbox a little and do a square crop
            HCenter = (rect[1] + rect[3]) / 2
            WCenter = (rect[0] + rect[2]) / 2
            side_len = roi_box[3] - roi_box[1]
            margin = side_len * 1.2 // 2
            roi_box[0], roi_box[1], roi_box[2], roi_box[
                3] = WCenter - margin, HCenter - margin, WCenter + margin, HCenter + margin

            img = crop_img(img_ori, roi_box)
            img = cv2.resize(img, dsize=(IMG_SIZE, IMG_SIZE), interpolation=cv2.INTER_LINEAR)
            # cv2.imwrite(f'validate_{idx}.png', img)

            input = transform(img).unsqueeze(0)
            with torch.no_grad():
                input = input.cuda()
                param = model.forward_test(input)
                param = param.squeeze().cpu().numpy().flatten().astype(np.float32)

            # inferences
            lmks = predict_sparseVert(param, roi_box, transform=True)
            vertices = predict_denseVert(param, roi_box, transform=True)
            angles, translation = predict_pose(param, roi_box)

            # pts_res.append(lmks)
            # vertices_lst.append(vertices)
            poses.append([angles, translation, lmks])

        # if not osp.exists(f'inference_output/rendering_overlay/'):
        #     os.makedirs(f'inference_output/rendering_overlay/')
        # if not osp.exists(f'inference_output/landmarks/'):
        #     os.makedirs(f'inference_output/landmarks/')

        if not osp.exists(f'inference_output/poses/'):
            os.makedirs(f'inference_output/poses/')

        name = img_fp.rsplit('/', 1)[-1][:-4]
        img_ori_copy = img_ori.copy()

        # # mesh
        # render(img_ori, vertices_lst, alpha=0.6, wfp=f'inference_output/rendering_overlay/{name}.jpg')
        # # landmarks
        # draw_landmarks(img_ori_copy, pts_res, wfp=f'inference_output/landmarks/{name}.jpg')

        # face orientation
        img_axis_plot = img_ori_copy
        i = 0
        faces_info = []
        for angles, translation, lmks in poses:
            # angle : yaw, pitch, roll
            img_axis_plot = draw_axis(img_axis_plot, angles[0], angles[1],
                                      angles[2], translation[0], translation[1], size=50, pts68=lmks)
            yaw = round(angles[0], 3)
            pitch = round(angles[1], 3)
            ret = rects[i]
            xmin, ymin, xmax, ymax, score = ret

            str_yaw = str(yaw)
            str_pitch = str(pitch)
            cv2.putText(img_axis_plot, str_yaw, (int(xmin), int(ymin + 30)), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 1)
            cv2.putText(img_axis_plot, str_pitch, (int(xmin), int(ymin + 60)), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 255), 1)
            start = (int(xmin), int(ymin))
            end = (int(xmax), int(ymax))
            cv2.rectangle(img_axis_plot, start, end, (255, 255, 0), 2)

            # TEMP TEST
            # face_info = [i, yaws, img_axis_plot]

            face_img = get_face_image(img_ori, ret)
            face_info = [i, yaw, pitch, face_img]

            # View Information
            # start = (int(xmin), int(ymin))
            # end = (int(xmax), int(ymax))
            # cv2.rectangle(img_axis_plot, start, end, (255, 255, 0), 2)
            # org = (int(xmin), int(ymin - 30))
            # str_yaw = str(yaw)
            # cv2.putText(img_axis_plot, str_yaw, org, cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 1)
            # cl_name = "person_" + str(i)
            # cv2.putText(img_axis_plot, cl_name, org, cv2.FONT_HERSHEY_SIMPLEX, 2, (255, 0, 0), 2)
            i += 1

            faces_info.append(face_info)

        frames_faces.append(faces_info)

        # wfp = f'inference_output/poses/{name}.jpg'
        # cv2.imwrite(wfp, img_axis_plot)
        # print(f'Save pose result to {wfp}')

    target_yaw_angles = [-90, -45, 0, 45, 90]
    target_pitch_angles = [-45, 0, 45]
    # target_angles = [-90, 0, 90]
    thresh = 5

    query_faces_array = []
    for num in range(num_objs):
        print(num)
        class_faces = get_class_info(frames_faces, num)
        query_faces = get_face_angle_query(class_faces, num, target_yaw_angles, target_pitch_angles, thresh)

        # View Query Faces
        # view_query_faces(query_faces)

        query_face_array = []
        for query_face in query_faces:
            if query_face is not None:
                direct = query_face[0]
                data = query_face[1]
                if data is not None:
                    yaw = data[0]
                    pitch = data[1]
                    face = data[2]

                    query_face_array.append(face)

        query_faces_array.append(query_face_array)

    return query_faces_array


def view_query_faces(query_faces):
    result_img = np.zeros((360, 600, 3), np.uint8)

    y_idx = 0
    for n, query_face in enumerate(query_faces):
        direct = query_face[0]
        data = query_face[1]
        if data is not None:
            face = data[2]
            print("GET FACE", direct)
        else:
            face = np.zeros((120, 120, 3), np.uint8)

        resize_face = None
        face_h, face_w, _ = face.shape
        if face_h < 240 or face_w < 240:
            resize_face = cv2.resize(face, dsize=(120, 120), interpolation=cv2.INTER_LINEAR)
        elif face_h > 240 or face_w > 240:
            resize_face = cv2.resize(face, dsize=(120, 120), interpolation=cv2.INTER_AREA)
        elif face_h == 120 and face_w == 120:
            resize_face = face.copy()

        if direct > 4:
            x_idx = direct % 5
            y_idx = 1
            if direct > 9:
                y_idx = 2
        else:
            x_idx = direct
            y_idx = 0

        print("x idx : {} ' y idx : {}".format(x_idx, y_idx))

        px = x_idx * 120
        py = y_idx * 120
        result_img[py: py + 120, px: px + 120] = resize_face

    cv2.imshow("Query Faces", result_img)
    cv2.waitKey(0)

def recognize_face_images(target_imgs, gallery_imgs, parameter):
    model, jits, lerance = parameter

    target_face_encodings = []
    target_face_names = []
    for target_name, target_img in target_imgs:
        target_h, target_w, _ = target_img.shape
        target_face_locations = [(0, target_w, target_h, 0)]
        target_face_encoding = face_recognition.face_encodings(target_img, target_face_locations, num_jitters=jits)[0]
        target_face_encodings.append(target_face_encoding)
        target_face_names.append(target_name)

    results = []
    class_scores = []
    for n, gallery_img in enumerate(gallery_imgs):
        gallery_locations = face_recognition.face_locations(gallery_img, model="cnn")

        boxes = convert_box_format(gallery_locations)
        rects = boxes.copy()
        if n == 0:
            pre_boxes = boxes.copy()
        else:
            boxes_idx = []
            for box in boxes:
                box_idx = sort_bbox(pre_boxes, box)
                boxes_idx.append(box_idx)
            for n, b_idx in enumerate(boxes_idx):
                rects[b_idx] = boxes[n]

        pre_boxes = rects.copy()

        gallery_locations = convert_location_format(rects)
        gallery_encodings = face_recognition.face_encodings(gallery_img, gallery_locations, num_jitters=jits)

        face_names = []
        scores = []
        for gallery_encoding in gallery_encodings:
            matches = face_recognition.compare_faces(target_face_encodings, gallery_encoding, tolerance=lerance)
            name = "Unknown"
            face_distances = face_recognition.face_distance(target_face_encodings, gallery_encoding)
            best_match_index = np.argmin(face_distances)
            if matches[best_match_index]:
                name = target_face_names[best_match_index]

            face_names.append(name)
            scores.append(face_distances)

        # return gallery_locations, face_names
        result_image = cv2.cvtColor(gallery_img.copy(), cv2.COLOR_RGB2BGR)

        number = 0
        class_score = []
        for (top, right, bottom, left), name, score in zip(gallery_locations, face_names, scores):
            class_name = "Person_" + str(number)
            class_score.append(class_name)
            for s in score:
                class_score.append(s)
            score = str(score)

            if name != "Unknown":
                cv2.rectangle(result_image, (left, top), (right, bottom), (0, 255, 0), 2)
                cv2.putText(result_image, name, (left, top - 10), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 1)
                cv2.putText(result_image, class_name, (left, bottom + 10), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 1)
                cv2.putText(result_image, score, (left - 150, top - 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 1)
            else:
                cv2.rectangle(result_image, (left, top), (right, bottom), (0, 0, 255), 2)
                cv2.putText(result_image, name, (left, top - 10), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 1)
                cv2.putText(result_image, class_name, (left, bottom + 10), cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 1)
                cv2.putText(result_image, score, (left - 150, top - 30), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 0, 255), 1)
            number += 1

        # print(class_score)
        results.append(result_image)
        class_scores.append(class_score)

    return results, class_scores

if __name__ == '__main__':
    # Pose Estimation Parser
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--files', default='img/samples/heroine_1',
                        help='path to a single image or path to a folder containing multiple images')
    parser.add_argument("--png", action="store_true", help="if images are with .png extension")
    parser.add_argument('--img_size', default=120, type=int)
    parser.add_argument('-b', '--batch-size', default=1, type=int)
    args = parser.parse_args()

    print("ARGS")
    print(args)

    # query_faces = main(args)
    # num_objects = len(query_faces)
    main(args)

    query_path = "img/samples/compare/query2"
    gallery_path = "img/samples/compare/test2"

    # jitters = 100  # [1, 5, 10, 20, 50, 100]
    jitters = [1, 5, 10, 20, 50, 100]
    lerance = 0.55
    parameter = ["cnn", jitters, lerance]

    # query_faces = read_query_images(query_path)
    # # query_faces = read_images(query_path, mode=1)
    # gallery_images = read_images(gallery_path)

    # # save_path = gallery_path + "_query1_result3_(jitter={}, tolearn={})".format(jitters, lerance)
    # # os.makedirs(save_path, exist_ok=True)
    #
    # # for n, gallery_image in enumerate(gallery_images):
    # #     result_image = gallery_image.copy()
    # #     result = recognize_faces(query_faces, gallery_image, parameter)
    # #
    # #     file_name = str(n).zfill(5) + ".png"
    # #     cv2.imwrite(save_path + "/" + file_name, result)
    #
    # results, scores = recognize_face_images(query_faces, gallery_images, parameter)
    #
    # csv_path = gallery_path + "_query1_result_(jitter={}, tolerance={}).csv".format(jitters, lerance)
    # S = np.array(scores)
    # np.savetxt(csv_path, S, fmt="%s", delimiter=",")

    # For TEST of Jitter Performance
    # for jit in jitters:
    #     parameter = parameter = ["cnn", jit, lerance]
    #     results, scores = recognize_face_images(query_faces, gallery_images, parameter)
    #
    #     csv_path = gallery_path + "_query1_result_(jitter={}, tolerance={}).csv".format(jit, lerance)
    #     S = np.array(scores)
    #     np.savetxt(csv_path, S, fmt="%s", delimiter=",")