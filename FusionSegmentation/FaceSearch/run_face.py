import torch
import torchvision.transforms as transforms
import numpy as np
import cv2
from utils.ddfa import ToTensor, Normalize
from model_building import SynergyNet
from utils.inference import crop_img, predict_sparseVert, draw_landmarks, predict_denseVert, predict_pose, draw_axis
import argparse
import torch.backends.cudnn as cudnn

cudnn.benchmark = True
import os
import os.path as osp
import glob
# from FaceBoxes import FaceBoxes
from utils.render import render

import face_recognition
from img_util import *
from box_util import *

# Following 3DDFA-V2, we also use 120x120 resolution
IMG_SIZE = 120


# FACE DETECTION
def face_detection(img):
    face_locations = face_recognition.face_locations(img, model="cnn")  # face_location = (top, right, bottom, left)
    face_locations = remove_overlap_bbox(face_locations)  # Remove Overlap Detection Results

    faces = []
    rects = []
    for face_location in face_locations:
        ymin, xmax, ymax, xmin = face_location
        score = float(1.0)
        rect = [xmin, ymin, xmax, ymax, score]

        roi_box = rect.copy()

        # enlarge the bbox a little and do a square crop
        HCenter = (rect[1] + rect[3]) / 2
        WCenter = (rect[0] + rect[2]) / 2
        side_len = roi_box[3] - roi_box[1]
        margin = side_len * 1.2 // 2
        roi_box[0], roi_box[1], roi_box[2], roi_box[
            3] = WCenter - margin, HCenter - margin, WCenter + margin, HCenter + margin

        face = crop_img(img, roi_box)
        face = cv2.resize(face, dsize=(IMG_SIZE, IMG_SIZE), interpolation=cv2.INTER_LINEAR)
        rects.append(rect)
        faces.append(face)
    return rects, faces


def get_face_image_bbox(query_imgs):
    faces_in_frames = []
    rects_in_frames = []

    max_num_face = 0
    for idx, query_img in enumerate(query_imgs):
        # print("Processing : ({} / {})".format(idx, len(query_imgs) - 1))
        locations, faces_img = face_detection(query_img)  # Input (single object) --> Output (single face)

        num_face = len(faces_img)
        if num_face > max_num_face:
            max_num_face = num_face

        # Location index match, up-to pre-location information (Class Miss Match 방지)
        rects = locations.copy()
        faces = faces_img.copy()

        if idx == 0:
            pre_rects = rects.copy()
        else:
            rects_idx = []
            for location in locations:
                rect_idx = sort_bbox(pre_rects, location)
                rects_idx.append(rect_idx)
            for n, rect_idx in enumerate(rects_idx):
                rects[rect_idx] = locations[n]
                faces[rect_idx] = faces_img[n]
        pre_rects = rects.copy()

        faces_in_frames.append(faces)
        rects_in_frames.append(rects)
    return faces_in_frames, rects_in_frames, max_num_face

# FACE RECOGNITION


# FACE POSE ESTIMATION
def face_pose_estimation(faces_in_frame):
    # load pre-trained model
    parser = argparse.ArgumentParser()
    args = parser.parse_args()
    checkpoint_fp = 'pretrained/best.pth.tar'
    args.arch = 'mobilenet_v2'
    args.devices_id = [0]
    args.batch_size = 1
    args.img_size = 120

    checkpoint = torch.load(checkpoint_fp, map_location=lambda storage, loc: storage)['state_dict']

    # print("POSE ESTIMATION ARGS")
    # print(args)

    model = SynergyNet(args)
    model_dict = model.state_dict()

    # because the model is trained by multiple gpus, prefix 'module' should be removed
    for k in checkpoint.keys():
        model_dict[k.replace('module.', '')] = checkpoint[k]

    model.load_state_dict(model_dict, strict=False)
    model = model.cuda()
    model.eval()

    # preparation
    transform = transforms.Compose([ToTensor(), Normalize(mean=127.5, std=128)])

    poses = []
    for idx, face in enumerate(faces_in_frame):

        input = transform(face).unsqueeze(0)
        with torch.no_grad():
            input = input.cuda()
            param = model.forward_test(input)
            param = param.squeeze().cpu().numpy().flatten().astype(np.float32)

        # inferences
        angles, translation = predict_pose(param)
        poses.append([angles, face])

    # face orientation
    faces_info = []
    for idx, pose in enumerate(poses):
        angles, face = pose
        # angle ==> [yaw, pitch, roll]
        yaw = round(angles[0], 3)
        pitch = round(angles[1], 3)

        face_info = [idx, yaw, pitch, face]
        faces_info.append(face_info)

    return faces_info


# Query Table
def register_query_table(face_info, query_table, angle_table):
    _, yaw, pitch, face = face_info
    table_idx, table_angle = get_candidate_idx(angle_table, face_info)
    if table_idx is not None:
        if query_table[table_idx] is None:
            candidate = [yaw, pitch, face]
            score = get_candidate_score(candidate, table_angle)
            query_table[table_idx] = [yaw, pitch, face, score]
            print("Registered ...!")
        else:
            candidate = query_table[table_idx]
            new_candidate = [yaw, pitch, face]
            score = get_candidate_score(new_candidate, table_angle)
            new_candidate = [yaw, pitch, face, score]
            selected_candidate = select_candidate(candidate, new_candidate)
            query_table[table_idx] = selected_candidate

    return query_table


def get_candidate_idx(angle_table, face_info):
    yaw_table, pitch_table, thresh = angle_table
    yaw_size = len(yaw_table)
    # face_info => [idx, yaw, pitch, face_img]

    candidate_idx = None
    angle = None
    for yaw in yaw_table:
        candidate = check_angle(face_info[1], yaw, thresh)
        if candidate is True:
            for pitch in pitch_table:
                candidate = check_angle(face_info[2], pitch, thresh)
                if candidate is True:
                    angle = [yaw, pitch, thresh]
                    direct = get_direct(angle, yaw_size)
                    candidate_idx = direct

    return candidate_idx, angle


def check_angle(face_angle, target_angle, margin):
    candidate = None
    if target_angle - margin <= face_angle <= target_angle + margin:
        candidate = True
    return candidate


def select_candidate(candidate, new_candidate):
    candidate_score = candidate[3]
    new_candidate_score = new_candidate[3]

    if candidate_score > new_candidate_score:
        selected_candidate = candidate
    elif candidate_score < new_candidate_score:
        selected_candidate = new_candidate
    else:
        selected_candidate = candidate

    return selected_candidate



if __name__ == '__main__':
    # Face Pose Estimation Parser
    # parser = argparse.ArgumentParser()
    # parser.add_argument("--png", action="store_true", help="if images are with .png extension")
    # parser.add_argument('--img_size', default=120, type=int)
    # parser.add_argument('-b', '--batch-size', default=1, type=int)
    # args = parser.parse_args()

    ###################################################################################################################
    # Query Image Read -> Query Face Detection -> Query Face Pose Estimation -> Create Query Face Tables
    # query_path = "img/samples/heroine_1"
    # query_path = "img/samples/heroine_sidekick"
    query_path = "img/samples/heroine_test"
    gallery_path = "img/samples/recog_test"

    print("Read Query Images ...")
    query_imgs = read_images(query_path)  # Load Query Images

    # Get Sorted Query Faces & Locations
    print("Get Face Information from Query ...")
    query_faces_in_frames, query_rects_in_frames, num_face = get_face_image_bbox(query_imgs)

    yaw_table = [-90, -45, 0, 45, 90]
    pitch_table = [-45, 0, 45]
    thresh = 10

    angle_table = [yaw_table, pitch_table, thresh]
    query_table = [None for i in range(len(yaw_table) * len(pitch_table))]
    query_tables = []
    for i in range(num_face):
        query_tables.append(query_table)

    fn = 0
    for faces_in_frame, rects_in_frame in zip(query_faces_in_frames, query_rects_in_frames):
        faces_info = face_pose_estimation(faces_in_frame)  # [[Class id, yaw, pitch, face img] ...]
        # print(faces_info)
        for face_info in faces_info:
            class_id = face_info[0]
            yaw = face_info[1]
            pitch = face_info[2]
            face = face_info[3]

            # print("Yaw : {} | Pitch : {}".format(yaw, pitch))
            # cv2.imshow("FACE", face)
            # cv2.waitKey(0)

            query_table = query_tables[class_id]
            # print(str(fn).zfill(5))
            query_table = register_query_table(face_info, query_table, angle_table)
            query_tables[class_id] = query_table
        fn += 1

    view_query_tables(query_tables)

    # for i, query_table in enumerate(query_tables):
    #     for n, data in enumerate(query_table):
    #         # print(data)
    #         if data is not None:
    #             face = data[2]
    #             # print(n)
    #             cv2.imshow("face", face)
    #             cv2.waitKey(0)

    ###################################################################################################################
    # Read Gallery Images -> Get Face Images & Locations -> Face Recognition
    print("Read Gallery Images ...")
    gallery_imgs = read_images(gallery_path)

    # Query Encodings
    classes_encoding = []
    classes_name = []
    for idx, query_table in enumerate(query_tables):
        class_idx = "Person_" + str(idx)
        class_encoding = []
        class_name = []
        for query in query_table:
            if query is not None:
                query_face = query[2]
                query_h, query_w, _ = query_face.shape
                query_face_location = [(0, query_w, query_h, 0)]
                query_face_encoding = face_recognition.face_encodings(query_face, query_face_location,
                                                                      num_jitters=10)[0]
                class_encoding.append(query_face_encoding)
                class_name.append(class_idx)
        classes_encoding.append(class_encoding)
        classes_name.append(class_name)

    print("Get Face Information from Gallery ...")
    gallery_faces_in_frames, gallery_rects_in_frames, num_face = get_face_image_bbox(gallery_imgs)
    gallery_classes_faces, gallery_classes_rects = get_class_faces(gallery_faces_in_frames, gallery_rects_in_frames, num_face)

    gallery_classes = []
    update_lists = []
    register_lists = []
    for n, class_encoding in enumerate(classes_encoding):
        class_name = classes_name[n]

        fn = 0
        for i, gallery_class_faces in enumerate(gallery_classes_faces):
            print("--------------------")
            print(fn)
            class_match = 0
            for gallery_class_face in gallery_class_faces:
                face_h, face_w, _ = gallery_class_face.shape
                face_location = [(0, face_w, face_h, 0)]
                face_encodings = face_recognition.face_encodings(gallery_class_face, face_location, num_jitters=10)

                for face_encoding in face_encodings:
                    matches = face_recognition.compare_faces(class_encoding, face_encoding, tolerance=0.6)
                    name = "Unknown"
                    face_distance = face_recognition.face_distance(class_encoding, face_encoding)
                    best_match_idx = np.argmin(face_distance)
                    if matches[best_match_idx]:
                        class_match += 1.0
                        name = class_name[best_match_idx]
            match_rate = round(class_match / len(gallery_class_faces), 2)
            print(match_rate)

            # Match rate = Query Table Images <---> Gallery Class Face Images
            if match_rate >= 0.8:
                # n: query_idx / i: gallery_idx
                update_lists.append([n, i])
            else:
                # TODO : Register New Face
                register_lists.append(i)
            fn += 1

    # Update Query Table
    for update_list in update_lists:
        query_idx, gallery_idx = update_list

        new_faces = gallery_classes_faces[gallery_idx]
        new_rects = gallery_classes_rects[gallery_idx]

        faces_info = face_pose_estimation(new_faces)  # [[Class id, yaw, pitch, face img] ..]

        for face_info in faces_info:
            yaw = face_info[1]
            pitch = face_info[2]
            face = face_info[3]

            query_table = query_tables[query_idx]
            query_table = register_query_table(face_info, query_table, angle_table)
            query_tables[query_idx] = query_table

    view_query_tables(query_tables)