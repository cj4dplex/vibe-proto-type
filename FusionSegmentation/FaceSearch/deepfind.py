import time
import os
import numpy as np
import pandas as pd
from tqdm import tqdm
import pickle

from deepface.basemodels import VGGFace, OpenFace, Facenet, Facenet512, FbDeepFace, DeepID, DlibWrapper, ArcFace, Boosting, SFaceWrapper
from deepface.commons import functions, realtime, distance as dst

# Tensorflow Error Log Filtering
# '1' : Filtering "INFO Log"
# '2' : Filtering "INFO Log" + "WARNING Log"
# '3' : Filtering "INFO Log" + "WARNING Log" + "ERROR Log"
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

import tensorflow as tf
tf_version = int(tf.__version__.split(".")[0])
if tf_version == 2:
    import logging
    tf.get_logger().setLevel(logging.ERROR)

gpus = tf.config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        tf.config.experimental.set_memory_growth(gpus[0], True)
    except RuntimeError as runTimeError:
        print(runTimeError)


def build_model(model_name):
    global model_obj #singleton design pattern
    models = {
        'VGG-Face': VGGFace.loadModel,
        'OpenFace': OpenFace.loadModel,
        'Facenet': Facenet.loadModel,
        'Facenet512': Facenet512.loadModel,
        'DeepFace': FbDeepFace.loadModel,
        'DeepID': DeepID.loadModel,
        'Dlib': DlibWrapper.loadModel,
        'ArcFace': ArcFace.loadModel,
        'SFace': SFaceWrapper.load_model,
        # 'Emotion': Emotion.loadModel,
        # 'Age': Age.loadModel,
        # 'Gender': Gender.loadModel,
        # 'Race': Race.loadModel
    }

    if not "model_obj" in globals():
        model_obj = {}

    if not model_name in model_obj.keys():
        model = models.get(model_name)
        if model:
            model = model()
            model_obj[model_name] = model
        else:
            raise ValueError('Invalid model_name passed - {}'.format(model_name))

    return model_obj[model_name]


def represent(img_path, model_name = 'VGG-Face', model = None, enforce_detection = True, detector_backend = 'opencv', align = True, normalization = 'base'):
    if model is None:
        model = build_model(model_name)

    #decide input shape
    input_shape_x, input_shape_y = functions.find_input_shape(model)

    #detect and align
    # preprocess_face -> img = 1) image path
    #                          2) numpy array image

    img = functions.preprocess_face(img = img_path
        , target_size=(input_shape_y, input_shape_x)
        , enforce_detection = enforce_detection
        , detector_backend = detector_backend
        , align = align)

    #custom normalization
    img = functions.normalize_input(img = img, normalization = normalization)

    #represent
    embedding = model.predict(img)[0].tolist()

    return embedding


def find(img_path, cls_faces, cls_names, model_name ='VGG-Face', distance_metric = 'cosine', model = None, enforce_detection = True, detector_backend = 'opencv', align = True, prog_bar = True, normalization = 'base', silent=False):
    """
    img_path: Single Image (nparray)
    db_path: Query Tables : [query table1, query table2, ...] -> query table : [face1, face2, ...]
    """
    pkPath = r"F:\deepface_temp"
    os.makedirs(pkPath, exist_ok=True)

    tic = time.time()
    img_paths, bulkProcess = functions.initialize_input(img_path)

    # if os.path.isdir(db_path) == True:
    if cls_faces != None:
        if model == None:
            if model_name == 'Ensemble':
                if not silent: print("Ensemble learning enabled")
                models = Boosting.loadModel()
            else: #model is not ensemble
                model = build_model(model_name)
                models = {}
                models[model_name] = model
        else: #model != None
            if not silent: print("Already built model is passed")
            if model_name == 'Ensemble':
                Boosting.validate_model(model)
                models = model.copy()
            else:
                models = {}
                models[model_name] = model

        #---------------------------------------

        if model_name == 'Ensemble':
            model_names = ['VGG-Face', 'Facenet', 'OpenFace', 'DeepFace']
            metric_names = ['cosine', 'euclidean', 'euclidean_l2']
        elif model_name != 'Ensemble':
            model_names = []; metric_names = []
            model_names.append(model_name)
            metric_names.append(distance_metric)

        #---------------------------------------

        file_name = "representations_%s.pkl" % (model_name)
        file_name = file_name.replace("-", "_").lower()
        pkgPath = os.path.join(pkPath, file_name)

        if os.path.exists(pkgPath):
            os.remove(pkgPath)
            # print("Remove Previous Representations")

        if os.path.exists(pkgPath):
            if not silent: print("WARNING: Representations for images in ",pkPath," folder were previously stored in ", file_name, ". If you added new instances after this file creation, then please delete this file and call find function again. It will create it again.")

            f = open(pkPath+'/'+file_name, 'rb')
            representations = pickle.load(f)

            if not silent: print("There are ", len(representations)," representations found in ",file_name)

        else: #create representation.pkl from scratch
            employees = []

            for query_table, cls_name in zip(cls_faces, cls_names):
                for face in query_table:
                    if face is not None:
                        employees.append([face, cls_name])

            if len(employees) == 0:
                raise ValueError("There is no image in Query Tables!")

            #------------------------
            #find representations for db images

            representations = []

            pbar = tqdm(range(0,len(employees)), desc='Finding representations', disable=prog_bar)

            #for employee in employees:
            for index in pbar:
                employee_face, employee_name = employees[index]  # png or jpg 단일 이미지 경로
                employee = employee_face.copy()
                employee_name = list(set(employee_name))[0]
                # print("Class Name ", employee_name)

                instance = []
                if employee_name not in instance:
                    instance.append(employee_name)  # Identity 입력

                for j in model_names:
                    custom_model = models[j]

                    representation = represent(img_path = employee
                        , model_name = model_name, model = custom_model
                        , enforce_detection = enforce_detection, detector_backend = detector_backend
                        , align = align
                        , normalization = normalization
                        )

                    instance.append(representation)

                #-------------------------------

                representations.append(instance)

            f = open(pkgPath, "wb")
            pickle.dump(representations, f)
            f.close()

            if not silent: print("Representations stored in ",pkgPath,"/",file_name," file. Please delete this file when you add new identities in your database.")

        #----------------------------
        #now, we got representations for facial database

        if model_name != 'Ensemble':
            df = pd.DataFrame(representations, columns = ["identity", "%s_representation" % (model_name)])
        else: #ensemble learning

            columns = ['identity']
            [columns.append('%s_representation' % i) for i in model_names]

            df = pd.DataFrame(representations, columns = columns)

        df_base = df.copy() #df will be filtered in each img. we will restore it for the next item.

        resp_obj = []

        global_pbar = tqdm(range(0, len(img_paths)), desc='Analyzing', disable=prog_bar)
        for j in global_pbar:
            img_path = img_paths[j]

            #find representation for passed image

            for j in model_names:
                custom_model = models[j]

                target_representation = represent(img_path = img_path
                    , model_name = model_name, model = custom_model
                    , enforce_detection = enforce_detection, detector_backend = detector_backend
                    , align = align
                    , normalization = normalization
                    )

                for k in metric_names:
                    distances = []
                    for index, instance in df.iterrows():
                        source_representation = instance["%s_representation" % (j)]

                        if k == 'cosine':
                            distance = dst.findCosineDistance(source_representation, target_representation)
                        elif k == 'euclidean':
                            distance = dst.findEuclideanDistance(source_representation, target_representation)
                        elif k == 'euclidean_l2':
                            distance = dst.findEuclideanDistance(dst.l2_normalize(source_representation), dst.l2_normalize(target_representation))

                        distances.append(distance)

                    #---------------------------

                    if model_name == 'Ensemble' and j == 'OpenFace' and k == 'euclidean':
                        continue
                    else:
                        df["%s_%s" % (j, k)] = distances

                        if model_name != 'Ensemble':
                            threshold = dst.findThreshold(j, k)
                            df = df.drop(columns = ["%s_representation" % (j)])
                            df = df[df["%s_%s" % (j, k)] <= threshold]

                            df = df.sort_values(by = ["%s_%s" % (j, k)], ascending=True).reset_index(drop=True)

                            resp_obj.append(df)
                            df = df_base.copy() #restore df for the next iteration

            #----------------------------------

            if model_name == 'Ensemble':

                feature_names = []
                for j in model_names:
                    for k in metric_names:
                        if model_name == 'Ensemble' and j == 'OpenFace' and k == 'euclidean':
                            continue
                        else:
                            feature = '%s_%s' % (j, k)
                            feature_names.append(feature)

                #print(df.head())

                x = df[feature_names].values

                #--------------------------------------

                boosted_tree = Boosting.build_gbm()

                y = boosted_tree.predict(x)

                verified_labels = []; scores = []
                for i in y:
                    verified = np.argmax(i) == 1
                    score = i[np.argmax(i)]

                    verified_labels.append(verified)
                    scores.append(score)

                df['verified'] = verified_labels
                df['score'] = scores

                df = df[df.verified == True]
                #df = df[df.score > 0.99] #confidence score
                df = df.sort_values(by = ["score"], ascending=False).reset_index(drop=True)
                df = df[['identity', 'verified', 'score']]

                resp_obj.append(df)
                df = df_base.copy() #restore df for the next iteration

            #----------------------------------

        toc = time.time()

        if not silent: print("find function lasts ",toc-tic," seconds")

        if len(resp_obj) == 1:
            return resp_obj[0]

        return resp_obj

    else:
        raise ValueError("Passed db_path does not exist!")
    return None