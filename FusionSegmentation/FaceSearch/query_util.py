import cv2
import face_recognition
import os, glob
from PIL import Image
import numpy as np

from box_util import get_direct, get_candidate_score
from face_util import face_pose_estimation
from utility.deepface_utils import get_feature


# 1. Register Query Table
def register_query_table(face_info, query_table, angle_table, mode=None):
    if mode is None:
        _, yaw, pitch, face = face_info
    else:
        yaw, pitch, face = face_info

    table_idx, pose, table_angle = get_candidate_idx(angle_table, face_info, mode=mode)
    if table_idx is not None:  # Target Angle 맞는 Face 있는 경우
        # print("Registered in query table ...! --> table idx : ", table_idx)
        if query_table[table_idx] is None:
            candidate = [yaw, pitch, face]
            score = get_candidate_score(candidate, pose, table_angle)
            query_table[table_idx] = [yaw, pitch, face, score]
        else:
            candidate = query_table[table_idx]
            new_candidate = [yaw, pitch, face]
            score = get_candidate_score(new_candidate, pose, table_angle)
            new_candidate = [yaw, pitch, face, score]
            selected_candidate = select_candidate(candidate, new_candidate)
            query_table[table_idx] = selected_candidate

    return query_table


def get_candidate_idx(angle_table, face_info, mode=None):
    yaw_table, pitch_table, thresh = angle_table

    target_angles = []
    if pitch_table is not None:
        for p in pitch_table:
            for y in yaw_table:
                ypt = [y, p, thresh]
                target_angles.append(ypt)
    else:
        for y in yaw_table:
            ypt = [y, None, thresh]
            target_angles.append(ypt)

    # face_info => [idx, yaw, pitch, face_img]  -> mode = None
    if mode is not None:
        yaw, pitch, face = face_info
    else:
        idx, yaw, pitch, face = face_info

    yaw_size = len(yaw_table)

    direct = None
    angle = None
    # for yaw in yaw_table:
        # candidate = check_angle(face_info[1], yaw, thresh)
        # if candidate is True:
    if pitch_table is not None:
        # for pitch in pitch_table:
            # candidate = check_angle(face_info[2], pitch, thresh)
            # if candidate is True:
        angle = [yaw, pitch, thresh]
        direct = get_direct(angle, yaw_size)
    else:
        angle = [yaw, None, thresh]
        direct = get_direct(angle, yaw_size)

    # # Yaw, Pitch -> Target 범위 내 들어가지 못한 경우에 Query Table에 미등록되는 상황 방지
    # # Thresh 내 포함되지 않는 경우로 Target Angle에 가장 근접한 Angle에 등록
    # if candidate_idx is None and angle is None:
    #     min_yaw = 360
    #     tmp_yaw = 0
    #     for yaw in yaw_table:
    #         yaw_gap = abs(yaw - face_info[1])
    #         if min_yaw > yaw_gap:
    #             min_yaw = yaw_gap
    #             tmp_yaw = yaw
    #
    #     if pitch_table is not None:
    #         min_pitch = 360
    #         tmp_pitch = 0
    #         for pitch in pitch_table:
    #             pitch_gap = abs(pitch - face_info[2])
    #             if min_pitch > pitch_gap:
    #                 min_pitch = pitch_gap
    #                 tmp_pitch = pitch
    #         angle = [tmp_yaw, tmp_pitch, thresh]
    #         direct = get_direct(angle, yaw_size)
    #         candidate_idx = direct
    #     else:
    #         angle = [tmp_yaw, None, thresh]
    #         direct = get_direct(angle, yaw_size)
    #         candidate_idx = direct

    if direct is not None:
        if pitch_table is None:
            candidate_idx = direct[0]
            pose = direct[1]
            target_angle = target_angles[candidate_idx]
        else:
            candidate_idx = direct
            pose = None
            target_angle = target_angles[candidate_idx]
    else:
        candidate_idx = None
        pose = None
        target_angle = None

    return candidate_idx, pose, target_angle


def check_angle(face_angle, target_angle, margin):
    candidate = False
    if target_angle - margin <= face_angle <= target_angle + margin:
        candidate = True
    return candidate


def select_candidate(candidate, new_candidate):
    candidate_score = candidate[3]
    new_candidate_score = new_candidate[3]

    if candidate_score > new_candidate_score:
        selected_candidate = candidate
    elif candidate_score < new_candidate_score:
        selected_candidate = new_candidate
    else:
        selected_candidate = candidate

    return selected_candidate


def load_query_tables(query_tables, query_names):
    print("Load Query Tables ..!")
    query_tables_names = []
    query_tables_faces = []

    for query_table, query_name in zip(query_tables, query_names):
        class_name = query_name
        query_names = []
        query_faces = []

        for query_data in query_table:
            if query_data is not None:
                query_face = query_data[2]
                cv2.imshow("FACE", query_face)
                cv2.waitKey(0)

                query_faces.append(query_face)
                query_names.append(class_name)

        query_tables_faces.append(query_faces)
        query_tables_names.append(query_names)

    return query_tables_names, query_tables_faces


def get_query_feature(query_features, yaw, pitch=None, query_table=None, model=None, flip=False):  # pitch는 현재 사용 안함
    query_feature = None

    # query_feats_cp = query_features.copy()
    query_feats_cp = query_features

    # Flip 기능 추가
    if flip == True:
        for idx in range(int(len(query_feats_cp) / 2)):
            if query_feats_cp[idx] is None:
                if query_feats_cp[len(query_feats_cp) - 1 - idx] is not None:
                    flip_face = cv2.flip(query_table[len(query_feats_cp) - 1 - idx][2], 1)
                    query_feats_cp[idx] = get_feature(flip_face, model=model)
            else:
                if query_feats_cp[len(query_feats_cp) - 1 - idx] is None:
                    flip_face = cv2.flip(query_table[idx][2], 1)
                    query_feats_cp[len(query_feats_cp) - 1 - idx] = get_feature(flip_face, model=model)

    if -15 <= yaw <= 15:  # Frontal Face
        query_feature = query_feats_cp[2]
    elif 60 >= yaw > 15:  # Half-Profile Face (Left)
        query_feature = query_feats_cp[3]
    elif -60 <= yaw < -15:  # Half-Profile Face (Right)
        query_feature = query_feats_cp[1]
    elif yaw > 60:  # Profile Face (Left)
        query_feature = query_feats_cp[4]
    elif yaw > -60:  # Profile Face (Right)
        query_feature = query_feats_cp[0]

    return query_feature


def get_query_face(query_faces, yaw, pitch=None):  # pitch는 현재 사용 안함
    query_face = None
    if pitch is None:
        if -15 <= yaw <= 15:  # Frontal Face
            query_face = query_faces[2]
        elif 60 >= yaw > 15:  # Half-Profile Face (Left)
            query_face = query_faces[3]
        elif -60 <= yaw < -15:  # Half-Profile Face (Right)
            query_face = query_faces[1]
        elif yaw > 60:  # Profile Face (Left)
            query_face = query_faces[4]
        elif yaw > -60:  # Profile Face (Right)
            query_face = query_faces[0]

    return query_face


def load_query_encoding(query_tables, model=None):
    query_tables_encodings = []
    query_tables_names = []
    query_tables_faces = []

    for idx, query_table in enumerate(query_tables):
        class_name = "Class_" + str(idx).zfill(3)
        query_encodings = []
        query_names = []
        query_faces = []

        for query_data in query_table:
            if query_data is not None:
                query_face = query_data[2]
                if model is not None:  # Face Recognition에서만 사용
                    face_h, face_w, _ = query_face.shape
                    query_face_location = [(0, face_w, face_h, 0)]
                    query_face_encoding = face_recognition.face_encodings(query_face, query_face_location,
                                                                          num_jitters=10)[0]
                    query_encodings.append(query_face_encoding)
            else:
                query_face = None

            query_names.append(class_name)
            query_faces.append(query_face)

        if model is not None:  # Face Recognition에서만 사용
            query_tables_encodings.append(query_encodings)

        query_tables_names.append(query_names)
        query_tables_faces.append(query_faces)

    return query_tables_encodings, query_tables_names, query_tables_faces


def best_match_query_table(candidate_update_list):
    max_distance_rate = 0
    best_query_table_idx = None
    for candidate in candidate_update_list:
        query_table_idx, distance_rate = candidate
        if max_distance_rate < distance_rate:
            max_distance_rate = distance_rate
            best_query_table_idx = query_table_idx

    # print("Best Matched Query Table Index : ", best_query_table_idx)
    # if max_distance_rate != 0:
    #     print("Distance Score : ", max_distance_rate)

    return best_query_table_idx


def read_query_tables(query_table_datas_path):
    print("Read Query Tables ...")
    query_tables = []
    query_names = []

    # image_files = os.listdir(query_table_datas_path)
    image_files = glob.glob(os.path.join(query_table_datas_path, "class_*.npy"))

    idxs = []
    for image_file in image_files:
        fileName = os.path.basename(image_file)
        name, _ = fileName.split(".")
        idxs.append(int(name[6:]))

    idxs.sort()
    for idx in idxs:
        image_file = query_table_datas_path + "/class_{}.npy".format(idx)
        fileName = os.path.basename(image_file)
        name, _ = fileName.split(".")
        print(fileName)

        query_table = np.load(image_file, allow_pickle=True)

        query_tables.append(query_table)
        query_names.append(name)

    print("Done ...")
    return query_tables, query_names
